select
	*
from
	dbo.refPlanning pl
	join dbo.refTypesOfPlanning tp on (tp.id=pl.idTypeOfPlanning)
	join dbo.refStoredProcedures sp on (sp.id=tp.idStoredProcedure)
	join sysobjects so on (so.name=sp.spName) and (so.type=N'P')
	join dbo.EnumsBase est on (est.id=pl.idSubPeriodType)
	join dbo.refPositions p on (p.id=pl.idResponsible)
	left join dbo.refPlanningValues pv on (pv.idPlanning=pl.id) and (pv.deleted=0) /* !!! */
where
	(pl.deleted=0)
	and (tp.deleted=0)
	and (sp.deleted=0)
	and (p.deleted=0)

declare
	@p0 bigint=0,
	@p1 bit=0,
	@p2 int=1301,
	@p3 bigint=281474976735885,
	@p4 int=1090,
	@p5 bigint=281474994082369,
	@p6 bigint=281474976710657
	
select
	*
from
(
	"dbo"."refPlanning" N0 with (nolock)
	left join "dbo"."refPositions" N11 with (nolock) on (N0."idResponsible" = N11."id")
)
outer apply
(
	select
		count(*) as Res
	from
	(
		(
			"dbo"."VisibilityScope" N1
			left join "dbo"."refPositions" N2 on (N1."idPosition" = N2."id")
		)
		left join "dbo"."refEmployeesPositions" N3 on (N2."idChief" = N3."id")
	)
	outer apply
	(
		select
			count(*) as Res
		from
			"dbo"."refEmployeesPositions" N4
		where ((N1."idPosition" = N4."idPosition") and (N4."idPosition" > 0) and N4."idEmployee" in (@p3) and (N4."deleted" = 0))
	) OA0 
	where ((N0."id" = N1."idItem") and (N1."ObjectType" = @p2) and ((N3."idEmployee" = @p3) or (OA0.Res > 0)) and (N1."deleted" = 0))
) OA0 
outer apply
(
	select
		count(*) as Res
	from
		"dbo"."refSets" N5
	outer apply
	(
		select
			count(*) as Res
		from
		(
			"dbo"."refPositionSets" N6
			left join "dbo"."refPositions" N7 on (N6."idPosition" = N7."id")
		)
		outer apply
		(
			select
				count(*) as Res
			from
				"dbo"."refPositionsDistributors" N8
			where ((N7."id" = N8."idPosition") and N8."idDistributor" in (@p5,@p6))
		) OA0 
		where ((N5."id" = N6."idSet") and (N7."idDistributor" in (@p5,@p6) or (OA0.Res > 0)) and (N6."deleted" = 0))
	) OA0 
	where ((N0."id" = N5."idItem") and (N5."ObjectType" = @p4) and (OA0.Res > 0) and (N5."deleted" = 0))
) OA1 
outer apply
(
	select
		count(*) as Res
	from
		"dbo"."refSets" N9
		outer apply
		(
			select
				count(*) as Res
			from
				"dbo"."refDistributorSets" N10
			where ((N9."id" = N10."idSet") and N10."idDistributor" in (@p5,@p6) and (N10."deleted" = 0))
		) OA0 
		where ((N0."id" = N9."idItem") and (N9."ObjectType" = @p4) and (OA0.Res > 0) and (N9."deleted" = 0))
) OA2 
where
(
	(N0."id" <> @p0)
	and (N0."deleted" = @p1)
	and (OA0.Res > 0)
	and ((OA1.Res > 0) or (OA2.Res > 0) or N11."idDistributor" in (@p5,@p6) or exists(select * from "dbo"."refPositionsDistributors" N12 where ((N11."id" = N12."idPosition") and N12."idDistributor" in (@p5,@p6))))
)
--Select parametrs: 0 False 1301 281474976735885 1090 281474994082369 281474976710657 7036874461357948

select
	*
from
	refPlanning pl
	join refPositions p on (p.id=pl.idResponsible)
where
	(pl.deleted=0) and
	(p.deleted=0) and
	(p.idDistributor in (281474994082369, 281474976710657))

select
	*
from
	refPlanning pl
	join refTypesOfPlanning tp on (tp.id=pl.idTypeOfPlanning)
	join refSets s on (s.idItem=pl.id)
	join XPObjectType ot on (ot.OID=s.ObjectType)
	join refBuyPointSets bs on (bs.idSet=s.id)
	join refBuyPoints bp on (bp.id=bs.idBuypoint)
where
	(pl.id=562954286500228)
	and (s.deleted=0)
	and (ot.TypeName=N'Chicago2.Core.StuBaseCore.StuObjects.StuSetPlanning')

select
	*
from
	refPlanning pl
	join refTypesOfPlanning tp on (tp.id=pl.idTypeOfPlanning)
	join refSets s on (s.idItem=pl.id)
	join XPObjectType ot on (ot.OID=s.ObjectType)
	join refDistributorSets ds on (ds.idSet=s.id)
	join refDistributors d on (d.id=ds.idDistributor)
where
	(pl.id=562954286500228)
	and (tp.isDistributorsSection=1)
	and (tp.deleted=0)
	and (s.TableName=N'refDistributorSets')
	and (s.deleted=0)
	and (ot.TypeName=N'Chicago2.Core.StuBaseCore.StuObjects.StuSetPlanning')

select
	*
from
	refPlanning pl
	join refTypesOfPlanning tp on (tp.id=pl.idTypeOfPlanning)
	join refSets s on (s.idItem=pl.id)
	join XPObjectType ot on (ot.OID=s.ObjectType)
	join refPositionSets ps on (ps.idSet=s.id)
	join refPositions p on (p.id=ps.idPosition)
where
	(pl.id=562954286500228)
	and (ps.deleted=0)
	and (ot.TypeName=N'Chicago2.Core.StuBaseCore.StuObjects.StuSetPlanning')

select
	*
from
	refPlanning pl
	join refDistributors d on (d.id=pl.idDistributor)
where
	(pl.deleted=0)
	and (d.deleted=0)

select
	*
from
	refPlanning pl
	join VisibilityScope vs on (vs.idItem=pl.id)
	join refPositions pc on (pc.id=vs.idPosition)
	join refEmployeesPositions epc on epc.id=pc.idChief
where
	(pl.deleted=0)
	and (vs.ObjectType=1301)
	and (vs.deleted=0)
	and (pc.deleted=0)
	and (epc.idEmployee=562949953422460)
	and (epc.deleted=0)