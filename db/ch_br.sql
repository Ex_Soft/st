backup database ch to disk = 'd:\temp\ch.bak' with format;
go

/*
Processed 527744 pages for database 'ch', file 'KRAFT_M' on file 1.
Processed 270168 pages for database 'ch', file 'KRAFT_M_1' on file 1.
Processed 266520 pages for database 'ch', file 'KRAFT_M_2' on file 1.
Processed 273088 pages for database 'ch', file 'KRAFT_M_3' on file 1.
Processed 5 pages for database 'ch', file 'KRAFT_M_log' on file 1.
Processed 0 pages for database 'ch', file 'KRAFT_M_log_3' on file 1.
BACKUP DATABASE successfully processed 1337525 pages in 443.177 seconds (23.578 MB/sec).
*/

/*
BACKUP DATABASE AdventureWorks2008R2 
   TO AdventureWorks2008R2Backups ;

RESTORE FILELISTONLY 
   FROM AdventureWorks2008R2Backups ;

RESTORE DATABASE TestDB 
   FROM AdventureWorks2008R2Backups 
   WITH MOVE 'AdventureWorks2008R2_Data' TO 'C:\MySQLServer\testdb.mdf',
   MOVE 'AdventureWorks2008R2_Log' TO 'C:\MySQLServer\testdb.ldf';
GO

USE master;
GO
-- First determine the number and names of the files in the backup.
-- AdventureWorks2012_Backup is the name of the backup device.
RESTORE FILELISTONLY
   FROM AdventureWorks2012_Backup;
-- Restore the files for MyAdvWorks.
RESTORE DATABASE MyAdvWorks
   FROM AdventureWorks2012_Backup
   WITH RECOVERY,
   MOVE 'AdventureWorks2012_Data' TO 'D:\MyData\MyAdvWorks_Data.mdf', 
   MOVE 'AdventureWorks2012_Log' TO 'F:\MyLog\MyAdvWorks_Log.ldf';
GO
*/

/*
RESTORE DATABASE
	[ch_old]
	FILE = N'KRAFT_M',
	FILE = N'KRAFT_M_1',
	FILE = N'KRAFT_M_2',
	FILE = N'KRAFT_M_3'
	FROM DISK = N'c:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\Backup\ch.bak'
	WITH FILE = 1,
	MOVE N'KRAFT_M' TO N'd:\db\ch_old.mdf',
	MOVE N'KRAFT_M_1' TO N'd:\db\ch_old_1.ndf',
	MOVE N'KRAFT_M_2' TO N'd:\db\ch_old_2.ndf',
	MOVE N'KRAFT_M_3' TO N'd:\db\ch_old_3.ndf',
	MOVE N'KRAFT_M_log' TO N'd:\db\ch_old_log.ldf',
	MOVE N'KRAFT_M_log_3' TO N'd:\db\ch_old_log_1.ldf',
	NOUNLOAD,  STATS = 10
GO
*/

/*
USE [master]
GO
CREATE DATABASE [ch] ON 
( FILENAME = N'D:\DB\ch.mdf' ),
( FILENAME = N'D:\DB\ch_log.ldf' ),
( FILENAME = N'D:\DB\ch_1.ndf' ),
( FILENAME = N'D:\DB\ch_2.ndf' ),
( FILENAME = N'D:\DB\ch_3.ndf' ),
( FILENAME = N'D:\DB\ch_log_1.ldf' )
 FOR ATTACH
GO
*/