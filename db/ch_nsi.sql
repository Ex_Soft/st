/*
declare
	@distributorIdCO bigint = null,
	@distributorId bigint = null,
	@distributorIdParent bigint = null,
	@distributorName nvarchar(100),
	@distributorCode nvarchar(3);

select @distributorName = N'��', @distributorCode = '1', @distributorIdParent = 0
select @distributorIdCO = id from dbo.refDistributors where IsRoot = 1 and deleted = 0;
if @distributorIdCO is not null
	update dbo.refDistributors set Name = @distributorName where id = @distributorIdCO;
else
	begin
		insert into dbo.refDistributors (id, Name, idParent, deleted, Code, [Address], TelNumber, IsReplicatable, ObjectType, NodeID)
		values (dbo.fn_getIdEx(@distributorIdCO, 1), @distributorName, @distributorIdParent, 0, @distributorCode, N'', N'', 0, 111, (select max(NodeID) + 1 from dbo.refDistributors));

		select @distributorIdCO = id from dbo.refDistributors where IsRoot = 1 and deleted = 0;
	end;

select @distributorId = null, @distributorName = N'�����-2', @distributorCode = N'2', @distributorIdParent = @distributorIdCO;
select @distributorId = id from dbo.refDistributors where Name = @distributorName and deleted = 0;
if @distributorId is not null
	update dbo.refDistributors set idParent = @distributorIdParent where id = @distributorId;
else
	insert into dbo.refDistributors (id, Name, idParent, deleted, Code, [Address], TelNumber, IsReplicatable, ObjectType, NodeID)
	values (dbo.fn_getIdEx(@distributorIdCO, 1), @distributorName, @distributorIdParent, 0, @distributorCode, N'', N'', 0, 111, (select max(NodeID) + 1 from dbo.refDistributors));

select @distributorId = null, @distributorName = N'�����-3', @distributorCode = N'3', @distributorIdParent = @distributorIdCO;
select @distributorId = id from dbo.refDistributors where Name = @distributorName and deleted = 0;
if @distributorId is not null
	update dbo.refDistributors set idParent = @distributorIdParent where id = @distributorId;
else
	insert into dbo.refDistributors (id, Name, idParent, deleted, Code, [Address], TelNumber, IsReplicatable, ObjectType, NodeID)
	values (dbo.fn_getIdEx(@distributorIdCO, 1), @distributorName, @distributorIdParent, 0, @distributorCode, N'', N'', 0, 111, (select max(NodeID) + 1 from dbo.refDistributors));

select @distributorId = null, @distributorName = N'�����-2.1', @distributorCode = N'21', @distributorIdParent = (select id from dbo.refDistributors where Name = N'�����-2' and deleted = 0);
select @distributorId = id from dbo.refDistributors where Name = @distributorName and deleted = 0;
if @distributorId is not null
	update dbo.refDistributors set idParent = @distributorIdParent where id = @distributorId;
else
	insert into dbo.refDistributors (id, Name, idParent, deleted, Code, [Address], TelNumber, IsReplicatable, ObjectType, NodeID)
	values (dbo.fn_getIdEx(@distributorIdCO, 1), @distributorName, @distributorIdParent, 0, @distributorCode, N'', N'', 0, 111, (select max(NodeID) + 1 from dbo.refDistributors));

declare @physicalPersons table (Name nvarchar(250));

insert into @physicalPersons (Name)
select N'���-�� (�����-2)'
union
select N'���-��.1 (�����-1)'
union
select N'���-2 (�����-2)'
union
select N'���-2.1 (�����-2)'
union
select N'���-2.1.0 (�����-1)'
union
select N'���-2.1.1 (�����-2)'
union
select N'���-2.2 (�����-1)'
union
select N'���-2.3 (�����-1)'
union
select N'���-2.4 (�����-2)'
union
select N'���-3 (�����-2)'
union
select N'���-3.1 (�����-2)';

;merge into dbo.refPhysicalPersons as tgt
using
(
	select
		Name
	from
		@physicalPersons
) as src
on tgt.Name = src.Name
when matched
	then
		update
		set
			deleted = 0
when not matched
	then
		insert (id, Name, LastName, FirstName, MiddleName, INN, JurAddress, FactAddress, Phone, PersonDocument, Limit, Rights, PostIndex1, PostIndex2, webLogin, webPassword)
		values (dbo.fn_getIdEx(@distributorIdCO, 1), src.Name, N'', N'', N'', N'', N'', N'', N'', N'', 0, 0, N'', N'', N'', N'');

declare
	@positionId bigint = null,
	@positionName nvarchar(50);

select @positionId = id, @positionName = N'������. (����� ��) (���-��) (� ������ ������������ ���-��.1)' from dbo.refPositions where idParent = 0 and deleted = 0;

if @positionId is not null
	update dbo.refPositions set Name = @positionName where id = @positionId;

declare @positions table (Name nvarchar(50));

insert into @positions (Name)
select N'����-2 (�����-2) (���-2) (� ������ ������������ ���-2.1.0)'
union
select N'����-3 (�����-3) (���-3) (� ������ ������������ ���-3.1)'
union
select N'����-2.3 (�����-2.1) (���-2.3)'
union
select N'����-2.4 (�����-2.1) (���-2.4)';

;merge into dbo.refPositions as tgt
using
(
	select
		Name
	from
		@physicalPersons
) as src
on tgt.Name = src.Name
when matched
	then
		update
		set
			deleted = 0
when not matched
	then
		insert (id, Name, LastName, FirstName, MiddleName, INN, JurAddress, FactAddress, Phone, PersonDocument, Limit, Rights, PostIndex1, PostIndex2, webLogin, webPassword)
		values (dbo.fn_getIdEx(@distributorIdCO, 1), src.Name, N'', N'', N'', N'', N'', N'', N'', N'', 0, 0, N'', N'', N'', N'');
*/
/*
insert into dbo.refPositions
(id, Name, idParent, idDistributor, deleted)
select
	dbo.fn_getIdEx(id, 1),
	N'����-2.2 (�����-2.1) (���-2.2)',
	20829148276589545,
	id,
	0
from
	dbo.refDistributors
where
	Name = N'�����-2.1'
	and deleted = 0
*/

--update dbo.refPositions set idDistributor = 281475014533942 where id = 21392098230011859
--update dbo.refPositions set idChief = 281475014730609 where id = 21392098230012860
--update dbo.refEmployeesPositions set deleted = 1 where id in (281475014754630)
/*
declare
	@distributorNameCO nvarchar(100) = N'��',
	@positionName nvarchar(50) = N'������. (����� ��) (���-��) (� ������ ������������',
	@physicalPersonName nvarchar(250) = N'���-2.1 (�����-2)'

select * from dbo.refEmployeesPositions where idEmployee = (select id from dbo.refPhysicalPersons where Name = @physicalPersonName) and deleted = 0
select * from dbo.refEmployeesPositions where idPosition = (select id from dbo.refPositions where Name = @positionName)	and deleted = 0

select
	physicalPersons.Name,
	case
		when positions.idChief = employeesPositions.id then N'isChief'
		else N'!isChief'
	end as isChief,
	*
from
	dbo.refPositions positions
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idPosition = positions.id
	join dbo.refPhysicalPersons physicalPersons on physicalPersons.id = employeesPositions.idEmployee
where
	positions.Name = @positionName
	and positions.deleted = 0
	and employeesPositions.deleted = 0

if not exists
(
select
	*
from
	dbo.refPositions positions
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idPosition = positions.id
	join dbo.refPhysicalPersons physicalPersons on physicalPersons.id = employeesPositions.idEmployee
where
	positions.Name = @positionName
	and positions.deleted = 0
	and employeesPositions.deleted = 0
	and physicalPersons.Name = @physicalPersonName
)
begin
;with cte(idDistributor, idPosition, idEmployee)
as
(
	select
		id as idDistributor,
		(select id from dbo.refPositions where Name = @positionName) as idPosition,
		(select id from dbo.refPhysicalPersons where Name = @physicalPersonName) as idEmployee
	from
		dbo.refDistributors where Name = @distributorNameCO
)
--select * from cte
insert into dbo.refEmployeesPositions
(id, idPosition, idEmployee, deleted)
select
	dbo.fn_getIdEx(cte.idDistributor, 1),
	cte.idPosition,
	cte.idEmployee,
	0 as deleted
from
	cte cte;
end

set @physicalPersonName = N'���-2.1.1 (�����-2)'
if not exists
(
select
	*
from
	dbo.refPositions positions
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idPosition = positions.id
	join dbo.refPhysicalPersons physicalPersons on physicalPersons.id = employeesPositions.idEmployee
where
	positions.Name = @positionName
	and positions.deleted = 0
	and employeesPositions.deleted = 0
	and physicalPersons.Name = @physicalPersonName
)
begin
;with cte(idDistributor, idPosition, idEmployee)
as
(
	select
		id as idDistributor,
		(select id from dbo.refPositions where Name = @positionName) as idPosition,
		(select id from dbo.refPhysicalPersons where Name = @physicalPersonName) as idEmployee
	from
		dbo.refDistributors where Name = @distributorNameCO
)
--select * from cte
insert into dbo.refEmployeesPositions
(id, idPosition, idEmployee, deleted)
select
	dbo.fn_getIdEx(cte.idDistributor, 1),
	cte.idPosition,
	cte.idEmployee,
	0 as deleted
from
	cte cte;
end
*/

declare
	@distributorNameCO nvarchar(100) = N'��',
	@positionName nvarchar(50) = N'������. (����� ��) (���-��) (� ������ ������������ ���-��.1)',
	@physicalPersonName nvarchar(250) = N'���-2.1 (�����-1)',
	@roleName nvarchar(35) = N'�����-1'

select
	*
from
	dbo.refRoles [roles]
	left join dbo.refEmployeesRoles employeesRoles on employeesRoles.idRole = [roles].id
where
	[roles].Name = @roleName

select
	*
from
	dbo.refPhysicalPersons physicalPersons
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idEmployee = physicalPersons.id
	join dbo.refPositions positions on positions.id = employeesPositions.idPosition
where
	physicalPersons.Name = @physicalPersonName

select
	*
from
	dbo.refPositions positions
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idPosition = positions.id
	join dbo.refPhysicalPersons physicalPersons on physicalPersons.id = employeesPositions.idEmployee
where
	positions.Name = @positionName
	and employeesPositions.deleted = 0

/*
insert into dbo.refEmployeesRoles (id, idRole, idEmployeesPositionsRoles, deleted) values (dbo.fn_getIdEx(281474976710657, 1), 281475014718364, 281475014723599, 0)
insert into dbo.refEmployeesRoles (id, idRole, idEmployeesPositionsRoles, deleted) values (dbo.fn_getIdEx(281474976710657, 1), 281475014718364, 281475014727603, 0)
insert into dbo.refEmployeesRoles (id, idRole, idEmployeesPositionsRoles, deleted) values (dbo.fn_getIdEx(281474976710657, 1), 281475014718364, 281475014730607, 0)
insert into dbo.refEmployeesRoles (id, idRole, idEmployeesPositionsRoles, deleted) values (dbo.fn_getIdEx(281474976710657, 1), 281475014718364, 281475014730608, 0)
insert into dbo.refEmployeesRoles (id, idRole, idEmployeesPositionsRoles, deleted) values (dbo.fn_getIdEx(281474976710657, 1), 281475014718364, 281475014730606, 0)
insert into dbo.refEmployeesRoles (id, idRole, idEmployeesPositionsRoles, deleted) values (dbo.fn_getIdEx(281474976710657, 1), 281475014718364, 281475014728604, 0)
insert into dbo.refEmployeesRoles (id, idRole, idEmployeesPositionsRoles, deleted) values (dbo.fn_getIdEx(281474976710657, 1), 281475014718364, 281475014728605, 0)

insert into dbo.refEmployeesRoles (id, idRole, idEmployeesPositionsRoles, deleted) values (dbo.fn_getIdEx(281474976710657, 1), 281475014717125, 281475014723600, 0)
insert into dbo.refEmployeesRoles (id, idRole, idEmployeesPositionsRoles, deleted) values (dbo.fn_getIdEx(281474976710657, 1), 281475014717125, 281475014727604, 0)
insert into dbo.refEmployeesRoles (id, idRole, idEmployeesPositionsRoles, deleted) values (dbo.fn_getIdEx(281474976710657, 1), 281475014717125, 281475014730609, 0)
insert into dbo.refEmployeesRoles (id, idRole, idEmployeesPositionsRoles, deleted) values (dbo.fn_getIdEx(281474976710657, 1), 281475014717125, 281475014729605, 0)
*/

exec sp_sys_UpdateTableVision