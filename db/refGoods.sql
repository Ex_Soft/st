﻿select * from dbo.EnumsBase where CodeKey like N'goods!_type!_%' escape N'!'

select
	*
from
	dbo.refGoods goods
	join dbo.EnumsBase enumsGoodsType on enumsGoodsType.id = goods.idGoodsType
where
	goods.deleted = 0

select
	goodsS.FullName,
	goodsC.FullName,
	*
from
	dbo.refGoods goodsS
	join dbo.refGoodsCompetitors goodsCompetitors on goodsCompetitors.idGoods = goodsS.id
	join dbo.refGoods goodsC on goodsC.id = goodsCompetitors.idGoodsCompetitor
	join dbo.EnumsBase enumsGoodsType on enumsGoodsType.id = goodsC.idGoodsType
where
	goodsS.deleted = 0
	and goodsCompetitors.deleted = 0
	and goodsC.deleted = 0
	and enumsGoodsType.CodeKey = N'goods_type_competitor'

select
	*
from
	dbo.refGoods goods
	left join dbo.EnumsBase enumsGoodsType on enumsGoodsType.id = goods.idGoodsType
where
	goods.deleted = 0
	and enumsGoodsType.CodeKey = N'goods_type_competitor'

select * from dbo.refGoods goods where goods.FullName like N'%сопо%'
--update dbo.refGoods set idGoodsCO = 281475013268946 where id = 562954287753820
select * from dbo.EnumsBase where CodeKey = N'goods_type_competitor'

select
	*
from
	dbo.refGoodsCompetitors

select * from dbo.SettingsBase where CodeKey = N'PhotoFilesServerPath'
select * from dbo.ConstsBase where CodeKey in (N'PhotoExchangeAddress', N'FileHostingObjects') and deleted = 0
select * from dbo.EnumsBase where CodeKey like N'%ConstType!_FileHostingObjects!_%' escape N'!' order by id

--update dbo.ConstsBase set Deleted = 0 where CodeKey = N'PhotoExchangeAddress'
--update dbo.ConstsBase set Deleted = 1 where CodeKey = N'PhotoExchangeAddress'
--update dbo.ConstsBase set Value = N'http://st-drive.systtech.ru/' where CodeKey = N'PhotoExchangeAddress'
--update dbo.ConstsBase set Value = N'20160204502791' where CodeKey = N'FileHostingObjects'
--update dbo.EnumsBase set CodeKey = N'ConstType_FileHostingObjects_PicturesOfGoods' where CodeKey = N'*ConstType_FileHostingObjects_PicturesOfGoods'
--update dbo.EnumsBase set CodeKey = N'ConstType_FileHostingObjects_PhotographyDocuments' where CodeKey = N'*ConstType_FileHostingObjects_PhotographyDocuments'

/* update dbo.refGoods set ImageBody='0x0', ImageCRC=0 where id=562954286347874 */

select
	*
	--id,
	--idDistributor,
	--ImageFileName,
	--ImageBody,
	--HtmlBody
from
	dbo.refGoods
where
	deleted = 0
	and idDistributor = 281474976710657
	--and (charindex(N'img', lower(HtmlBody)) != 0 or datalength(ImageBody) != 0)

select
	*
from
	dbo.refGoods goods
	join dbo.refFilePackets filePackets on filePackets.id = goods.idPacket
	join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idPacket = filePackets.id
	join dbo.refFiles files on files.id = filePacketsFiles.idFile
where
	goods.deleted = 0
	and filePackets.deleted = 0
	and filePacketsFiles.deleted = 0
	and files.deleted = 0
	and files.idLoadType = 201601144647402
	--and goods.FullName = N'53837 (verstamp) (II)'
	--and goods.FullName like N'53837%'
	and goods.idPacket != 0
	--goods.id in (281474976713112, 281474977068233, 281474988839173)

select * from dbo.EnumsBase where CodeKey like N'%LoadType_%' order by id

/*

delete
	dbo.refFilePacketsFiles
from
	dbo.refFilePacketsFiles filePacketsFiles
	join dbo.refFiles files on files.id = filePacketsFiles.idFile
where
	files.id in (562954287802987, 562954287803989)

delete from dbo.refFiles where id in (562954287802987, 562954287803989)

*/

select * from refFiles where id = 562954287789967
select * from refFiles where id in (562954287789967, 562954287770862)




/*
declare
	@Path nvarchar(max) = N'D:\temp\images\',
	@ImageFileName nvarchar(max) = N'koala.jpg'

exec(N'
	update
		dbo.refGoods
	set
		ImageBody = (select pic.* from openrowset(bulk '''+@Path+@ImageFileName+N''', single_blob) as pic)
	where
		FullName = N''Test ImgGoods2Files II''
	'
)

update dbo.refGoods set ImageFileName = @ImageFileName where FullName = N'Test ImgGoods2Files II'
*/

select
	*
from
	dbo.refGoods g
where
	g.deleted=0
	and g.FullName=N'Test ImgGoods2Files II'
	--and g.FullName like N'Test%'
	--and g.FullName like N'45846%'
	--and g.FullName like N'Товар с картинками в refFilePackets #%'
	--and g.idPacket = 562954286643427
	and g.idPacket != 0

--update dbo.refGoods set Name = N'45846 (002)' where id = 562954286637419
--update dbo.refGoods set idPacket = 562954286643432 where id = 562954286643438
--update dbo.refGoods set HtmlBody = N'<html><head></head><body>562954286643438</body></html>' where id = 562954286643438

select
	*
from
	dbo.refOutercodes oc
	join dbo.refGoods g on g.id=oc.idItem
where
	oc.TableName=N'refGoods'
	and oc.outercode=N'outerCodeTestGood'

;with isbase(id, weight)
as
(
   select
     gu.idGoods,
     g.Weight
   from
     refGoodsUnits gu
     join refGoods g on (g.id=gu.idGoods)
   where
     (gu.deleted=0)
     and (g.deleted=0)
     and (gu.idUnit=g.idBaseUnitMT)
)
select
  *
from
  isbase ib
  join refGoodsUnits gu on (gu.idGoods=ib.id)
  join refUnits u on (u.id=gu.idUnit)
where
  (gu.deleted=0)
  and (u.deleted=0)
  and (ib.id=562954286347874)

------------------------------------------------------------

select
	*
from
	dbo.refGoodsUnits gu
	left join dbo.refUnits un on un.id=gu.idUnit and un.deleted=0

select
	*
from
	dbo.refGoods goods
	join dbo.EnumsBase enumsGoodsType on enumsGoodsType.id = goods.idGoodsType
	left join dbo.refUnits activeUnit on activeUnit.id = goods.idActiveUnit and activeUnit.deleted = 0
	left join dbo.refUnits activeUnitMT on activeUnitMT.id = goods.idActiveUnitMT and activeUnitMT.deleted = 0
	left join dbo.refUnits baseUnitMT on baseUnitMT.id = goods.idBaseUnitMT and baseUnitMT.deleted = 0
where
	goods.FullName like N'67144%'
	--goods.deleted = 0

select
	*
from
	dbo.refGoods goods
	left join dbo.refGoodsUnits goodsUnits on goodsUnits.idGoods = goods.id and goodsUnits.deleted = 0
	left join dbo.refUnits units on units.id = goodsUnits.idUnit and units.deleted = 0
where
	--goodsUnits.Factor = 1
	goods.FullName like N'71851%'
	--goods.deleted = 0

select
	*
from
	dbo.refGoods gd
	left join dbo.refGoodsUnits gu on (gu.idGoods=gd.id) and (gu.idUnit=gd.idActiveUnit) and (gu.deleted=0)
	left join dbo.refUnits un on (un.id=gu.idUnit) and (un.deleted=0)

select
	--*
	--gd.*
	--ua.*
	--uamt.*
	--ubmt.*
	gu.*
	--guu.*
	--gd.FullName,
	--gd.idActiveUnit,
	--ua.Name as ActiveUnit,
	--gd.idActiveUnitMT,
	--uamt.Name as ActiveUnitMT,
	--gd.idBaseUnitMT,
	--ubmt.Name as BaseUnitMT
from
	dbo.refGoods gd
	left join dbo.refUnits ua on (ua.id=gd.idActiveUnit)
	left join dbo.refUnits uamt on (uamt.id=gd.idActiveUnitMT)
	left join dbo.refUnits ubmt on (ubmt.id=gd.idBaseUnitMT)
	left join dbo.refGoodsUnits gu on gu.idGoods = gd.id
	left join dbo.refUnits guu on guu.id = gu.idUnit
where gd.id in (281474993347371, 562954286637419)
order by gd.FullName

--update dbo.refGoods set FactorCO = 1.123456789012345 where id = 562954286637419
--update dbo.refGoodsUnits set Factor = 1.12345678901 where id = 562954286637421

select
	*
from
	dbo.refGoods g
	join dbo.refGoodsGroups gg on gg.id = g.idGroup
where
	g.deleted = 0

select
	g.id,
	count(gu.id)
from
	dbo.refGoods g
	join dbo.refGoodsUnits gu on gu.idGoods = g.id
where
	g.FullName = N'Grizly Sarate 80g'
	and g.deleted = 0
	and gu.deleted = 0
group by g.id
having count(gu.id) > 1
