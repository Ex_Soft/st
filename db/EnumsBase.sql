﻿--insert into dbo.enumsBase (id, CodeKey, Value, Description, Value_0, Description_0, MTCode) values (2017090468492, N'Attrs_ValueType_HTML', N'HTML текст', N'HTML текст', N'HTML text', N'HTML text', 0)

select
	*
from
	dbo.EnumsBase enumsBase
where
	--enumsBase.id = 133
	--enumsBase.CodeKey = N'ConstType_PreorderRestsStorageType_AgntLandStorage'
	--enumsBase.CodeKey like N'ConstType!_FileHostingObjects%' escape N'!'
	--enumsBase.CodeKey like N'%ReferencesAdditionalRight%'
	--enumsBase.CodeKey like N'%ConstType!_FileHostingObjects!_%' escape N'!'
	--e.CodeKey like N'Consts!_Category!_%' escape N'!'
	--enumsBase.CodeKey like N'Attrs!_ObjectType!_%' escape N'!'
	--enumsBase.CodeKey like N'Attrs!_ValueType!_%' escape N'!'
	--e.CodeKey like N'SplitTable%'
	--e.CodeKey like N'MimeType!_%' escape N'!'
	--e.CodeKey like N'Object!_Type!_MT%' escape N'!'
	--e.CodeKey like N'Object!_Type!_%' escape N'!'
	--enumsBase.CodeKey = N'Object_Name_Planning'
	enumsBase.CodeKey like N'Event!_%' escape N'!'
	--enumsBase.CodeKey like N'ScheduleObjects!_%' escape N'!'
	--enumsBase.Value like N'Мобильная часть системы%'
	--enumsBase.Value like N'%агент%'
	--e.Value = N'Редактирование'
	--e.Value = N'Настройки'
	--enumsBase.CodeKey like N'Right!_Type!_%' escape N'!'
	--e.CodeKey like N'RightType!_ForbidStartFinishWork!_%' escape N'!'
	--enumsBase.CodeKey like N'OrgStructure!_Right!_%' escape N'!'
	--e.Value like N'%Photo%'
order by enumsBase.CodeKey

--update dbo.EnumsBase set CodeKey = N'*ConstType_FileHostingObjects_AddAttrPhotoOfOutlet' where CodeKey = N'ConstType_FileHostingObjects_AddAttrPhotoOfOutlet'
--update dbo.EnumsBase set CodeKey = N'RightType_ForbidStartFinishWork_AtFinishNotifyOnly' where CodeKey = N'RightType_ForbidStartFinishWork_AtFinishVisitFrbd'
--update dbo.EnumsBase set CodeKey = N'Attrs_ValueType_Photo' where CodeKey = N'Attrs_ValueType__Photo'
--update dbo.EnumsBase set CodeKey = N'ConstType_FileHostingObjects_MarketingMaterials' where CodeKey = N'*ConstType_FileHostingObjects_MarketingMaterials'
--update dbo.EnumsBase set CodeKey = N'ReferencesAdditionalRight_Outlets_DelRestore' where CodeKey = N'ReferencesAdditionalRight_BuyPoints_DelRestore'
--update dbo.EnumsBase set CodeKey = N'ReferencesAdditionalRight_Outlets_Create' where CodeKey = N'ReferencesAdditionalRight_BuyPoints_Create'
--update dbo.EnumsBase set CodeKey = N'ReferencesAdditionalRight_OutletsCO_DelRestore' where CodeKey = N'ReferencesAdditionalRight_BuyPointsCO_DelRestore'
--update dbo.EnumsBase set CodeKey = N'ReferencesAdditionalRight_OutletsCO_Create' where CodeKey = N'ReferencesAdditionalRight_BuyPointsCO_Create'

select
	N'new {CodeKey = "' + CodeKey + N'", Value = "' + replace(Value, N'"', N'\"') + N'"},' as def,
	*
from
	dbo.EnumsBase
where
	CodeKey like N'Right!_Type!_%' escape N'!'
order by CodeKey

;merge into dbo.EnumsBase as tgt
using
(
	select 201707295844801 as id, N'OrgStructure_Right_WorkAddAttributesTypePhoto' as CodeKey, N'Работа с дополнительными атрибутами справочников с типом "Фотография"' as [Value], N'Работа с дополнительными атрибутами справочников с типом "Фотография"' as Value_0, 0 as MTCode, N'Право распространяется на все дополнительные атрибуты с типом «Фотография» используемые при работе со справочной информацией на мобильном устройстве' as [Description], N'Право распространяется на все дополнительные атрибуты с типом «Фотография» используемые при работе со справочной информацией на мобильном устройстве' as Description_0
	union
	select 201707295844802 as id, N'RightType_WorkAddAttributesTypePhoto_Edit' as CodeKey, N'Агент может редактировать доп. атрибуты с типом "Фото"' as [Value], N'Агент может редактировать доп. атрибуты с типом "Фото"' as Value_0, 0 as MTCode, N'отображать дополнительные атрибуты для справочников в мобильном устройстве с возможностью просмотра, добавления и удаления имеющихся фотографий' as [Description], N'отображать дополнительные атрибуты для справочников в мобильном устройстве с возможностью просмотра, добавления и удаления имеющихся фотографий' as Description_0
	union
	select 201707295844803 as id, N'RightType_WorkAddAttributesTypePhoto_View' as CodeKey, N'Агент может только просматривать доп. атрибуты с типом "Фото"' as [Value], N'Агент может только просматривать доп. атрибуты с типом "Фото"' as Value_0, 0 as MTCode, N'отображать дополнительные атрибуты для справочников в мобильном устройстве с возможностью просмотра имеющихся фотографий' as [Description], N'отображать дополнительные атрибуты для справочников в мобильном устройстве с возможностью просмотра имеющихся фотографий' as Description_0
)
as src
on tgt.CodeKey = src.CodeKey
when matched
	then
		update set [Value] = src.[Value], Value_0 = src.Value_0, MTCode = src.MTCode, [Description] = src.[Description], Description_0 = src.Description_0
when not matched
	then
		insert (id, CodeKey, [Value], Value_0, MTCode, [Description], Description_0)
		values (src.id, src.CodeKey, src.[Value], src.Value_0, src.MTCode, src.[Description], src.Description_0);

declare
	@tmpStr nvarchar(max),
	@len int,
	@i int,
	@tmpChar nchar(1)

select @tmpStr = CodeKey from dbo.EnumsBase where id = 1130
print @tmpStr
set @len=len(@tmpStr)
set @i=1

while @i<=@len
	begin
		set @tmpChar = substring(@tmpStr, @i, 1)
		print @i
		print @tmpChar
		print unicode(@tmpChar)
		set @i = @i+1
	end

set @tmpChar = nchar(1057)
print @tmpChar
print unicode(@tmpChar)

select * from dbo.EnumsBase where CodeKey = N'Object_Name_ViewBP' + nchar(1057) + N'ontrolledCOOutsideScope'
select * from dbo.EnumsBase where CodeKey = N'Object_Name_ViewBP' + nchar(67) + N'ontrolledCOOutsideScope'

--update dbo.EnumsBase set CodeKey = N'Object_Name_ViewBPControlledCOOutsideScope' where CodeKey = N'Object_Name_ViewBP' + nchar(1057) + N'ontrolledCOOutsideScope'
	