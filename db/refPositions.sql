﻿exec dbo.sp_dev_GetPositionsIds @employeeId = 281475014529937
select
	distributors.Name,
	positions.Name,
	*
from
	dbo.refPhysicalPersons physicalPersons
	join dbo.alPositionVision alPositionVision on alPositionVision.idEmployee = physicalPersons.id
	join dbo.refPositions positions on positions.id = alPositionVision.idPosition
	join dbo.refDistributors distributors on distributors.id = positions.idDistributor
where
	physicalPersons.Name = N'Сот-2 (Право-2)'
	and alPositionVision.deleted = 0
	and positions.deleted = 0
	and distributors.deleted = 0

select
	case
		when positions.idParent = 0 then N'isRootUser'
		else N'!isRootUser'
	end as isChief,
	employeesPositions.deleted as employeesPositionsDeleted,
	positions.Name,
	*
from
	dbo.refPhysicalPersons physicalPersons
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idEmployee = physicalPersons.id
	join dbo.refPositions positions on positions.id = employeesPositions.idPosition
where
	physicalPersons.Name = N'Сот-2 (Право-2)'
	and employeesPositions.deleted = 0
	and positions.deleted = 0

select * from dbo.refEmployeesPositions where idPosition = 20829148276589545 and deleted = 0
select * from dbo.refPositions where id in (281474976711438, 20829148276589545) and deleted = 0
select * from dbo.refPositions where idParent in (20829148276589545, 21392098230012859, 21392098230012860) and deleted = 0
select * from dbo.refPositions where Name like N'%ULS%'
--update dbo.refPositions set idParent = 281475014837714 where id = 281475014837762

select
	positions.Name,
	distributors.Name,
	*
from
	dbo.refPositionsDistributors positionsDistributors
	join dbo.refPositions positions on positions.id = positionsDistributors.idPosition
	join dbo.refDistributors distributors on distributors.id = positionsDistributors.idDistributor
where
	positionsDistributors.deleted = 0
order by positions.Name, distributors.Name

------------------------------------------------------------

select
	*
from
	dbo.refPositions positions
	left join dbo.refEmployeesPositions employeesPositions on employeesPositions.id = positions.idChief
	left join dbo.refPhysicalPersons physicalPersons on physicalPersons.id = employeesPositions.idEmployee
where
	positions.deleted = 0
	--and positions.Name = N'999_01'
	and positions.Name like N'Кор%'
	--and positions.id = 281474976711438


select
	*
from
	dbo.refPhysicalPersons physicalPersons
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idEmployee = physicalPersons.id
	join dbo.refPositions positions on positions.id = employeesPositions.idPosition
where
	physicalPersons.Name = N'_A_0000000000001'
	and physicalPersons.deleted = 0
	and employeesPositions.deleted = 0
	and positions.deleted = 0

;select
	case
		when positions.idChief = employeesPositions.id then N'isChief'
		else N'!isChief'
	end as isChief,
	*
from
	dbo.refPhysicalPersons employee with (nolock)
	left join dbo.refEmployeesPositions employeesPositions with (nolock) on employeesPositions.idEmployee = employee.id
	left join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
	left join dbo.refEmployeesRoles employeesRoles with (nolock) on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
where
	positions.deleted = 0
	and positions.idChief = employeesPositions.id
	and employee.Code = N'D1570'
	--and positions.Name = N'TestPosition'
	and positions.Name = N'_A_'

;select
	*
from
	dbo.refPositions position
	join dbo.refEmployeesPositions employeePosition on employeePosition.id = position.idChief
	join dbo.refPhysicalPersons employee on employee.id = employeePosition.idEmployee
where
	position.idParent = 0
	and position.deleted = 0
	and employeePosition.deleted = 0
	and employee.deleted = 0

;select
	*
from
	dbo.refPositions position
	join dbo.refEmployeesPositions employeePosition on employeePosition.idPosition = position.id
	join dbo.refPhysicalPersons employee on employee.id = employeePosition.idEmployee
where
	position.idParent = 0
	and position.deleted = 0
	and employeePosition.deleted = 0
	and employee.deleted = 0
	and employeePosition.id != position.idChief

select
	*
from
	dbo.refPositions p
	join dbo.refEmployeesPositions ep on ep.id=p.idChief
	join dbo.refPhysicalPersons pp on pp.id=ep.idEmployee
where
	p.Name=N'999_01'

select
	*
from
	dbo.refPositions p
	join dbo.refEmployeesPositions ep on ep.idPosition=p.id
	join dbo.refPhysicalPersons pp on pp.id=ep.idEmployee
where
	p.Name=N'999_01'

select
	employeesPositions.idEmployee
from
	dbo.refPositions positions
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.id = positions.idChief
where
	positions.deleted = 0
	and employeesPositions.deleted = 0
	and positions.idDistributor in (281474992256801)
union
select
	employeesPositions.idEmployee
from
	dbo.refPositions positions
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idPosition = positions.id
	join dbo.refPhysicalPersons employees on employees.id = employeesPositions.idEmployee
where
	positions.deleted = 0
	and employeesPositions.deleted = 0
	and employees.deleted = 0
	and employeesPositions.id != positions.idChief
	and employees.idDistributor in (281474992256801)

select
	case
		when p.idChief=ep.id then N'isChief'
		else N'!isChief'
	end as isChief,
	*
from
	dbo.refPhysicalPersons pp
	join dbo.refEmployeesPositions ep on ep.idEmployee = pp.id
	join dbo.refPositions p on p.id = ep.idPosition
where
	pp.Name='!ТЕСТ'

select
	*
from
	dbo.refPositions positions
where
	positions.id in (281474976711438, 281474976711444, 281474977188470, 281474978350709, 281474976711456, 281474976711531, 13229323905401835, 13229323905401837, 281474992513228, 281474992515235, 281474992515246, 281474992515257, 281474992516234, 18295873486195643, 18577348462903297, 281474992314561)
	--positions.idChief=0

select
	*
from
	dbo.refPositions p
	join dbo.refEmployeesPositions ep on ep.id=p.idChief

------------------------------------------------------------

select
	case
		when positions.idChief = employeesPositions.id then N'isChief'
		else N'!isChief'
	end as isChief,
	employees.Name,
	*
from
	dbo.refPositions positions
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.id = positions.idChief
	join dbo.refPhysicalPersons employees on employees.id = employeesPositions.idEmployee
where
	positions.id = 14355223812246461
union
select
	case
		when positions.idChief = employeesPositions.id then N'isChief'
		else N'!isChief'
	end as isChief,
	employees.Name,
	*
from
	dbo.refPositions positions
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idPosition = positions.id
	join dbo.refPhysicalPersons employees on employees.id = employeesPositions.idEmployee
where
	positions.id = 14355223812246461
	and employeesPositions.id != positions.idChief

-- down (all)
;with RecursiveQuery (id, idParent, idChief, Name, Level, Path)
as
(
   select
     t.id,
     t.idParent,
	 t.idChief,
     t.Name,
     0 as Level,
     cast(N'/' + t.Name as varchar(max)) as Path
   from
     dbo.refPositions t
   where
     --(t.id=281474976711531)
	 t.idParent = 0
     and t.deleted = 0
   union all
   select
     t.id,
     t.idParent,
	 t.idChief,
     t.Name,
     Level+1,
     cast(Path + N'/' + t.Name as varchar(max))
   from
     dbo.refPositions t
     join RecursiveQuery rq on (rq.id=t.idParent)
   where
     t.deleted = 0
)
select
	rq.id,
	rq.idParent,
	rq.idChief,
	case
		when rq.idChief = employeesPositions.id then N'isChief'
		else N'!isChief'
	end as isChief,
	rq.Name,
	rq.Level,
	rq.Path,
	employeesPositions.*,
	physicalPersons.*
from
	RecursiveQuery rq
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idPosition = rq.id
	join dbo.refPhysicalPersons physicalPersons on physicalPersons.id = employeesPositions.idEmployee
where
	employeesPositions.deleted = 0
	and physicalPersons.deleted = 0
order by rq.Level
go

-- down (all)
;with RecursiveQuery (id, idParent, Name, Level, Path)
as
(
   select
     t.id,
     t.idParent,
     t.Name,
     0 as Level,
     cast(N'/' + t.Name as varchar(max)) as Path
   from
     dbo.refPositions t
   where
     (t.id=281474976711438)
     and t.deleted = 0
   union all
   select
     t.id,
     t.idParent,
     t.Name,
     Level+1,
     cast(Path + N'/' + t.Name as varchar(max))
   from
     dbo.refPositions t
     join RecursiveQuery rq on (rq.id=t.idParent)
   where
     t.deleted = 0
)
select
  rq.id,
  rq.idParent,
  rq.Name,
  rq.Level,
  rq.Path
from
  RecursiveQuery rq
order by rq.Level
go

;with cte(id, idParent, idDistributor, Name, Level, Path)
as
(
	select
		positions.id,
		positions.idParent,
		distributors.id,
		positions.Name,
		0 as Level,
		cast(N'/' + positions.Name as varchar(max)) as Path
	from
		dbo.refEmployeesPositions employeesPositions with (nolock)
		join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
		join dbo.refDistributors distributors with (nolock) on distributors.id = positions.idDistributor
	where
		employeesPositions.idEmployee = 5910974531913347
		and employeesPositions.deleted = 0
		and positions.deleted = 0
		and distributors.deleted = 0
	union all
	select
		positions.id,
		positions.idParent,
		distributors.id,
		positions.Name,
		Level + 1,
		cast(Path + N'/' + positions.Name as varchar(max))
	from
		cte cte
		join dbo.refPositions positions with (nolock) on positions.idParent = cte.id
		join dbo.refDistributors distributors with (nolock) on distributors.id = positions.idDistributor
	where
		positions.deleted = 0
		and distributors.deleted = 0
)
select
	*
from
	cte
go

;with cte(id, idParent, idDistributor, Level)
as
(
	select
		positions.id,
		positions.idParent,
		distributors.id,
		0 as Level
	from
		dbo.refEmployeesPositions employeesPositions with (nolock)
		join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
		join dbo.refDistributors distributors with (nolock) on distributors.id = positions.idDistributor
	where
		employeesPositions.idEmployee = 5910974531913347
		and employeesPositions.deleted = 0
		and positions.deleted = 0
		and distributors.deleted = 0
	union all
	select
		positions.id,
		positions.idParent,
		distributors.id,
		Level + 1
	from
		cte cte
		join dbo.refPositions positions with (nolock) on positions.idParent = cte.id
		join dbo.refDistributors distributors with (nolock) on distributors.id = positions.idDistributor
	where
		positions.deleted = 0
		and distributors.deleted = 0
)
select distinct
	idDistributor
from
	cte
where
	Level > 0
go

-- up
;with positionsDown (id, idParent, idChief, Name, deleted, Level, Path)
as
(
	select
		positions.id,
		positions.idParent,
		positions.idChief,
		positions.Name,
		positions.deleted,
		0 as Level,
		cast(N'/' + positions.Name as varchar(max)) as Path
	from
		dbo.refPositions positions
	where
		positions.id != 0
		and positions.idParent = 0
		--and positions.deleted = 0
	union all
	select
		positions.id,
		positions.idParent,
		positions.idChief,
		positions.Name,
		positions.deleted,
		Level + 1,
		cast(Path + N'/' + positions.Name as varchar(max))
	from
		dbo.refPositions positions
		join positionsDown recursiveQuery on recursiveQuery.id = positions.idParent
	where
		--positions.deleted = 0
		positions.idParent != 0
)
select
	*
from
	positionsDown positionsDown
order by positionsDown.Level
--option (maxrecursion 10)

;with positionsDown (id, idParent, idChief, Name, deleted, Level, Path)
as
(
	select
		positions.id,
		positions.idParent,
		positions.idChief,
		positions.Name,
		positions.deleted,
		0 as Level,
		cast(N'/' + positions.Name as varchar(max)) as Path
	from
		dbo.refPositions positions
	where
		positions.id != 0
		and positions.idParent = 0
		--and positions.deleted = 0
	union all
	select
		positions.id,
		positions.idParent,
		positions.idChief,
		positions.Name,
		positions.deleted,
		Level + 1,
		cast(Path + N'/' + positions.Name as varchar(max))
	from
		dbo.refPositions positions
		join positionsDown recursiveQuery on recursiveQuery.id = positions.idParent
	where
		--positions.deleted = 0
		positions.idParent != 0
)
select
	positionsDown.id,
	positionsDown.idParent,
	count(*)
from
	positionsDown positionsDown
group by positionsDown.id, positionsDown.idParent
--option (maxrecursion 10)

;with positionsUp (id, idParent, idChief, Name, deleted, Level, Path)
as
(
	select
		positions.id,
		positions.idParent,
		positions.idChief,
		positions.Name,
		positions.deleted,
		0 as Level,
		cast(N'/' + positions.Name as varchar(max)) as Path
	from
		dbo.refPositions positions
	where
		positions.id = 35747322111496546
		--and positions.deleted = 0
	union all
	select
		positions.id,
		positions.idParent,
		positions.idChief,
		positions.Name,
		positions.deleted,
		Level + 1,
		cast(Path + N'/' + positions.Name as varchar(max))
	from
		dbo.refPositions positions
		join positionsUp recursiveQuery on recursiveQuery.idParent = positions.id
	where
		--positions.deleted = 0
		positions.id != 0
)
select
	*
from
	positionsUp positionsUp
order by positionsUp.Level

------------------------------------------------------------

--позиции чувака

select
	*
from
	dbo.refEmployeesPositions employeesPositions with (nolock)
	join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
	join dbo.refDistributors distributors with (nolock) on distributors.id = positions.idDistributor
where
	employeesPositions.idEmployee = 5910974531913347
	and employeesPositions.deleted = 0
	and positions.deleted = 0
	and distributors.deleted = 0

select
	case
		when employeesPositions.id = positions.idChief then N'isChief'
		else N'!isChief'
	end as isChief,
	*
from
	dbo.refEmployeesPositions employeesPositions with (nolock)
	join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
	join dbo.refDistributors distributors with (nolock) on distributors.id = positions.idDistributor
where
	employeesPositions.idEmployee = (select id from dbo.refPhysicalPersons where Name = N'test_CAL' and deleted = 0)
	and employeesPositions.deleted = 0
	and positions.deleted = 0
	and distributors.deleted = 0

------------------------------------------------------------

;with positionsUp(id, idParent, Level)
as
(
	select
		positions.id,
		positions.idParent,
		0 as Level
	from
		dbo.refEmployeesPositions employeesPositions with (nolock)
		join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
	where
		employeesPositions.idEmployee = 5910974531913347
		and employeesPositions.deleted = 0
		and positions.deleted = 0
	union all
	select
		positions.id,
		positions.idParent,
		Level + 1
	from
		positionsUp positionsUp
		join dbo.refPositions positions with (nolock) on positions.id = positionsUp.idParent
	where
		positions.deleted = 0
),
positionsDown(id, idParent, Level)
as
(
	select
		positions.id,
		positions.idParent,
		0 as Level
	from
		dbo.refEmployeesPositions employeesPositions with (nolock)
		join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
	where
		employeesPositions.idEmployee = 5910974531913347
		and employeesPositions.deleted = 0
		and positions.deleted = 0
	union all
	select
		positions.id,
		positions.idParent,
		Level + 1
	from
		positionsDown positionsDown
		join dbo.refPositions positions with (nolock) on positions.idParent = positionsDown.id
	where
		positions.deleted = 0
)
select
	*
from
	positionsDown positionsDown
	join dbo.refPositions p on p.id = positionsDown.id
union
select
	*
from
	positionsUp positionsUp
	join dbo.refPositions p on p.id = positionsUp.id

------------------------------------------------------------

;with distributorsUp(id, idParent, idDistributor, Level)
as
(
	select
		positions.id,
		positions.idParent,
		distributors.id,
		0 as Level
	from
		dbo.refEmployeesPositions employeesPositions with (nolock)
		join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
		join dbo.refDistributors distributors with (nolock) on distributors.id = positions.idDistributor
	where
		employeesPositions.idEmployee = 5910974531913347
		and employeesPositions.deleted = 0
		and positions.deleted = 0
		and distributors.deleted = 0
	union all
	select
		positions.id,
		positions.idParent,
		distributors.id,
		Level + 1
	from
		distributorsUp distributorsUp
		join dbo.refPositions positions with (nolock) on positions.id = distributorsUp.idParent
		join dbo.refDistributors distributors with (nolock) on distributors.id = positions.idDistributor
	where
		positions.deleted = 0
		and distributors.deleted = 0
),
distributorsDown(id, idParent, idDistributor, Level)
as
(
	select
		positions.id,
		positions.idParent,
		distributors.id,
		0 as Level
	from
		dbo.refEmployeesPositions employeesPositions with (nolock)
		join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
		join dbo.refDistributors distributors with (nolock) on distributors.id = positions.idDistributor
	where
		employeesPositions.idEmployee = 5910974531913347
		and employeesPositions.deleted = 0
		and positions.deleted = 0
		and distributors.deleted = 0
	union all
	select
		positions.id,
		positions.idParent,
		distributors.id,
		Level + 1
	from
		distributorsDown distributorsDown
		join dbo.refPositions positions with (nolock) on positions.idParent = distributorsDown.id
		join dbo.refDistributors distributors with (nolock) on distributors.id = positions.idDistributor
	where
		positions.deleted = 0
		and distributors.deleted = 0
)
select
	*
from
	distributorsDown distributorsDown
	join dbo.refDistributors d on d.id = distributorsDown.idDistributor
union
select
	*
from
	distributorsUp
	join dbo.refDistributors d on d.id = distributorsUp.idDistributor

------------------------------------------------------------

select
	positions.Name,
	propertiesHistory.*,
	Manager.Name as Manager,
	Distributor.Name as Distributor,
	ManagerOwnJurPerson.Name as ManagerOwnJurPerson,
	ManagerOwnJurPersonAccessory.Value as ManagerOwnJurPersonAccessory,
	objectType.*
from
	dbo.refPositions positions
	join dbo.refPropertiesHistory propertiesHistory on propertiesHistory.idItem = positions.id
	join dbo.XPObjectType objectType on objectType.OID = propertiesHistory.ObjectType
	left join dbo.refPhysicalPersons Manager on propertiesHistory.AttributeName = N'Manager' and Manager.id = cast(coalesce(propertiesHistory.AttributeValue, N'0') as bigint)
	left join dbo.refDistributors Distributor on propertiesHistory.AttributeName = N'Distributor' and Distributor.id = cast(coalesce(propertiesHistory.AttributeValue, N'0') as bigint)
	left join dbo.refOwnJuridicalPersons ManagerOwnJurPerson on propertiesHistory.AttributeName = N'Manager.OwnJurPerson' and ManagerOwnJurPerson.id = cast(coalesce(propertiesHistory.AttributeValue, N'0') as bigint)
	left join dbo.EnumsBase ManagerOwnJurPersonAccessory on propertiesHistory.AttributeName = N'Manager.OwnJurPerson.Accessory' and ManagerOwnJurPersonAccessory.id = cast(coalesce(propertiesHistory.AttributeValue, N'0') as bigint)
where
	positions.id = 10414574138303417
	and propertiesHistory.deleted = 0
order by propertiesHistory.EventDate

------------------------------------------------------------

declare
	@idPosition bigint = 10414574138303417,
	--@asOfDate date = null,
	@asOfDate date = N'20150521 13:13:13.123',
	@idEmployee bigint = null

if @asOfDate is not null
	select top 1
		@idEmployee = cast(propertiesHistory.AttributeValue as bigint)
	from
		dbo.refPropertiesHistory propertiesHistory with (nolock)
		join dbo.XPObjectType objectType with (nolock) on objectType.OID = propertiesHistory.ObjectType
	where
		propertiesHistory.idItem = @idPosition
		and propertiesHistory.deleted = 0
		and propertiesHistory.AttributeName = N'Manager'
		and cast(propertiesHistory.EventDate as date) <= @asOfDate
		and objectType.TypeName = N'Chicago2.Core.StuBaseCore.StuObjects.StuChangedAttributesPosition'
	order by propertiesHistory.EventDate desc

if @idEmployee is null
	select
		@idEmployee = employeesPositions.idEmployee
	from
		dbo.refPositions positions with (nolock)
		join dbo.refEmployeesPositions employeesPositions with (nolock) on employeesPositions.id = positions.idChief
	where
		positions.id = @idPosition

select @idEmployee

------------------------------------------------------------

select
	employeesPositions.idEmployee,
	count(*)
from
	dbo.refEmployeesPositions employeesPositions
	join dbo.refPositions positions on positions.idChief = employeesPositions.id
where
	employeesPositions.deleted = 0
	and positions.deleted = 0
group by employeesPositions.idEmployee
having count(*) > 1

------------------------------------------------------------

;with rq (id, idParent, idChief, Name, Level, Path)
as
(
   select
     positions.id,
     positions.idParent,
	 positions.idChief,
     positions.Name,
     0 as Level,
     cast(N'/' + positions.Name as varchar(max)) as Path
   from
     dbo.refPositions positions
   where
	 positions.idParent = 0
     and positions.deleted = 0
   union all
   select
     positions.id,
     positions.idParent,
	 positions.idChief,
     positions.Name,
     Level+1,
     cast(Path + N'/' + positions.Name as varchar(max))
   from
     dbo.refPositions positions
     join rq rq on (rq.id = positions.idParent)
   where
     positions.deleted = 0
)
select
	case
		when rq.idChief = employeesPositions.id then N'isChief'
		else N'!isChief'
	end as isChief,
	physicalPersons.Name,
	rq.Name,
	*
from
	rq rq
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idPosition = rq.id
	join dbo.refPhysicalPersons physicalPersons on physicalPersons.id = employeesPositions.idEmployee
where
	employeesPositions.deleted = 0
	and physicalPersons.deleted = 0
	--and rq.idChief = employeesPositions.id
order by rq.Path;

select
	*
from
	dbo.alPositionVision alPositionVision
	join dbo.refPositions positions on positions.id = alPositionVision.idPosition
	join dbo.refPhysicalPersons physicalPersons on physicalPersons.id = alPositionVision.idEmployee
where
	alPositionVision.deleted = 0
	--and alPositionVision.idPosition = 281474976712779
	--and alPositionVision.idEmployee in (281474976712788, 281474976712789)
	--and alPositionVision.idEmployee = 281474976712786
	--and tableVision = 1
order by idPosition, idEmployee, tableVision;