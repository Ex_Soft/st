﻿select
	*
from
	dbo.alPositionVision alPositionVision
where
	alPositionVision.idEmployee = (select id from dbo.refPhysicalPersons where Name = N'Сот-2.1.1 (Право-2)')
	and alPositionVision.deleted = 0
	and alPositionVision.tableVision = 1
order by alPositionVision.idPosition

select
	case alPositionVision.tableVision
		when 1 then N'«Все»'
		when 2 then N'«Подчиненные»'
		when 3 then N'«Родительские»'
		when 4 then N'«Свои + ЦО»'
	end,
	*
from
	dbo.alPositionVision alPositionVision
	join dbo.refPositions positions on positions.id = alPositionVision.idPosition
	join dbo.refDistributors distributors on distributors.id = positions.idDistributor
where
	alPositionVision.idEmployee = (select id from dbo.refPhysicalPersons where Name = N'Сот-2.1.0 (Право-1)')
	and alPositionVision.deleted = 0
	and alPositionVision.tableVision = 1
	and positions.deleted = 0