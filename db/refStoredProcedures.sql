select
	*
from
	dbo.EnumsBase
where
	--CodeKey like N'SP%'
	--CodeKey like N'Object_Name%'
	--CodeKey like N'StoredProcedures_Type_%'
	CodeKey like N'Attrs_ValueType_%'

--insert into dbo.refStoredProceduresParam(id, idStoredProcedure, paramName, paramBaseName, paramDefaultValue, idMetaData, outputDirection, IsListValue, deleted) values (dbo.fn_getIdEx(281474977273617, 1), 281474992379608, N'@paramHTML', N'@paramHTML', N'', 2017090468492, 0, 0, 0)
--update dbo.refStoredProceduresParam set idStoredProcedure = 281474992379608, paramName = N'@paramHTML', paramDefaultValue = N'' where id = 4222124650666848

select
	*
from
	dbo.refStoredProcedures storedProcedures
	join dbo.EnumsBase enumStoredProcedureType on enumStoredProcedureType.id = storedProcedures.idStoredProcedureType
	join dbo.refStoredProceduresParam storedProceduresParam on storedProceduresParam.idStoredProcedure = storedProcedures.id
	join dbo.EnumsBase enumMetaData on enumMetaData.id = storedProceduresParam.idMetaData
where
	storedProcedures.deleted = 0
	and storedProcedures.spName = N'autoInformerBySchedule'
	and storedProceduresParam.deleted = 0
	and enumMetaData.CodeKey = N'Attrs_ValueType_String'

select * from dbo.refStoredProceduresParam where idMetaData = 2017090468492

select
	*
from
	dbo.refStoredProcedures storedProcedures
	join dbo.refStoredProceduresParam storedProceduresParam on storedProceduresParam.idStoredProcedure = storedProcedures.id
where
	storedProcedures.spName = N'sp_import_xls_BuyPoints'
	storedProcedures.CodeKey = N'report_test_parameters_sp'
	storedProcedures.id = 281474977118722

/* update dbo.refStoredProceduresParam set IsListValue = 1 where id = 281474977118725 */
/* update dbo.refStoredProceduresParam set paramDefaultValue = N'' where id = 281475016082516 */

select
	*
from
	dbo.enXlsExportTypesBase xlsExportTypesBase
where
	xlsExportTypesBase.refMetadata = N'BuyPoints'
	xlsExportTypesBase.reportSP = N'sp_import_xls_Outlets'