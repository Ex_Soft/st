select
	*
from
	dbo.DocJournal docJournal
	join dbo.dhMerchandise dh on dh.id = docJournal.id
	join dbo.drMerchandise dr on dr.idDoc = dh.id
	join dbo.refAttributesValues attributesValues on attributesValues.idElement = dh.id 
	join dbo.refFilePackets filePackets on filePackets.id = attributesValues.idPacket
	join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idPacket = filePackets.id
	join dbo.refFiles files on files.id = filePacketsFiles.idFile
	join dbo.EnumsBase enumsLoadType on enumsLoadType.id = files.idLoadType
where
	docJournal.deleted = 0
	and attributesValues.deleted = 0
	and filePackets.deleted = 0
	and filePacketsFiles.deleted = 0
	and files.deleted = 0

select
	*
from
	dbo.refFiles files
	join dbo.EnumsBase enumsLoadType on enumsLoadType.id = files.idLoadType
	join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idFile = files.id
	join dbo.refFilePackets filePackets on filePackets.id = filePacketsFiles.idPacket
	join dbo.refAttributesValues attributesValues on attributesValues.idPacket = filePackets.id
	join dbo.dhMerchandise dh on dh.id = attributesValues.idElement
	join dbo.DocJournal docJournal on docJournal.id = dh.id
	join dbo.drMerchandise dr on dr.idDoc = dh.id
where
	files.[FileName] = N'simpsons.jpg'

select
	*
from
	dbo.DocJournal docJournal
	join dbo.dhMerchandise dh on dh.id = docJournal.id
	join dbo.drMerchandise dr on dr.idDoc = dh.id
	join dbo.jrnVisits jrnVisits on jrnVisits.id = docJournal.idVisit
	join dbo.refOutlets outlets on outlets.id = jrnVisits.idOutlet
where
	docJournal.PrintDocNum = N'9880000000264459'
	docJournal.deleted = 0

select
	*
	--dj.*,
	--dhM.*
	--drM.*
from
	dbo.DocJournal docJournal
	join dbo.dhMerchandise dh on dh.id = docJournal.id
	join dbo.drMerchandise dr on dr.idDoc = dh.id
	join dbo.enMerchParticipationBase merchParticipationBase on merchParticipationBase.id = dr.idMerchParticipation
	join dbo.refGoods goods on goods.id = dr.idGoods
where
	cast(docJournal.DateTimeStamp as date) = N'20170223'
order by docJournal.idPreviousVersion

-- update dbo.drMerchandise set Price = 9.11 where idDoc = 562954286628397

select
	*
from
	dbo.DocJournal docJournal
	join dbo.dhMerchandise dh on dh.id = docJournal.id
	join dbo.drMerchandise dr on dr.idDoc = dh.id
	join dbo.refAttributesValues attributesValues on attributesValues.idElement in (dh.id, dr.id)
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
	left join dbo.refAttributesListValues attributesListValues on attributesListValues.id = attributesValues.idValue and attributesListValues.deleted = 0
	join dbo.XPObjectType xpObjectType on xpObjectType.OID = attributesValues.ObjectType
where
	cast(docJournal.DateTimeStamp as date) = N'20170223'
	and attributesValues.deleted = 0
	and attributesBase.deleted = 0

/* update dbo.refAttributesValues set idValue = 0 where id = 562954287747800 */

select
	*
from
	dbo.refAttributesValues attributesValues
	join dbo.refAttributesListValues attributesListValues on attributesListValues.id = attributesValues.idValue and attributesListValues.deleted = 0
	join dbo.refFilePackets filePackets on filePackets.id = attributesValues.idValue and attributesListValues.deleted = 0
