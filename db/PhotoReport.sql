declare
	@ProcessDate date = N'20131115'

/* update dbo.DocJournal set DateTimeStamp=N'20131115' where id=562954286516207 */

select
	dj.OpDate as DATE_VISIT,
	pp.Name as ID_TRADER,
	bp.CodeDistr as ID_SELLER,
	oc.outercode as CATEGORY,
	drPr.photoFileName as [FILE_NAME]
from
	dbo.DocJournal dj
	join dbo.drPhotoReport drPr on drPr.idDoc=dj.id
	join dbo.refPhysicalPersons pp on pp.id=dj.idCreator
	join dbo.refBuyPoints bp on bp.id=dj.idBuyPoint
	join dbo.refOutercodes oc on oc.idItem=drPr.idPhotoObject
	join dbo.refDistributors d on d.id=oc.idDistr
where
	cast(dj.DateTimeStamp as date)=@ProcessDate
	and dj.idStatus=1
	and dj.deleted=0
	and oc.TableName=N'refPhotoObjects'
	and d.deleted=0
	and d.IsRoot=1
order by dj.OpDate, pp.Name, bp.CodeDistr

------------------------------------------------------------

select
	*
from
	dbo.DocJournal dj
	join dbo.dhPhotoReport dhPhr on dhPhr.id=dj.id
	join dbo.drPhotoReport drPhr on drPhr.idDoc=dhPhr.id
	join dbo.refPhotoObjects phO on phO.id = drPhr.idPhotoObject
	join dbo.refBuyPoints bp on bp.id = dj.idBuyPoint
where
	--dj.id=562954286516207
	cast(dj.OpDate as date) = N'20140202'

--update dbo.drPhotoReport set photoFileName=N'C:\System Volume Information\TestImageII.jpg' where id=562954286516209

------------------------------------------------------------

declare
	@two48 bigint = power(cast(2 as bigint),cast(48 as bigint))

select
	@two48 as two48,
	cast(drPhR.idDoc/@two48 as bigint) as dNodeID,
	*
from
	dbo.drPhotoReport drPhR
	left join dbo.refPhotoObjects phO on phO.id = drPhR.idPhotoObject and phO.deleted=0
	left join dbo.refDistributors d on d.id=phO.idDistributor and d.deleted=0 and d.NodeID=cast(drPhR.idDoc/@two48 as bigint)-1

select
	*
from
	dbo.refPhotoObjects
where
	deleted=0

------------------------------------------------------------

select * from ConstsBase where Validation is not null
select * from ConstsBase where CodeKey in (N'MaxPhotoSize', N'PhotoExchangeAddress')
select * from SettingsBase where CodeKey = N'PhotoFilesServerPath'

/* update ConstsBase set Deleted=1 where CodeKey = N'PhotoExchangeAddress' */

select
	*
from
	dbo.drPhotoReport drPR
	join dbo.dhPhotoReport dhPR on dhPR.id = drPR.idDoc
	join dbo.DocJournal dj on dj.id = dhPR.id
where
	cast(dj.OpDate as date) = N'20141022'
