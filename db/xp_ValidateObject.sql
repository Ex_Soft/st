USE [chicago_kraft]
GO

/****** Object:  StoredProcedure [dbo].[xp_ValidateObject]    Script Date: 23.01.2015 17:35:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[xp_ValidateObject] 
	  @xml xml
    , @errorcode int OUTPUT --0 - ok, 1 - warning, 2 - error
    , @errortext NVARCHAR(255) OUTPUT
AS
BEGIN
	select @errorcode = 0, @errortext = ''

	if exists (
		SELECT t.rows.value('id[1]','bigint') as [id]
		FROM @xml.nodes('/refBuyPoints') as t(rows)
		)
	begin
		EXEC [dbo].[xp_ValidateObjectBuypointsPeriodicStatuses] 
		  @xml = @xml
		, @errorcode = @errorcode OUTPUT --0 - ok, 1 - warning, 2 - error
		, @errortext = @errortext OUTPUT

		if IsNull(@errorcode, 0) != 0
			return

		EXEC [dbo].[xp_ValidateObjectBuypoints] 
		  @xml = @xml
		, @errorcode = @errorcode OUTPUT --0 - ok, 1 - warning, 2 - error
		, @errortext = @errortext OUTPUT
	end

	if exists (
		SELECT t.rows.value('id[1]','bigint') as [id]
		FROM @xml.nodes('/refPositions') as t(rows)
		)
	EXEC [dbo].[xp_ValidateObjectPositions] 
	  @xml = @xml
    , @errorcode = @errorcode OUTPUT --0 - ok, 1 - warning, 2 - error
    , @errortext = @errortext OUTPUT		
		
END

GO


