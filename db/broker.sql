declare @catName nvarchar(50)

set @catName = db_name()

if (select is_broker_enabled from sys.databases where name = @catName) = 0
begin
 declare @cmd nvarchar(1000)
 
 set @cmd = N'use master;'
 exec (@cmd)
 
 set @cmd = N'alter database [' + @catName + '] set single_user with rollback immediate'
 exec (@cmd)
 
 if exists (select 1 from sys.databases 
    where service_broker_guid = (select service_broker_guid from sys.databases where name = @catName and is_broker_enabled = 0)
    and is_broker_enabled = 1)
 begin
  set @cmd = N'alter database [' + @catName + '] set new_broker'
  exec (@cmd)
 end
 
 set @cmd = N' alter database [' + @catName + '] set enable_broker;'
 exec (@cmd)

 set @cmd = 'alter database [' + @catName + '] set multi_user with rollback immediate'
 exec (@cmd)
 
 set @cmd = N' use [' + @catName + '];'
 exec (@cmd)
 
end