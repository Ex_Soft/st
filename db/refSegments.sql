select
	*
from
	dbo.refSegments segments
where
	segments.deleted = 0

select
	*
from
	dbo.refSegments segments
	join dbo.refSegmentBuyPoints segmentBuyPoints on segmentBuyPoints.idSegment = segments.id
	join dbo.refBuyPoints buyPoints on buyPoints.id = segmentBuyPoints.idBuypoint
where
	segments.id = 562954287571169
	--segments.deleted = 0

/*
update
	dbo.refSegmentBuyPoints
set
	deleted = 1
from
	dbo.refSegments segments
	join dbo.refSegmentBuyPoints segmentBuyPoints on segmentBuyPoints.idSegment = segments.id
where
	segments.id = 562954287571169
*/

select * from dbo.refBuyPoints where id = 562954286578147