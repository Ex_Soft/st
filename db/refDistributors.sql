﻿select
	*
from
	dbo.refDistributors
where
	--ObjectType = 111
	Name like N'S%'
order by NodeID;

--update dbo.refDistributors set Emails = N'123@123.com', IsCanSendEmail = 1 where id = 281474977273617

select
	*
from
	dbo.refDistributors distributors
	join dbo.refScheduleDistributor scheduleDistributor on scheduleDistributor.id = distributors.idScheduleDistributor
	join dbo.EnumsBase enumsScheduleType on enumsScheduleType.id = scheduleDistributor.idScheduleType
	join dbo.refScheduleDistributorOperationTypes scheduleDistributorOperationTypes on scheduleDistributorOperationTypes.idScheduleDistributor = scheduleDistributor.id
	join dbo.enOperationTypesBase operationTypesBase on operationTypesBase.id = scheduleDistributorOperationTypes.idOperationType

/*
declare @id bigint
select @id = dbo.fn_getIdEx(281474977273617, 1)
insert into dbo.refScheduleDistributor (id, idScheduleType, DaysOfWeek, OnceADay, EveryFewHours, NumberOfWeeks, IsOnceADay, TimeBegin, TimeEnd, DateFrom, DateTo, NoUseDateEnd) values (@id, 20140319232331, 0, N'19000101', N'19000101', 1, 0, N'19000101', N'19000101', N'20170706', N'19000101', 1)
update dbo.refDistributors set idScheduleDistributor = @id where id = 281474977273617

;with cte(id, idScheduleDistributor)
as
(
	select id, idScheduleDistributor from dbo.refDistributors where id = 281474977273617
)
insert into dbo.refScheduleDistributorOperationTypes
	(id, idScheduleDistributor, idOperationType, deleted)
select
	dbo.fn_getIdEx(cte.id, 1) as id,
	cte.idScheduleDistributor,
	23 as idOperationType,
	0 as deleted
from
	cte;
*/

select
	*
from
	dbo.refDistributors distributors
	left join dbo.refDistributorsExt distributorsExt on distributorsExt.id = distributors.id
where
	--distributorsExt.id is null
	distributorsExt.id = 0

insert into dbo.refDistributorsExt (id, idNonExclusivePosition) values (281475014533942, 0)

-- https://wiki.systtech.ru/index.php/Dbo.refDistributors

select
	NodeID,
	power(cast(2 as bigint), cast(48 as bigint)) * NodeID as [Begin],
	power(cast(2 as bigint), cast(48 as bigint)) * (NodeID + 1) - 1 as [End],
	*
from
	dbo.refDistributors
where
	deleted = 0
order by 1

select
	*
from
	dbo.refDistributors distributors
	left join dbo.refScheduleDistributor scheduleDistributor on scheduleDistributor.id = distributors.idScheduleDistributor
	left join dbo.refScheduleDistributorOperationTypes scheduleDistributorOperationTypes on scheduleDistributorOperationTypes.idScheduleDistributor = scheduleDistributor.id
where
	distributors.Name = N'ChocolateFood, Kalinigrad'

select
	* /* 281474976710657 */
from
	refDistributors d
where
	(d.deleted=0)
	and (d.IsRoot=1)

/* down */
;with distributorsDown(id, idParent, Name, level, path)
as
(
	select
		distributors.id,
		distributors.idParent,
		distributors.Name,
		0 as level,
		cast(N'/' + distributors.Name as varchar(max)) as path
	from
		dbo.refDistributors distributors
	where
		distributors.id = (select id from dbo.refDistributors distributors where /*distributors.Name = N'ChocolateFood, Kalinigrad'*/ IsRoot = 1 and deleted = 0)
		and distributors.deleted = 0
	union all
	select
		distributors.id,
		distributors.idParent,
		distributors.Name,
		level + 1 as level,
		cast(path + N'/' + distributors.Name as varchar(max)) as path
	from
		dbo.refDistributors distributors
		join distributorsDown distributorsDown on distributors.idParent = distributorsDown.id
	where
		distributors.deleted = 0
)
select
	*
from
	distributorsDown



/* up */
;with distributorsUp(id, idParent, Name, level, path)
as
(
	select
		distributors.id,
		distributors.idParent,
		distributors.Name,
		0 as level,
		cast(N'/' + distributors.Name as varchar(max)) as path
	from
		dbo.refDistributors distributors
	where
		distributors.id = (select id from dbo.refDistributors distributors where distributors.Name = N'9999')
		and distributors.deleted = 0
	union all
	select
		distributors.id,
		distributors.idParent,
		distributors.Name,
		level + 1 as level,
		cast(N'/' + distributors.Name + path as varchar(max)) as path
	from
		dbo.refDistributors distributors
		join distributorsUp distributorsUp on distributors.id = distributorsUp.idParent
	where
		distributors.deleted = 0
)
select
	*
from
	distributorsUp

------------------------------------------------------------

select
	*
from
	dbo.refDistributors distributors
where
	distributors.NodeID = cast(13229323910229482/281474976710656 as smallint)
	--distributors.NodeID = cast(13229323910229482/power(cast(2 as bigint), cast(48 as bigint)) as bigint)

-- join dbo.refDistributors d with (nolock) on dj.id between d.NodeID * 281474976710656 and (d.NodeID + 1) * 281474976710656 - 1

------------------------------------------------------------