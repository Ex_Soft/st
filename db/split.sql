declare
	@enDistributors nvarchar(max) = N'1;2;3'
	
declare @ids table (id bigint)
declare @distributorsCount bigint

insert into @ids
	(id)
select
	cast(word as bigint)
from
	-- dbo.fn_SplitId(@enDistributors, N';')
	-- dbo.fnSplitId(@enDistributors, N';')
	dbo.fn_SplitWord2(@enDistributors, N';')
	
select @distributorsCount = count(*) from @ids

print @distributorsCount
