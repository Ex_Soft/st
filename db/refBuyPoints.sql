﻿select
	*
from
	dbo.refBuyPoints buyPoints
	left join dbo.vBPGridFields vBPGridFields on vBPGridFields.id = buyPoints.idvBPActivity
where
	vBPGridFields.id is null
	or vBPGridFields.PeriodicStatus is null
	or vBPGridFields.IsActivePeriodicActivity  is null


select
	buyers.idDoubleMain as BuyersIdDoubleMain,
	buyers.id as BuyersId,
	buyers.Name as BuyersName,
	buyers.deleted as BuyersDeleted,
	buyPoints.idDoubleMain as BuyPointsIdDoubleMain,
	buyPoints.id as BuyPointsId,
	buyPoints.idMain as BuyPointsIdMain,
	buyPoints.Name as BuyPointsName,
	buyPoints.deleted as BuyPointsDeleted,
	buyPoints.idBuyer as BuyPointsIdBuyer,
	*
from
	ch_old.dbo.refBuyPoints buyPoints
	join ch_old.dbo.refBuyers buyers on buyers.id = buyPoints.idBuyer
	left join ch_old.dbo.LogDataChangeDoubles logDataChangeDoubles on logDataChangeDoubles.idRecord = buyPoints.id
where
	buyPoints.Name in (N'111', N'222', N'333', N'444', N'!но1', N'!но2')
order by buyPoints.idMain, buyPoints.id;

select
	*
from
	ch_old.dbo.LogDataChangeDoubles logDataChangeDoubles
	left join ch_old.dbo.refBuyPoints buyPoints on buyPoints.id = logDataChangeDoubles.idRecord
where
	cast(logDataChangeDoubles.ChangeDate as date) = N'20170922';

select
	*
from
	ch_old.dbo.LogDataChangeDoubles logDataChangeDoubles
	left join ch_old.dbo.refBuyers buyers on buyers.id = logDataChangeDoubles.idRecord
where
	cast(logDataChangeDoubles.ChangeDate as date) = N'20170922';

/*
update ch_old.dbo.refBuyPoints set deleted = 1, idDoubleMain = 281475014317347, idBuyer = 281475014304321 where id = 281475014315332 -- 111
update ch_old.dbo.refBuyPoints set deleted = 1, idDoubleMain = 562954287829074, idBuyer = 562954287824068 where id = 562954287828072 -- 111

update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 281475014317347, idBuyer = 281475014305332 where id = 281475014317347 -- 222
update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 562954287829074, idBuyer = 562954287825069 where id = 562954287829074 -- 222

update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 281475014317362, idBuyer = 281475014305332 where id = 281475014317362 -- 333
update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 562954287829076, idBuyer = 562954287825069 where id = 562954287829076 -- 333

update ch_old.dbo.refBuyPoints set deleted = 1, idDoubleMain = 281475014317362, idBuyer = 281475014308331 where id = 281475014317377 -- 444
update ch_old.dbo.refBuyPoints set deleted = 1, idDoubleMain = 562954287829076, idBuyer = 562954287827071 where id = 562954287829078 -- 444

update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 281475014319370, idBuyer = 281475014305332 where id = 281475014319370 -- !но1
update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 562954287830076, idBuyer = 562954287825069 where id = 562954287830076 -- !но1

update ch_old.dbo.refBuyPoints set deleted = 1, idDoubleMain = 281475014319370, idBuyer = 281475014306329 where id = 281475014319385 -- !но2
update ch_old.dbo.refBuyPoints set deleted = 1, idDoubleMain = 562954287830076, idBuyer = 562954287826070 where id = 562954287830078 -- !но2

update ch_old.dbo.refBuyers set deleted = 1, idDoubleMain = 281475014305332 where id = 281475014304321 -- !1
update ch_old.dbo.refBuyers set deleted = 1, idDoubleMain = 562954287825069 where id = 562954287824068 -- !1

update ch_old.dbo.refBuyers set deleted = 0, idDoubleMain = 281475014305332 where id = 281475014305332 -- !2
update ch_old.dbo.refBuyers set deleted = 0, idDoubleMain = 562954287825069 where id = 562954287825069 -- !2

update ch_old.dbo.refBuyers set deleted = 1, idDoubleMain = 281475014305332 where id = 281475014306329 -- !3
update ch_old.dbo.refBuyers set deleted = 1, idDoubleMain = 562954287825069 where id = 562954287826070 -- !3

update ch_old.dbo.refBuyers set deleted = 1, idDoubleMain = 281475014305332 where id = 281475014308331 -- !4
update ch_old.dbo.refBuyers set deleted = 1, idDoubleMain = 562954287825069 where id = 562954287827071 -- !4

update ch_old.dbo.LogDataChangeDoubles set deleted = 0 where id = 281475014333470
*/

/*
update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 281475014315332 where id = 281475014315332
update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 562954287828072 where id = 562954287828072

update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 281475014317347 where id = 281475014317347
update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 562954287829074 where id = 562954287829074

update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 281475014317362 where id = 281475014317362
update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 562954287829076 where id = 562954287829076

update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 281475014317377 where id = 281475014317377
update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 562954287829078 where id = 562954287829078
--
update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 281475014319370 where id = 281475014319370
update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 562954287830076 where id = 562954287830076

update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 281475014319385 where id = 281475014319385
update ch_old.dbo.refBuyPoints set deleted = 0, idDoubleMain = 562954287830078 where id = 562954287830078
*/

/*
update ch_old.dbo.refBuyers set deleted = 0, idDoubleMain = 281475014304321 where id = 281475014304321
update ch_old.dbo.refBuyers set deleted = 0, idDoubleMain = 562954287824068 where id = 562954287824068

update ch_old.dbo.refBuyers set deleted = 0, idDoubleMain = 281475014305332 where id = 281475014305332
update ch_old.dbo.refBuyers set deleted = 0, idDoubleMain = 562954287825069 where id = 562954287825069

update ch_old.dbo.refBuyers set deleted = 0, idDoubleMain = 281475014306329 where id = 281475014306329
update ch_old.dbo.refBuyers set deleted = 0, idDoubleMain = 562954287826070 where id = 562954287826070

update ch_old.dbo.refBuyers set deleted = 0, idDoubleMain = 281475014308331 where id = 281475014308331
update ch_old.dbo.refBuyers set deleted = 0, idDoubleMain = 562954287827071 where id = 562954287827071
*/

--delete from ch_old.dbo.LogDataChangeDoubles where cast(logDataChangeDoubles.ChangeDate as date) = N'20170922'

--update dbo.refBuyPoints set UrName = N'test trigger# 1' where idMain = 281474992204624

select
	*
from
	dbo.refBuyPoints buyPoints
where
	--buyPoints.id = 562954287838088
	buyPoints.UrName like N'test trigger'
order by buyPoints.idDistributor, buyPoints.idMain, buyPoints.id

select
	*
from
	dbo.refBuyPoints outlets
	left join dbo.refPropertiesHistory propertiesHistory on propertiesHistory.idItem = outlets.idNetwork
where
	outlets.Name = N'Что-то задублировались записи в refPropertiesHistory 2'

;with cte(id, idMain)
as
(
select
	id,
	idMain
from
	dbo.refBuyPoints buyPoints
where
	buyPoints.id != buyPoints.idMain
	and buyPoints.deleted = 0
)
select
	*
from
	dbo.refAttributesValues attributesValuesD
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValuesD.idAttribute
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join cte cte on cte.id = attributesValuesD.idElement
	left join dbo.refAttributesValues attributesValuesCO on attributesValuesCO.idElement = cte.idMain and attributesValuesCO.idAttribute =  attributesValuesD.idAttribute and attributesValuesCO.StartDate = attributesValuesD.StartDate
where
	attributesValuesD.deleted = 0
	and attributesBase.deleted = 0
	and enumsAttrType.CodeKey = N'Attrs_Type_Periodic'
	and attributesValuesCO.id is null
	--and attributesValuesD.StartDate >= N'20160701'

select * from dbo.refAttributesValues where id = 562954287070649

select
	*
from
	dbo.refPeriodicStatuses periodicStatuses
	join dbo.EnumsBase enumsType on enumsType.id = periodicStatuses.IdType

select
	--attributesValues.*
	*
from
	dbo.refBuyPoints buyPoints
	join dbo.refAttributesValues attributesValues on attributesValues.idElement = buyPoints.id
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
where
	--buyPoints.idTenant = 0
	--buyPoints.CodeBP = N'031886'
	--buyPoints.UrName like N'BP 4%'
	buyPoints.idMain = 281475001449927
	--and buyPoints.deleted = 0
	and attributesValues.deleted = 0
	--and enumsAttrType.CodeKey = N'Attrs_Type_Periodic'
order by attributesValues.idElement, attributesValues.idAttribute, attributesValues.StartDate

--update dbo.refAttributesValues set StartDate = N'20160701' where id in (281475002820042, 562954287130661)
--insert into dbo.refAttributesValues (id, idElement, idAttribute, ObjectType, Startdate, Value, idValue, deleted) values (dbo.fn_GetIdEx(281474976710657, 1), 281475001449912, 126948, 10364, N'20160718', N'', 1, 0)
--insert into dbo.refAttributesValues (id, idElement, idAttribute, ObjectType, Startdate, Value, idValue, deleted) values (dbo.fn_GetIdEx(281474976711134, 1), 562954286900615, 126948, 10364, N'20160718', N'', 1, 0)
/*
update dbo.refAttributesValues set deleted = 0 where idElement in (281475002820041, 562954287130660) and idAttribute = 126948 and ObjectType = 10364 and StartDate = N'20160701'
delete from dbo.refAttributesValues where idElement in (281475002820041, 562954287130660) and idAttribute = 126948 and ObjectType = 10364 and StartDate > N'20160901'

delete from dbo.refAttributesValues where id = 562954287250675
update dbo.refAttributesValues set idValue = 2 where id = 281475003140082

update dbo.refAttributesValues set deleted = 0 where id in (281475003140082, 562954287250674)
update dbo.refAttributesValues set deleted = 1 where id in (281475003140076, 562954287250673)
*/
--update dbo.refBuyPoints set UrName = N'BP 4 test duplicates by activity (D) (CO)' where id = 281475001449912
--update dbo.refBuyPoints set UrName = N'BP 4 test duplicates by activity (D) (D)' where id = 562954286900615

select
	mbp.id,
	count(mbp.id)
from
	dbo.refBuyPoints mbp
	left join dbo.snsAttributesValues av on mbp.id = av.idElement
group by mbp.id
having count(mbp.id) > 1

select
	*
from
	dbo.vBPGridFields
where
	id in (281475094747872, 27866022694421746, 307370674568070396)
	or idMain in (281475094747872, 27866022694421746, 307370674568070396)

select
	id,
	count(id)
from
	dbo.vBPGridFields
group by id
having count(id) > 1

select
	--bpd.UrName,
	--bp.UrName,
	bpd.idMain,
	bpd.idDoubleMain,
	*
from
	dbo.refBuyPoints bpd
	join dbo.refBuyPoints bp on bp.id = bpd.idMain
where
	--bpd.deleted = 0
	--and bpd.idDistributor = 281474976711134
	bpd.UrName like N'62374 check_refRouteTerritory_doubles%'
	--and bpd.UrName = N'62374 check_refRouteTerritory_doubles'
	--and bpd.UrName like N'BP #1 (999)%'
order by bpd.idMain, bpd.id

--update dbo.refBuyPoints set UrName = N'62374 check_refRouteTerritory_doubles' where id in (281475013718302, 14355223812267578, 281475013718328, 2251799820265581, 281475013718346, 562954287804990)
--update dbo.refBuyPoints set UrName = N'62374 check_refRouteTerritory_doubles 002 #1' where id in (281475013718346, 562954287804990)
--update dbo.refBuyPoints set UrName = N'62374 check_refRouteTerritory_doubles 003' where id in (281475013718328, 2251799820265581)
--update dbo.refBuyPoints set UrName = N'62374 check_refRouteTerritory_doubles 999' where id in (281475013718302, 14355223812267578)

select
	*
from
	dbo.refBuyPoints buyPoints
where
	--buyPoints.UrName = N'Test xp_OnRefChanged (CO)'
	--buyPoints.deleted = 0
	buyPoints.CodeBP = N'0020000000001048'
	--and buyPoints.CodeBP like N'%(ЦО)%'
	--and 
	buyPoints.UrName like N'%more%'
	--and buyPoints.IdTenant = 0
	--and buyPoints.isLocal = 1
	and buyPoints.idMain in (281475012107822, 281475012107807)
order by buyPoints.UrName, buyPoints.id

/*
update dbo.refBuyPoints set UrName = N'Test Reload #1 (CO)' where id in (562954287650327, 281475011737067)
update dbo.refBuyPoints set CodeDistr = N'_AB0000000000001 (D)' where id in (281474992795821, 18858823439614956)
update dbo.refBuyPoints set CreditLimit = 100 where id in (281475011739057, 562954287652331)
update dbo.refBuyPoints set CodeBP = N'0020000000001091' where id in (2251799820262573)
update dbo.refBuyPoints set deleted = 1 where id in (281475012107822, 562954287681848, 2251799820262575, 281475012107807, 562954287681846, 2251799820262573)
update dbo.refBuyPoints set idBuypointType = 0 where id in (281475012107822, 562954287681848, 2251799820262575, 281475012107807, 562954287681846, 2251799820262573) /* 281474988442780 */
update dbo.refBuyPoints set idBuypointType = 281474988442780 where id in (281475012107822, 281475012107807)
update dbo.refBuyPoints set idBuypointType = 281474988442780 where id in (562954287681848, 562954287681846)
update dbo.refBuyPoints set idMain = 281475012107807 where id = 2251799820262573
*/

;with cte(id, Name, idParent)
as
(
	select
		d.id,
		d.Name,
		d.idParent
	from
		dbo.refDistributors d
	where
		d.id=281474976711134
		and d.deleted=0
	union all
	select
		d.id,
		d.Name,
		d.idParent
	from
		dbo.refDistributors d
		join cte cte on d.idParent=cte.id
	where
		d.deleted=0
)
select
	cte.id,
	cte.Name,
	bp.id,
	bp.idMain,
	bp.Name,
	bp.CodeBP
from
	cte
	join dbo.refBuyPoints bp on bp.idDistributor=cte.id
where
	cte.id=281474977164328
	and bp.deleted=0
order by cte.id, bp.idMain

select * from dbo.refBuyPoints bp where bp.CodeBP=N'031886'

select
	*
from
	dbo.refBuyPoints bp
where
	bp.UrName like N'___!_0%' escape N'!'
order by bp.idMain, bp.idDistributor

------------------------------------------------------------

select
	*
from
	dbo.refBuyPoints buyPoints
where
	buyPoints.idMain in (281474996589751, 281474996669540)
	--buyPoints.UrName like N'ТТ КБ %'
order by buyPoints.idTenant, buyPoints.id

select
	*
from
	dbo.refBuyPointMatches bpm
where
	bpm.idVendorBuypoint in (281474992362418, 562954286579146, 281474992362434, 562954286579149)
	or bpm.idDistributorBuypoint in (281474992362418, 562954286579146, 281474992362434, 562954286579149)

select
	*
from
	dbo.refBuyPoints
where
	--id = 562954286579146
	--UrName = N'ТТ КБ Д (1)'
	codebp = N'0020000000001093'
order by idTenant, idMain, id

select
	buyPointsD.id,
	buyPointsD.idTenant,
	buyPointsCO.id,
	buyPointsCO.idTenant,
	*
from
	dbo.refBuyPoints buyPointsD
	join dbo.refBuyPointMatches buyPointMatches on buyPointMatches.idDistributorBuypoint = buyPointsD.id
	join dbo.refBuyPoints buyPointsCO on buyPointsCO.id = buyPointMatches.idVendorBuypoint
where
	buyPointsD.id = 281474996589751
	buyPointsD.deleted = 0
	and buyPointMatches.deleted = 0
	and buyPointsCO.deleted = 0
	and buyPointsD.idTenant = 0

select
	buyPointsCO.idMain,
	buyPointsD.idMain,
	*
from
	dbo.refBuyPoints buyPointsCO
	join dbo.refBuyPointMatches buyPointMatches on buyPointMatches.idVendorBuypoint = buyPointsCO.id
	join dbo.refBuyPoints buyPointsD on buyPointsD.id = buyPointMatches.idDistributorBuypoint
where
	buyPointsCO.id = 281474996589751
	buyPointsCO.deleted = 0
	and buyPointMatches.deleted = 0
	and buyPointsD.deleted = 0

-- update dbo.refBuyPointMatches set deleted = 1 where id = 281474992807822

select * from dbo.vBPGridFields where id in (281474992362434, 281474992362418, 281474992808870)

