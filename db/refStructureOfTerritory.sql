select
	*
from
	dbo.refTerritoryStructure territoryStructure
	left join dbo.refTerritoryLevelTypes territoryLevelTypes on territoryLevelTypes.id = territoryStructure.idterritoryLevelType
	left join dbo.refTerritoryStructurePositions territoryStructurePositions on territoryStructurePositions.idTerritoryStructure = territoryStructure.id
	left join dbo.refTerritoryStructureDistributors territoryStructureDistributors on territoryStructureDistributors.idTerritoryStructure = territoryStructure.id
	left join dbo.refTerritoryStructureOutlets territoryStructureOutlets on territoryStructureOutlets.idTerritoryStructure = territoryStructure.id
	left join dbo.refTerritoryStructureAddress territoryStructureAddress on territoryStructureAddress.idTerritoryStructure = territoryStructure.id
	left join dbo.refPositions positions on positions.id = territoryStructurePositions.idPosition
	left join dbo.refDistributors distributors on distributors.id = territoryStructureDistributors.idDistributor
	left join dbo.refBuyPoints buyPoints on buyPoints.id = territoryStructureOutlets.idOutlet
	left join dbo.refAddressClassifier addressClassifier on addressClassifier.id = territoryStructureAddress.idAddressClassifier
where
	territoryStructure.deleted = 0

/* update dbo.refTerritoryLevelTypes set Name = N'Level# I' where Code = N'1' */
/* update dbo.refTerritoryStructure set Code = N'' where id = 0 */
/* update dbo.refTerritoryStructure set Name = N'Element# I' where Code = N'1' */

/*
select * from dbo.refTerritoryLevelTypes

insert into dbo.refTerritoryLevelTypes (id, Code, Name, deleted) values (dbo.fn_getIdEx(281474976710657, 1), N'1', N'Level# I', 0)
insert into dbo.refTerritoryLevelTypes (id, Code, Name, deleted) values (dbo.fn_getIdEx(281474976710657, 1), N'2', N'Level# II', 0)
insert into dbo.refTerritoryLevelTypes (id, Code, Name, deleted) values (dbo.fn_getIdEx(281474976710657, 1), N'3', N'Level# III', 0)
insert into dbo.refTerritoryLevelTypes (id, Code, Name, deleted) values (dbo.fn_getIdEx(281474976710657, 1), N'4', N'Level# IV', 0)
*/

/*
select * from dbo.refTerritoryStructure

insert into dbo.refTerritoryStructure
	(id, idParent, Code, Name, idTerritoryLevelType, MaterializedPath, deleted)
select
	dbo.fn_getIdEx(281474976710657, 1) as id,
	0 as idParent,
	N'1' as Code,
	N'1' as Name,
	id as idTerritoryLevelType,
	N'' as MaterializedPath,
	0 as deleted
from
	dbo.refTerritoryLevelTypes
where
	Code = N'1'
	and deleted = 0

insert into dbo.refTerritoryStructure
	(id, idParent, Code, Name, idTerritoryLevelType, MaterializedPath, deleted)
select
	dbo.fn_getIdEx(281474976710657, 1) as id,
	(select id from dbo.refTerritoryStructure where Code = N'1' and deleted = 0) as idParent,
	N'1.1' as Code,
	N'1.1' as Name,
	id as idTerritoryLevelType,
	N'' as MaterializedPath,
	0 as deleted
from
	dbo.refTerritoryLevelTypes
where
	Code = N'2'
	and deleted = 0

insert into dbo.refTerritoryStructure
	(id, idParent, Code, Name, idTerritoryLevelType, MaterializedPath, deleted)
select
	dbo.fn_getIdEx(281474976710657, 1) as id,
	(select id from dbo.refTerritoryStructure where Code = N'1.1' and deleted = 0) as idParent,
	N'1.1.1' as Code,
	N'1.1.1' as Name,
	id as idTerritoryLevelType,
	N'' as MaterializedPath,
	0 as deleted
from
	dbo.refTerritoryLevelTypes
where
	Code = N'3'
	and deleted = 0

insert into dbo.refTerritoryStructure
	(id, idParent, Code, Name, idTerritoryLevelType, MaterializedPath, deleted)
select
	dbo.fn_getIdEx(281474976710657, 1) as id,
	(select id from dbo.refTerritoryStructure where Code = N'1.1.1' and deleted = 0) as idParent,
	N'1.1.1.1' as Code,
	N'1.1.1.1' as Name,
	id as idTerritoryLevelType,
	N'' as MaterializedPath,
	0 as deleted
from
	dbo.refTerritoryLevelTypes
where
	Code = N'4'
	and deleted = 0
*/

/*
insert into dbo.refTerritoryStructurePositions
	(id, idterritoryStructure, idPosition, deleted)
select
	dbo.fn_getIdEx(281474976710657, 1) as id,
	id as idterritoryStructure,
	(select id from dbo.refPositions where idParent = 0 and deleted = 0) as idPosition,
	0 as deleted
from
	dbo.refTerritoryStructure
where
	Code = N'1'
	and deleted = 0
*/

/*
insert into dbo.refTerritoryStructureDistributors
	(id, idterritoryStructure, idDistributor, deleted)
select
	dbo.fn_getIdEx(281474976710657, 1) as id,
	id as idterritoryStructure,
	(select id from dbo.refDistributors where NodeID = 1 and deleted = 0) as idDistributor,
	0 as deleted
from
	dbo.refTerritoryStructure
where
	Code = N'1'
	and deleted = 0
*/

/*
insert into dbo.refTerritoryStructureOutlets
	(id, idterritoryStructure, idOutlet, deleted)
select
	dbo.fn_getIdEx(281474976710657, 1) as id,
	id as idterritoryStructure,
	(select id from dbo.refBuyPoints where id = 281474992362434) as idOutlet,
	0 as deleted
from
	dbo.refTerritoryStructure
where
	Code = N'1'
	and deleted = 0
*/

/*
insert into dbo.refTerritoryStructureAddress
	(id, idterritoryStructure, idAddressClassifier, deleted)
select
	dbo.fn_getIdEx(281474976710657, 1) as id,
	id as idterritoryStructure,
	(select id from dbo.refAddressClassifier where idLevel = 1 and deleted = 0) as idAddressClassifier,
	0 as deleted
from
	dbo.refTerritoryStructure
where
	Code = N'1'
	and deleted = 0
*/


/* update dbo.refAddressClassifier set idterritoryStructure = 281475013005576 where id = 281474977384290 */
/* update dbo.refAddressClassifier set idterritoryStructure = 0 where idterritoryStructure != 0 */

;with rq (idParent, Path)
as
(
	select
		t.idParent,
		cast(t.Name as nvarchar(max)) as Path
	from
		dbo.refTerritoryStructure t with (nolock)
	where
		t.id = 281475013007574
	union all
	select
		t.idParent,
		cast(t.Name + N'/' + rq.Path as nvarchar(max)) as Path
	from
		rq rq
		join dbo.refTerritoryStructure t with (nolock) on t.id = rq.idParent
	where
		t.deleted = 0
)
select
	*
from
	rq rq
where
	rq.idParent = 0;

select dbo.fn_GetMaterializedPath_refTerritoryStructure(281475013007574)

;with rq (id, idParent)
as
(
	select
		t.id,
		t.idParent
	from
		dbo.refTerritoryStructure t with (nolock)
	where
		t.id = 281475013006573
	union all
	select
		t.id,
		t.idParent
	from
		rq rq
		join dbo.refTerritoryStructure t with (nolock) on t.idParent = rq.id
	where
		t.deleted = 0
)
select
	*
from
	rq rq

select * from dbo.fn_GetChildren_refTerritoryStructure(281475013006573)
