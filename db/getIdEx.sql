﻿declare
	@tablenames nvarchar(4000) = N'refDistributors',
	@lastverstamps nvarchar(4000)

exec dbo.st_sp_get_lasverstamps @tablenames = @tablenames, @lastverstamps = @lastverstamps out

print @lastverstamps

declare
	@result bigint,
	@idDistributor bigint = 281474976710657,
	@poolSize bigint = 1

select @result = dbo.fn_getIdEx(@idDistributor, @poolSize)
print @result
