﻿if object_id(N'st_fn_get_filedata', N'fs') is not null
	drop function st_fn_get_filedata
go

if object_id(N'st_fn_create_dir', N'fs') is not null
	drop function st_fn_create_dir
go

if object_id(N'st_sp_get_lasverstamps', N'pc') is not null
	drop procedure st_sp_get_lasverstamps
go

if object_id(N'st_sp_set_lasverstamp', N'pc') is not null
	drop procedure st_sp_set_lasverstamp
go

if object_id(N'st_sp_get_lasverstamp', N'pc') is not null
	drop procedure st_sp_get_lasverstamp
go

if object_id(N'ConcatKeyValueAsc', N'af') is not null
	drop aggregate ConcatKeyValueAsc
go

if object_id(N'ConcatenateSemicolonDelimiter', N'af') is not null
	drop aggregate ConcatenateSemicolonDelimiter
go

if object_id(N'ConcatenateDelimiter', N'af') is not null
	drop aggregate ConcatenateDelimiter
go

if object_id(N'st_fn_exist_file', N'fs') is not null
	drop function st_fn_exist_file
go

if object_id(N'st_sp_get_dbver', N'pc') is not null
	drop procedure st_sp_get_dbver
go

if object_id(N'st_fn_get_dbver', N'fs') is not null
	drop function st_fn_get_dbver
go

if object_id(N'st_fn_put_ctlg', N'fs') is not null
	drop function st_fn_put_ctlg
go

if object_id(N'st_fn_chk_cfg', N'fs') is not null
	drop function st_fn_chk_cfg
go

if object_id(N'st_fn_put_cfg', N'fs') is not null
	drop function st_fn_put_cfg
go

if object_id(N'st_sp_imp_db', N'pc') is not null
	drop procedure st_sp_imp_db
go

if object_id(N'st_sp_get_db', N'pc') is not null
	drop procedure st_sp_get_db
go

if object_id(N'st_sp_put_db', N'pc') is not null
	drop procedure st_sp_put_db
go

if object_id(N'ReInit', N'pc') is not null
	drop procedure ReInit
go

if object_id(N'fn_getIdEx', N'fs') is not null
	drop function fn_getIdEx
go

if object_id(N'getIdEx', N'pc') is not null
	drop procedure getIdEx
go

if exists (select 1 from sys.assemblies asms where asms.name = N'ass_base' and is_user_defined = 1)
	drop assembly ass_base
go

create assembly ass_base authorization dbo from 'd:\WorkSpaces\CHICAGO\DEV\db\src\ass_base\ass_base\bin\Debug\ass_base.dll' with permission_set = unsafe
go

create procedure dbo.getIdEx (@idDistributor bigint, @id bigint output, @poolSize bigint) as 
external name ass_base.StoredProcedures.getIdEx 
go

create function dbo.fn_getIdEx (@idDistributor bigint, @poolSize bigint = 1) returns bigint as 
external name ass_base.StoredProcedures.fn_getIdEx
go

create procedure dbo.ReInit (@idDistributor bigint) as
external name ass_base.StoredProcedures.ReInit
go

create procedure [st_sp_put_db] @query nvarchar(max), @filename nvarchar(256), @ver [bigint] as
external name [ass_base].[StoredProcedures].[st_sp_put_db]
go

create procedure [st_sp_get_db] @filename nvarchar(256)	as
external name [ass_base].[StoredProcedures].[st_sp_get_db]
go

create procedure [st_sp_imp_db] @tablename sysname, @filename  nvarchar(256) as
external name [ass_base].[StoredProcedures].[st_sp_imp_db]
go

create function [st_fn_put_cfg] (@idConfig bigint, @FolderOut nvarchar(256)) returns bit as
external name [ass_base].[UserDefinedFunctions].[st_fn_put_cfg]
go

create function [st_fn_chk_cfg] (@idConfig bigint, @FolderIn nvarchar(256)) returns bit as
external name [ass_base].[UserDefinedFunctions].[st_fn_chk_cfg]
go

create function [st_fn_put_ctlg] (@idRoute bigint, @FolderOut nvarchar(256)) returns bit as
external name [ass_base].[UserDefinedFunctions].[st_fn_put_ctlg]
go

create function [st_fn_get_dbver] (@filename nvarchar(256)) returns bigint as
external name [ass_base].[UserDefinedFunctions].[st_fn_get_dbver]
go

create procedure [st_sp_get_dbver] @filename  nvarchar(256) as
external name [ass_base].[StoredProcedures].[st_sp_get_dbver]
go

create function [st_fn_exist_file] (@filename nvarchar(256)) returns bit as
external name [ass_base].[UserDefinedFunctions].[st_fn_exist_file]
go

create aggregate [dbo].[ConcatenateDelimiter] (@value [nvarchar](4000)) returns[nvarchar](4000)
external name [ass_base].[ConcatenateDelimiter]
go

create aggregate [dbo].[ConcatenateSemicolonDelimiter] (@value [nvarchar](4000)) returns[nvarchar](4000)
external name [ass_base].[ConcatenateSemicolonDelimiter]
go

create aggregate [dbo].[ConcatKeyValueAsc] (@value [nvarchar](4000)) returns[nvarchar](4000)
external name [ass_base].[ConcatKeyValueAsc]
go

create procedure [st_sp_get_lasverstamp] @tablename  nvarchar(128), @lastverstamp bigint output as
external name [ass_base].[StoredProcedures].[st_sp_get_lasverstamp]
go

create procedure [st_sp_get_lasverstamps] @tablenames nvarchar(4000), @lastverstamps nvarchar(4000) output as
external name [ass_base].[StoredProcedures].[st_sp_get_lasverstamps]
go

create procedure [st_sp_set_lasverstamp] @tablename  nvarchar(128), @lastverstamp bigint as
external name [ass_base].[StoredProcedures].[st_sp_set_lasverstamp]
go

create function [st_fn_create_dir] (@dir nvarchar(max)) returns bit as
external name [ass_base].[UserDefinedFunctions].[st_fn_create_dir]
go

create function [st_fn_get_filedata] (@filepath nvarchar(max)) returns varbinary(max) as
external name [ass_base].[UserDefinedFunctions].[st_fn_get_filedata]
go
