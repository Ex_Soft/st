select
	dh.id as idDh,
	dh.DiscountPromoRate as dhDiscountPromoRate,
	dh.DiscountPromoAmount as dhDiscountPromoAmount,
	dr.id as idDr,
	dr.DiscountPromoRate as drDiscountPromoRate,
	dr.DiscountPromoAmount as drDiscountPromoAmount,
	*
from
	dbo.DocJournal dj
	join dbo.dhPreOrder dh on dh.id = dj.id
	join dbo.drPreOrder dr on dr.idDoc = dh.id
where
	--dj.PrintDocNum = N'0020000000008486'
	dj.idFirstVersion = 562954287946547

/*
update dbo.dhPreOrder set DiscountPromoRate = 13, DiscountPromoAmount = 13 where id in (562954287946547, 562954287947547)
update dbo.drPreOrder set DiscountPromoRate = 13, DiscountPromoAmount = 13 where id in (562954287946548, 562954287947548)
*/

select
	RecommendedQuantity,
	Quantity,
	UnitFactor,
	*
from
	dbo.drPreOrder drPreOrder
	join dbo.dhPreOrder dhPreOrder on dhPreOrder.id = drPreOrder.idDoc
	join dbo.DocJournal docJournal on docJournal.id = dhPreOrder.id
	join dbo.refDistributors d on d.NodeID = cast(drPreOrder.idDoc/281474976710656 as smallint)
where
	drPreOrder.idDoc = 13229323910943122

select
	*
from
	dbo.DocJournal dj
	join dbo.dhPreOrder dh on dh.id = dj.id
	join dbo.drPreOrder dr on dr.idDoc = dh.id
	join dbo.refDistributors d on d.NodeID = cast(dj.id/281474976710656 as smallint)
where
	dj.PrintDocNum = N'0020000000008486'
	--dj.id = 41940892519854262
	and d.deleted=0

-- update dbo.dhPreOrder set idAgreement = 562954286740528 where id = 562954286740526

select
	*
from
	dbo.DocJournal dj
	join dbo.dhPreOrder dh on dh.id = dj.id
	join dbo.drPreOrder dr on dr.idDoc = dh.id
	left join dbo.refOutercodes oc on idItem = dj.id
where
	cast(dj.OpDate as date) = N'20151005'

select * from dbo.drPreOrder where idItemType is null

select
	*
from
	dbo.DocJournal dj
where
	dj.deleted = 0
order by dj.OpDate desc

select * from dbo.enOperationTypesBase where id=23

/*

declare
	@id bigint = 18858823439617958

select * from dbo.DocJournal where id = @id
select * from dbo.dhPreOrder where id = @id
select * from dbo.drPreOrder where idDoc = @id
select * from dbo.refOutercodes where idItem = @id

delete
	dbo.drPreOrder
from
	dbo.drPreOrder dr
	join dbo.dhPreOrder dh on dr.idDoc = dh.id
where
	dh.id = @id

delete
	dhPreOrder
from
	dhPreOrder dh
	join dbo.DocJournal dj on dh.id = dj.id
where
	dj.id = @id

delete from dbo.DocJournal where id = @id

select * from dbo.DocJournal where id = @id
select * from dbo.dhPreOrder where id = @id
select * from dbo.drPreOrder where idDoc = @id
select * from dbo.refOutercodes where idItem = @id

*/

------------------------------------------------------------

select
	*
from
	dbo.DocJournal dj
	left join dbo.refOutercodes ocdj on ocdj.idItem = dj.id
where
	dj.id in (562954286557720, 562954286561746)

declare
	--@id bigint = 562954286557720,
	@id bigint = 562954286561746,
	--@outercode nvarchar(50) = N'blah-blah-blah',
	--@outercode nvarchar(50) = N'outercode 4 0020000000000070',
	@outercode nvarchar(50) = N'outercode 4 0020000000000112',
	@tableName nvarchar(50) = N'DocJournal'

;merge into dbo.refOutercodes as tgt
using
(
	select
		dj.id,
		d.id as idDistr,
		(select OID from dbo.XPObjectType where TypeName = N'Chicago2.Core.xpoobjects.References.OutercodeDocJournal') as OID
	from
		dbo.DocJournal dj
		join dbo.refDistributors d on d.NodeID = cast(dj.id/281474976710656 as smallint)
	where
		dj.id = @id
		and d.deleted=0
) as src
on tgt.idItem = src.id and tgt.idDistr = src.idDistr and tgt.TableName = @tableName and tgt.ObjectType = src.OID
when matched and tgt.outercode != @outercode
	then
		update
			set
				outercode = @outercode
when not matched
	then
		insert
			(id, idDistr, idItem, TableName, outercode, ObjectType)
		values
			(dbo.fn_getIdEx(src.idDistr, 1), src.idDistr, src.id, @tableName, @outercode, src.OID);

select
	*
from
	dbo.DocJournal dj
	left join dbo.refOutercodes ocdj on ocdj.idItem = dj.id
where
	dj.id in (562954286557720, 562954286561746)
