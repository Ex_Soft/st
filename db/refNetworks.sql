select
	*
from
	dbo.refNetworks networks
	join dbo.refBuyPoints buyPoints on buyPoints.idNetwork = networks.id
where
	networks.deleted = 0
	and networks.NetworkCode = N'16070401'
	and buyPoints.deleted = 0

--update dbo.refNetworks set NetworkCode = N'16070401', Name = N'16070401' where id = 562954286790531

select
	*
from
	dbo.refNetworks networks
	join dbo.refAttributesValues attributesValues on attributesValues.idElement = networks.id
where
	networks.deleted = 0
	and networks.NetworkCode in (N'22222', N'55554545')

select
	id,
	count(id) as [count]
from
	dbo.vNetworks
group by id
having count(id) > 1