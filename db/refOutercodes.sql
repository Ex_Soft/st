-- https://wiki.systtech.ru/index.php/Dbo.refOutercodes

select
	*
from
	dbo.refOutercodes outercodes
	join dbo.refOutlets outlets on outlets.id = outercodes.idItem
where
	outercodes.outercode = N'2017091401'

select
	*
from
	refOutercodes oc
	join XPObjectType ot on (ot.OID=oc.ObjectType)
	join refPositions p on (p.id=oc.idItem)
	join refDistributors d on (d.id=oc.idDistr)
where
	(oc.TableName=N'refPositions')
	and (ot.TypeName=N'Chicago2.Core.xpoobjects.References.OutercodePositions')
	and (d.deleted=0)
	and (d.IsRoot=1)

------------------------------------------------------------

select
	*
from
	dbo.refOutercodes oc
	left join dbo.XPObjectType ot on ot.OID=oc.ObjectType
where
	oc.outercode like N'out%'

--update dbo.refOutercodes set ObjectType=13102508 where id=562954286528216
