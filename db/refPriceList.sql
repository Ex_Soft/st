﻿/*
delete
	dbo.refPriceList
from
	dbo.refPriceList priceList
	join dbo.refGoods goods on goods.id = priceList.idGoods
where
	goods.FullName like N'62849%'
*/

/*
update
	dbo.refPriceList
set
	Price = 1
from
	dbo.refPriceList priceList
	join dbo.refGoods goods on goods.id = priceList.idGoods
where
	goods.FullName like N'67144%'
*/

select
	*
from
	dbo.refPriceList priceList
	join dbo.refGoods goods on goods.id = priceList.idGoods
	join dbo.refUnits units on units.id = priceList.idUnit
where
	goods.FullName like N'67144%'

select
	*
from
	dbo.refPriceList priceList
	join dbo.refPriceTypes priceTypes on priceTypes.id = priceList.idPriceType
	join dbo.refPayTypes payTypes on payTypes.id = priceList.idPayType
	join dbo.EnumsBase enumsPayKind on enumsPayKind.id = payTypes.idPayKind
	join dbo.refGoods goods on goods.id = priceList.idGoods
where
	goods.FullName like N'67144%'

select
	*
from
	dbo.refPriceList priceList
	join dbo.refGoods goods on goods.id = priceList.idGoods
	join dbo.refDistributors distributors on distributors.id = goods.idDistributor
where
	priceList.deleted = 0
	--and goods.id = 281475013268865
	and goods.id in (281475013268931, 281475013268946, 562954287753817, 562954287753820)
	--and goods.Article = N'07622210171986'
	and goods.deleted = 0
	and distributors.Name = N'Jacobs RUS'
	and distributors.deleted = 0

select
	*
from
	dbo.refGoods goods
where
	--goods.FullName = N'62849 В прайс-листе для товара Д записи создались в контексте ЦО'
	goods.FullName like N'62849%' or goods.FullName like N'Свой товар%'
	--goods.idGoodsCO = 281475013268865
	--goods.id = 281474976714350
