https://errors-db.mtproject.ru/LogText.svc/GetLogTextById?error=460d1b8b-f30a-4e68-8681-b9efdff60780

select * from SQLCLUSTER.chicago_kraft.dbo.v_LogDataChange where TableName = N'refWorkSchedulePeriods' and ChangeDate between N'20141105' and N'20141106'
select * from chicago_kraft.dbo.LogSystemPerformance where StartOperation between '20150112 15:55:00' and '20150112 16:00:00' and TimeOperation > '19000101 00:02' https://cp.systtech.ru/company/personal/user/1909/blog/1386/

select * from dbo.DocJournal where id = 14662361218697372
select * from dbo.dhSales where id = 14636699535059010
select * from dbo.refOuterCodes where id = 14636699535059019
select * from dbo.drSales where id in (14636699535059011, 14636699535059012, 14636699535059013, 14636699535059014, 14636699535059015, 14636699535059016, 14636699535059017, 14636699535059018, 14636699535059020, 14636699535059021, 14636699535059022, 14636699535059023, 14636699535059024, 14636699535059025)

select * from dbo.DocJournal where id in (14636699535059010, 14662361218697372)
select * from dbo.DocJournal where PrintDocNum = N'0370000001149782'
select * from dbo.dhSales where id in (14636699535059010, 14662361218697372)
select * from dbo.refOuterCodes where idDistr = 281474978456148 and idItem in (14636699535059010, 14662361218697372) and TableName = N'DocJournal'
select * from dbo.drSales where idDoc in (14636699535059010, 14662361218697372)

select * from dbo.DocJournal where id = 14662361218697372
select * from dbo.dhSales where id = 14636699535059010
select * from dbo.refOuterCodes where id = 14636699535059019
select * from dbo.drSales where id in (14636699535059011, 14636699535059012, 14636699535059013, 14636699535059014, 14636699535059015, 14636699535059016, 14636699535059017, 14636699535059018, 14636699535059020, 14636699535059021, 14636699535059022, 14636699535059023, 14636699535059024, 14636699535059025)
select * from SQLCLUSTER.chicago_kraft.dbo.v_LogDataChange where TableName = N'DocJournal' and ChangeDate between N'20151208 12:34:00' and N'20151208 12:47:00' /*and idRecord in (14662361218697372, 14636699535059010)*/  order by ChangeDate
select * from SQLCLUSTER.chicago_kraft.dbo.v_LogDataChange where TableName = N'DocJournal' and ChangeDate between N'20151208 12:34:00' and N'20151208 12:47:00' and idRecord in (14662361218697372, 14636699535059010)  order by ChangeDate
select * from SQLCLUSTER.chicago_kraft.dbo.v_LogDataChange where /*TableName = N'DocJournal' and*/ ChangeDate between N'20150112 15:50:00' and N'20150112 16:00:00'/* and idRecord in (14662361218697372, 14636699535059010)*/  order by ChangeDate

select * from chicago_kraft.dbo.v_LogDataChange where ChangeDate between N'20150112 15:52:00' and N'20150112 16:00:00' order by ChangeDate
select * from sys_support.dbo.lockinfo_a l where l.Info_DateTime between '20150112 15:52:00' and '20150112 16:00:00' order by Info_DateTime
select * from chicago_kraft.dbo.LogSystemPerformance where StartOperation between '20150112 15:50:00' and '20150112 16:00:00' and idUser = 281477534298684 /* and TimeOperation > '19000101 00:02' */ order by StartOperation
select * from dbo.refPhysicalPersons where Name like N'%Vasyaev%'

select * from chicago_kraft.dbo.v_LogDataChange where ChangeDate between N'20150112 15:55:00' and N'20150112 16:00:00' order by ChangeDate
select * from sys_support.dbo.lockinfo_a l where l.Info_DateTime between'20150112 15:55:00' and '20150112 16:00:00' order by Info_DateTime
select * from chicago_kraft.dbo.LogSystemPerformance where StartOperation between '20150112 15:50:00' and '20150112 16:00:00' and idUser = 281477534298684 order by StartOperation

select * from chicago_kraft.dbo.v_LogDataChange where idRecord in (27866025290207089) order by idRecord, ChangeDate
select * from chicago_kraft.dbo.v_LogDataChange where ChangeDate > N'20160628 02:05:00' and TableName = N'refPropertiesHistory' and FieldName = N'deleted' and OldValue = N'0' and NewValue = N'1' order by ChangeDate

SELECT CONVERT(nvarchar(200), CONVERT (binary, job_id), 1) AS hex, *
FROM msdb.dbo.sysjobs_view
WHERE CONVERT(nvarchar(200), CONVERT(binary, job_id), 1)
LIKE '%874EE5FAE10E5842996%'
ORDER BY job_id

------------------------------------------------------------

;with cte(idItem)
as
(
	select distinct
		propertiesHistory.idItem
	from
		chicago_kraft.dbo.v_LogDataChange logDataChange
		join dbo.refPropertiesHistory propertiesHistory on propertiesHistory.id = logDataChange.idRecord
	where
		logDataChange.ChangeDate > N'20160628 02:05:00'
		and logDataChange.TableName = N'refPropertiesHistory'
		and logDataChange.FieldName = N'deleted'
		and logDataChange.OldValue = N'0'
		and logDataChange.NewValue = N'1'
)
select
	*
from
	dbo.refPropertiesHistory propertiesHistory
	join cte cte on cte.idItem = propertiesHistory.idItem
order by propertiesHistory.idItem, propertiesHistory.ObjectType, propertiesHistory.idEvent, propertiesHistory.EventDate

;with cte(idItem)
as
(
	select distinct
		propertiesHistory.idItem
	from
		chicago_kraft.dbo.v_LogDataChange logDataChange
		join dbo.refPropertiesHistory propertiesHistory on propertiesHistory.id = logDataChange.idRecord
	where
		logDataChange.ChangeDate > N'20160628 02:05:00'
		and logDataChange.TableName = N'refPropertiesHistory'
		and logDataChange.FieldName = N'deleted'
		and logDataChange.OldValue = N'0'
		and logDataChange.NewValue = N'1'
		and propertiesHistory.ObjectType = 20
)
select
	propertiesHistory.idItem,
	propertiesHistory.EventDate,
	count(*)
from
	dbo.refPropertiesHistory propertiesHistory
	join cte cte on cte.idItem = propertiesHistory.idItem
group by propertiesHistory.idItem, propertiesHistory.EventDate
having count(*) > 1
order by propertiesHistory.idItem, propertiesHistory.EventDate

------------------------------------------------------------


6535
	

16
	

���
	

���������� .NET Framework ���� ��������. ������ ������ ������ �������� AppDomain %.*ls. %.*ls

Error	4	Warning as Error: The variable 'e' is declared but never used	D:\WorkSpaces\CHICAGO\RELBranches\2.14.3\client\src\ch2_client\StuBaseForms\StuControls\StuProgress\StuProgressForm.cs	155	54	ch2_client

select * from Log where LoginDate > '20150421' and LoginDate < '20150421 11:30:34' and UserId = 0 order by 1


select
	*
from
	dbsrv.ch_demo_2_22_2_x.dbo.v_LogDataChange vLogDataChange
where
	vLogDataChange.HostName = N'I-NOZHENKO'
	--and vLogDataChange.UserName = N'I-NOZHENKO\i.nozhenko'
	and cast(vLogDataChange.ChangeDate as date) = N'20171102'
order by vLogDataChange.ChangeDate
