select
	*
from
	dbo.DocJournal docJournal
	join dbo.dhWorkingDayOfAgent dh on dh.id = docJournal.id
	join dbo.drWorkingDayOfAgent dr on dr.idDoc = dh.id
	join dbo.drWorkingDayOfAgentPhotos drPhotos on drPhotos.idDocRow = dr.id
	--join dbo.refDistributors d on d.NodeID = cast(dj.id/281474976710656 as smallint)
where
	docJournal.id = 562954288024773
	--docJournal.PrintDocNum = N'4210000000000001'
	--cast(docJournal.OpDate as date) = N'20180122'
	--docJournal.id = 41940892519854262
	--and d.deleted=0

select
	drPhotos.id,
	drPhotos.photoFileName,
	*
from
	dbo.drWorkingDayOfAgentPhotos drPhotos
	join dbo.drWorkingDayOfAgent dr on dr.id = drPhotos.idDocRow
	join dbo.DocJournal docJournal on docJournal.id = dr.idDoc
where
	drPhotos.photoFileName like N'http%'
	and (docJournal.idStatus = 1 or (docJournal.idStatus = 3 and docJournal.deleted = 1))

select
	*
from
	dbo.drWorkingDayOfAgentPhotos drPhotos
	join dbo.drWorkingDayOfAgent dr on dr.id = drPhotos.idDocRow
	join dbo.refFiles files on files.id = dr.idPacket
where
	drPhotos.id = 562954288026786

--update dbo.drWorkingDayOfAgentPhotos set photoFileName = N'http://st-drive.systtech.ru/31a19b1c9fc54fd6b63298379cfa7f1b' where id = 562954287820041
