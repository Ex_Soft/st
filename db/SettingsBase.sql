﻿select * from dbo.SettingsBase where CodeKey = N'BuildVersion'

select * from dbo.SettingsBase where CodeKey like N'%!_%' escape N'!'

select * from dbo.SettingsBase where CodeKey = N'MT2CompabilityMode'
--update dbo.SettingsBase set Value = 0 where CodeKey = N'MT2CompabilityMode'

select * from dbo.SettingsBase where CodeKey = N'AdditionalInfoInTitle'
--update dbo.SettingsBase set Value = N'ch_old' where CodeKey = N'AdditionalInfoInTitle'

/* update SettingsBase set Value=N'2.10.1.24' CodeKey=N'BuildVersion' */
/* update SettingsBase set Value=N'2.10.0.12' CodeKey=N'BuildVersion' */

--update dbo.SettingsBase set Value = N'BpCo'  where CodeKey = N'BpDisplayMode'
select * from dbo.SettingsBase where CodeKey = N'BpDisplayMode'

select * from dbo.SettingsBase where CodeKey = N'UsingProtectionPersonalInformation'
--update dbo.SettingsBase set Value = N'false' where CodeKey = N'UsingProtectionPersonalInformation'
--update dbo.SettingsBase set Value = N'true' where CodeKey = N'UsingProtectionPersonalInformation'

if not exists(select 1 from dbo.SettingsBase where CodeKey = N'TrialPeriod')
	insert into dbo.SettingsBase
	(CodeKey, [Value], [Description], [Type])
	values
	(N'TrialPeriod', convert(nvarchar(255), EncryptByPassPhrase('SecretKey', '2025.12.31'), 2), N'триальный ключ', N'Service')

select * from dbo.SettingsBase where CodeKey = N'TrialPeriod'
--update dbo.SettingsBase set [Value] = N'' where CodeKey = N'TrialPeriod'
--update dbo.SettingsBase set [Value] = convert(nvarchar(255), EncryptByPassPhrase('SecretKey', '2025.12.31'), 2) where CodeKey = N'TrialPeriod'
--update dbo.SettingsBase set CodeKey = N'TrialPeriod' where CodeKey = N'Trial_Period'
select convert(varchar(100), DecryptByPassPhrase('SecretKey', convert(varbinary(200), (select '0x' + Value from dbo.SettingsBase where CodeKey = N'TrialPeriod'), 1)))


select *, len(Value) as ValueLength from dbo.SettingsBase where CodeKey = N'CompanyId'
--update dbo.SettingsBase set Value = N'bdd5cd1a-6353-4cdd-bb28-882f14513f82' where CodeKey = N'CompanyId'
--update dbo.SettingsBase set Value = N'04a83e85-8714-44d6-b167-7f7b0d27d0c2' where CodeKey = N'CompanyId'

select * from dbo.SettingsBase where CodeKey = N'UlsUrl'
--update dbo.SettingsBase set Value = N'kgHcDpfNROY5y678gvxBj0peIU6HyoWPb4ItY1RoZP40tVLBbjk0sAobGu9E14bWigQD7xhPgMBZoZDylVbASmG7yPWBIwZnEY1dy7U6wq6oDyiOsnZiHUVte7ZKIOGts4VN4y4y166wxQHHN+baD/8HqBTmyP4icJjufT8z3cA=' where CodeKey = N'UlsUrl'

select * from dbo.SettingsBase where CodeKey = N'UlsConsumerKey'
--update dbo.SettingsBase set Value = N'Z8bTyZpipXPfN0hVOcdviVY0QUsqjvVPYBKdH4w60duY8wDMQ7dIAU2lGN/5WWkTia1Hrrh2nfDdneKXKcQdQQchQq4ZB+TtTNy4raURCpk5Dgh6OSHYd/QnPODqKvkl1QmyYHOC4Tsqluvhp5ufwA/6vtfhXEpgKaZQZIa7d+k=' where CodeKey = N'UlsConsumerKey'

select * from dbo.SettingsBase where CodeKey = N'DoubleServiceDistributorsMultipleSearch'
--update dbo.SettingsBase set Value = N'true' where CodeKey = N'DoubleServiceDistributorsMultipleSearch'

select * from dbo.SettingsBase where CodeKey in (N'ChicagoReportsLogin', N'ChicagoReportsPassword')

select * from dbo.SettingsBase where CodeKey = N'ApplicationSkin'
--update dbo.SettingsBase set Value = N'Office 2016 Colorful' where CodeKey = N'ApplicationSkin'
--update dbo.SettingsBase set Value = N'Caramel' where CodeKey = N'ApplicationSkin'