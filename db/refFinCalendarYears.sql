select
	*
from
	dbo.refFinCalendarYears finCalendarYears
	join dbo.refFinCalendarMonths finCalendarMonths on finCalendarMonths.idYear = finCalendarYears.id
	join dbo.refFinCalendarDays finCalendarDays on finCalendarDays.idMonth = finCalendarMonths.id