select
	*
from
	dbo.refOwners owners
where
	owners.Name like N'Owner%'

--update dbo.refOwners set Name = N'Owner# 1 (�����-2.1)' where id = 21392098230013861

select
	*
from
	dbo.refOwners owners
	join dbo.refOwnerDistributor ownerDistributor on ownerDistributor.idOwner = owners.id

select
	*
from
	dbo.refOwnerDistributor ownerDistributor
where
	ownerDistributor.idOwner = 0

--update dbo.refOwnerDistributor set deleted = 1 where id = 562954287778930
--delete from dbo.refOwnerDistributor where id = 562954287778930

select count(*) from dbo.refBuyers buyers where buyers.id = buyers.idMain and buyers.deleted = 0
select count(*) from dbo.refOwners owners where owners.deleted = 0

select
	*
from
	dbo.refBuyers buyers
	left join dbo.refOwners owners on owners.id = buyers.id -- and owners.deleted = 0
where
	buyers.id = buyers.idMain
	and buyers.deleted = 0
	and owners.id is null

select
	*
from
	dbo.refOwners owners
	left join dbo.refBuyers buyers on buyers.id = owners.id --and buyers.deleted = 0
where
	owners.deleted = 0
	and buyers.id is null

select count(*) from dbo.refBuyers buyersMain join dbo.refBuyers buyersDistributor on buyersDistributor.idMain = buyersMain.id where buyersMain.id = buyersMain.idMain and buyersMain.deleted = 0 and buyersDistributor.deleted = 0
select count(*) from dbo.refOwnerDistributor where deleted = 0

select
	*
from
	dbo.refBuyers buyersMain
	join dbo.refBuyers buyersDistributor on buyersDistributor.idMain = buyersMain.id
	left join dbo.refOwnerDistributor ownerDistributor on ownerDistributor.idOwner = buyersMain.idMain and ownerDistributor.idDistributor = buyersDistributor.idDistributor and ownerDistributor.deleted = 0
where
	buyersMain.id = buyersMain.idMain
	and buyersMain.deleted = 0
	and buyersDistributor.deleted = 0
	and ownerDistributor.id is null
order by buyersDistributor.idMain, buyersDistributor.id, buyersDistributor.idDistributor

select
	*
from
	dbo.refOwnerDistributor ownerDistributor
	left join dbo.refBuyers buyers on buyers.idMain = ownerDistributor.idOwner and buyers.idDistributor = ownerDistributor.idDistributor --and buyers.deleted = 0
where
	ownerDistributor.deleted = 0
	and buyers.id is null
