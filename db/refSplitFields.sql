﻿select
	*
from
	dbo.refSplitFields splitFields
	left join dbo.EnumsBase enumsTable on enumsTable.id = splitFields.idTable
	left join dbo.EnumsBase enumsField on enumsField.id = splitFields.idField
where
	enumsTable.CodeKey = N'SplitTable_refGoods'
order by splitFields.idTable

select
	*
from
	dbo.EnumsBase e
where
	e.CodeKey like N'SplitField_refGoods_%'
order by id

select
	et.Description as TableName,
	ef.Description as AttributeName,
	case
		when sf.idTable < 1000
			then
				case sf.deleted
					when 0 then N'Разделяемый'
					else N'Неразделяемый'
				end
		else
				case sf.deleted
					when 0 then N'Копировать при сопоставлении'
					else N'Не копировать при сопоставлении'
				end
	end as Result
from
	dbo.EnumsBase ef
	join dbo.refSplitFields sf on sf.idField = ef.id
	join dbo.EnumsBase et on et.id = sf.idTable
where
	ef.CodeKey like N'%SplitField%'
order by et.Description, ef.Description

select
	*
from
	dbo.EnumsBase e
where
	e.CodeKey like N'SplitField!_refCounteragent%' escape N'!'

select
	*
from
	dbo.EnumsBase e
where
	e.CodeKey like N'SplitField!_%' escape N'!'

------------------------------------------------------------

select
	*
from
	dbo.EnumsBase et
	join dbo.refSplitFields sf on sf.idTable = et.id
	left join dbo.EnumsBase ef on ef.id = sf.idField and ef.CodeKey like N'SplitField!_refBuyers!_%' escape N'!'
where
	et.CodeKey = N'SplitTable_refBuyers'
	and ef.id is null

delete
	dbo.refSplitFields
from
	dbo.EnumsBase et
	join dbo.refSplitFields sf on sf.idTable = et.id
	left join dbo.EnumsBase ef on ef.id = sf.idField and ef.CodeKey like N'SplitField!_refBuyers!_%' escape N'!'
where
	et.CodeKey = N'SplitTable_refBuyers'
	and ef.id is null

select
	*
from
	dbo.EnumsBase et
	join dbo.refSplitFields sf on sf.idTable = et.id
	left join dbo.EnumsBase ef on ef.id = sf.idField and ef.CodeKey like N'SplitField!_refCounteragentsEx!_%' escape N'!'
where
	et.CodeKey = N'SplitTable_refCounteragentsEx'
	and ef.id is null
	
select
	*
from
	dbo.EnumsBase et
	join dbo.refSplitFields sf on sf.idTable = et.id
	left join dbo.EnumsBase ef on ef.id = sf.idField and ef.CodeKey like N'%SplitField!_refGoods!_%' escape N'!'
where
	et.CodeKey = N'SplitTable_refGoods'
	and ef.id is null

select
	*
from
	dbo.EnumsBase et
	join dbo.refSplitFields sf on sf.idTable = et.id
	left join dbo.EnumsBase ef on ef.id = sf.idField and ef.CodeKey like N'SplitField!_refBuyPoints!_%' escape N'!'
where
	et.CodeKey = N'SplitTable_refBuyPoints'
	and ef.id is null

SELECT e.id, e.CodeKey, e.VALUE, s.idTable, s.idField, s.deleted, s.id
FROM EnumsBase e 
left join refSplitFields s 
ON e.id = s.idField and s.deleted = 0
WHERE e.CodeKey LIKE '%SplitField_refBuyPoints%'

select * from dbo.refBuyPoints where idMain = 281474992314709
select * from dbo.refDistributors where id = 281474977164328
