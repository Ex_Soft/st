select
	*
from
	dbo.refSets [sets]
	join dbo.XPObjectType xpObjectType on xpObjectType.OID = [sets].ObjectType
where
	--[sets].Field like N'%BuyPoint%' or [sets].TableName like N'%BuyPoint%'
	--[sets].id = 562954287887158
	[sets].idItem = 562954288065291

--update dbo.refSets set Field = N'Outlet' where Field = N'BuyPoint'
--update dbo.refSets set Field = N'OutletType' where Field = N'BuyPointType'
--update dbo.refSets set TableName = N'refOutletSets' where TableName = N'refBuyPointSets'

select
	*
from
	dbo.refFilters filters
	join dbo.refSets [sets] on [sets].idItem = filters.id
	join dbo.refSkuSets skuSets on skuSets.idSet = [sets].id
where
	--filters.id = 562954287030632
	filters.Name like N'55717%'

select
	*
from
	dbo.refQstTemplates qstTemplates
	join dbo.refSets [sets] on [sets].idItem = qstTemplates.id
	join dbo.refBuypointSets buypointSets on buypointSets.idSet = [sets].id
where
	qstTemplates.deleted = 0
	and buypointSets.id = 562954286541682

select
	*
from
	dbo.refQstTemplates qstTemplates
	join dbo.refSets [sets] on [sets].idItem = qstTemplates.id
	join dbo.refOutletSets outletSets on outletSets.idSet = [sets].id
where
	qstTemplates.deleted = 0

select
	*
from
	dbo.refPlanning planning
	join dbo.refSets [sets] on [sets].idItem = planning.id
	join dbo.refOutletSets outletSets on outletSets.idSet = [sets].id
	join dbo.refOutlets outlets on outlets.id = outletSets.idOutlet
where
	planning.id = 562954287887151
