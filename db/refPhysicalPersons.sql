﻿select * from dbo.refPhysicalPersons where len(coalesce(LicenseData, N'')) != 0

select * from dbo.refPhysicalPersons where Name = N'BuyPoints editor'
--update dbo.refPhysicalPersons set LicenseData = N'{"LicenseKey":{"ExpirationDate":"2017-10-17T21:00:00.0000000Z"}}' where Name = N'BuyPoints editor'

select distinct len([Password]) from dbo.refPhysicalPersons
--update dbsrv.ch_demo_3_0_3_X.dbo.refPhysicalPersons set Password=N'4909F1930A7906832845B9DE7911682517D0C13B' where Name = N'Administrator'
select * from dbo.refPhysicalPersons where id = 0
select * from dbo.refPhysicalPersons where Name like N'%pas%'
select * from dbo.refPhysicalPersons where Name = N'VictimForPassword'
select * from dbo.refPhysicalPersons where WindowsLogin = N'i-nozhenko\i.nozhenko'
select * from dbo.refPhysicalPersons where IsExportedToUls = 1
select * from dbo.refPhysicalPersons where IsSupport = 1
--update dbo.refPhysicalPersons set IsExportedToUls = 0 where id = 281474976711436

--update dbo.refPhysicalPersons set deleted = 1 where id = 281474989750384

--update dbo.refPhysicalPersons set IsSupport = 1 where Name = N'Administrator'

select
	--physicalPersons.idDistributor,
	*
from
	dbo.refPhysicalPersons physicalPersons
where
	physicalPersons.Name like N'%Криволапко АА%'

select
	*
from
	dbo.refPhysicalPersons physicalPersons with (nolock)
	join dbo.vEmplGridFields vEmplGridFields with (nolock) on vEmplGridFields.id = physicalPersons.idvEmplView
where
	physicalPersons.id = 281474978390434
	or (physicalPersons.deleted = 0 and vEmplGridFields.ischuser = 1)

select distinct
	physicalPersons.name
from
	dbo.refPhysicalPersons physicalPersons with (nolock)
	join dbo.refEmployeesPositions employeesPositions with (nolock) on employeesPositions.idEmployee = physicalPersons.id
	join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
	join dbo.refEmployeesRoles employeesRoles with (nolock) on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
	join dbo.refRoles roles with (nolock) on roles.id = employeesRoles.idRole
where
	physicalPersons.deleted = 0
	and employeesPositions.deleted = 0
	and positions.deleted = 0
	and employeesRoles.deleted = 0
	and roles.deleted = 0
	and roles.HasUserRights = 1

-- Add New Employee
--select * from dbo.refPhysicalPersons where Name like N'%Editor%'
--select * from dbo.refPositions where Name = N'DC Editor'

declare
	@sid varbinary(8000) = 0x010500000000000515000000713A8BEBEFCF07C61463BBFAE9030000

--update dbo.refPhysicalPersons set SID = @sid where id = 281474992222585

select * from  dbo.refPhysicalPersons where SID = @sid
--update dbo.refPhysicalPersons set deleted = 1 where id = 281474992222585
--update dbo.refPhysicalPersons set deleted = 1 where WindowsLogin = N'i-nozhenko\i.nozhenko'

--update dbo.refPhysicalPersons set WindowsLogin = N'domain\user' where id in (281474976711460, 281474976711466)

select
	vEmplGridFields.ischuser,
	*
from
	dbo.refPhysicalPersons physicalPersons
	left join dbo.refEmployeesPositions employeesPositions on employeesPositions.idEmployee = physicalPersons.id
	left join dbo.refPositions positions on positions.id = employeesPositions.idPosition
	left join dbo.refEmployeesRoles employeesRoles with (nolock) on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
	left join dbo.refRoles roles with (nolock) on roles.id = employeesRoles.idRole
	left join dbo.vEmplGridFields vEmplGridFields on vEmplGridFields.id = physicalPersons.id
where
	--len(isnull(physicalPersons.WindowsLogin, N'')) = 0
	--physicalPersons.Name like N'Test ULS%'
	physicalPersons.Name = N'WinUser'
	and employeesPositions.Deleted = 0
	and positions.Deleted = 0
	and employeesRoles.Deleted = 0
	and roles.Deleted = 0
	or physicalPersons.Name = N'Administrator'
	or physicalPersons.Name = N'999_01'
	or physicalPersons.Name = N'test rightless employee'

select
	*
from
	dbo.refPhysicalPersons physicalPersons
	left join dbo.refEmployeesPositions employeesPositions on employeesPositions.idEmployee = physicalPersons.id
	left join dbo.refPositions positions on positions.id = employeesPositions.idPosition
--  join dbo.refEmployeesRoles employeesRoles with (nolock) on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
--	left join dbo.refRoles roles with (nolock) on roles.id = employeesRoles.idRole
where
	physicalPersons.id = 281474976711490

select
	*
from
	dbo.vEmplGridFields vEmplGridFields
where
	vEmplGridFields.id = 562954287806994

select
	employeesPositions.idEmployee,
	count(*)
from
	dbo.refEmployeesPositions employeesPositions
where
	employeesPositions.deleted = 0
group by employeesPositions.idEmployee
having count(*) > 1

select
	employeesPositions.idPosition,
	count(*)
from
	dbo.refEmployeesPositions employeesPositions
where
	employeesPositions.deleted = 0
group by employeesPositions.idPosition
having count(*) > 1

--select * from dbo.refEmployeesPositions

--select * from dbo.refDistributors where id = 281474976711134

--insert into dbo.refEmployeesPositions (id, idEmployee, idPosition, deleted) values (dbo.fn_GetIdEx(281474976710657/*281474976711134*/, 1), 281475014837810, 281475014837746, 0)

--update dbo.refPositions set idChief = 281475015334748 where id = 281475014837746
--update dbo.refPhysicalPersons set webLogin = N'Reporter' where id = 562954286576137

--select * from dbo.refRoles

--insert into dbo.refEmployeesRoles (id, idEmployeesPositionsRoles, idRole, deleted) values (dbo.fn_GetIdEx(281474976710657/*281474976711134*/, 1), 281475015334748, 281475015320469, 0)

;with cte(idEmployee, idPosition)
as
(
	select
		id as idEmployee,
		(select id from dbo.refPositions where Name = N'Position 4 test ULS 1.1.1') as idPosition
	from
		dbo.refPhysicalPersons
	where
		Name = N'Test ULS 1.1.1'
)
select
	*
from
	cte cte
	left join dbo.refEmployeesPositions employeesPositions on employeesPositions.idEmployee = cte.idEmployee and employeesPositions.idPosition = cte.idPosition
	left join dbo.refEmployeesRoles employeesRoles on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
	left join dbo.refRoles roles on roles.id = employeesRoles.idRole;

-- update dbo.refRoles set Name = N'Role 4 Test ULS 1.1.1' where id = 281475015320469

------------------------------------------------------------

select
	*
from
	refPhysicalPersons pp
	join refEmployeesPositions ep on (ep.idEmployee=pp.id)
	join refPositions p on (p.id=ep.idPosition)

select
	*
from
	dbo.refPositions p
	left join dbo.refPositionsDistributors pd on pd.idPosition=p.id and pd.deleted=0
	left join dbo.refOutercodes oc on oc.idItem=p.id
where
	p.deleted = 0
	and pd.id is not null
	and p.id=281474977159395
order by p.id


select
	*
from
	dbo.refPhysicalPersons physicalPersons
where
	--physicalPersons.Name like N'Алехин%'
	--physicalPersons.Name like N'Але%'
	--physicalPersons.Name in (N'Данилков Александр', N'Андреев Олег')
	physicalPersons.Code = N'D1570'

--update dbo.refPhysicalPersons set Password = N'0D5399508427CE79556CDA71918020C1E8D15B53' where id = 13229323905425340
