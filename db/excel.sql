/*

if object_id(N'dbo.sp_ReadExcel', N'pc') is not null
	drop procedure dbo.sp_ReadExcel
go

if exists (select 1 from sys.assemblies asms where asms.name = N'fn_ReadExcel' and is_user_defined = 1)
	drop assembly fn_ReadExcel
go

--create assembly fn_ReadExcel from 'D:\WorkSpaces\CHICAGO\DEV\db\src\ReadExcel\fn_ReadExcel\bin\Debug\fn_ReadExcel.dll' with permission_set = unsafe
create assembly fn_ReadExcel from 'D:\WorkSpaces\CHICAGO\DEV\db\src\ReadExcel\fn_ReadExcel\bin\Release\fn_ReadExcel.dll' with permission_set = unsafe
go

CREATE PROCEDURE [dbo].[sp_ReadExcel]
	@isFirstRowColumnNames [bit],
	@FileName [nvarchar](1000),
	@SQLTableName [nvarchar](1000),
	@SheetName [nvarchar](1000),
	@ColumnCount [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [fn_ReadExcel].[fn_ReadExcel.ReadExcel].[LoadExcel]
GO

*/

declare @result int
exec @result = dbo.sp_ReadExcel @isFirstRowColumnNames = 1, @FileName = N'\\i-nozhenko\exchange\testRead.xlsx', @SQLTableName = N'TestTable_002', @SheetName = N'', @ColumnCount = 0;
select @result, * from TestTable_002