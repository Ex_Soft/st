﻿select
	*
from
	dbo.vAttributes vAttributes
where
	vAttributes.TypeCodeKey = N'Attrs_Type_Ref'
	and vAttributes.ObjectTypeCodeKey = N'Attrs_ObjectType_refGoods'

select
	enumsValueType.*,
	*
from
	dbo.refAttributesValues attributesValues
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
where
	enumsValueType.CodeKey in (N'Attrs_ValueType_Date', N'Attrs_ValueType_Datetime')

-- AddAdditional attribute
select
	*
from
	dbo.refAttributesBase attributesBase
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
where
	--attributesBase.id = 281474990447600
	--enumsBaseObject.CodeKey in (N''/*N'Attrs_ObjectType_dhMerch', N'Attrs_ObjectType_drMerch', N'Attrs_ObjectType_dhGoodsReturn', N'Attrs_ObjectType_drGoodsReturn'*/)
	--enumsValueType.CodeKey = N'Attrs_ValueType_Photo'
	(enumsBaseObject.CodeKey = N'Attrs_ObjectType_refGoods' or enumsBaseObject.CodeKey = N'Attrs_ObjectType_drGoodsReturn')
	--attributesBase.Name like N'%dr%'
	--and enumsAttrType.CodeKey != N'Attrs_Type_Periodic'
	and attributesBase.deleted = 0
order by attributesBase.Position, attributesBase.Name
order by enumsBaseObject.CodeKey, attributesBase.idValueType


--update dbo.refAttributesBase set Name = N'ДА Отгрузка товара (dr) #5 boo' where id = 562954287760820

select
	*
from
	dbo.refAttributesValues attributesValues
where
	attributesValues.id = 0
	attributesValues.idElement = 562954287603227

--insert into dbo.refAttributesValues (id, idElement, idAttribute, ObjectType, idPacket, Value) values (dbo.fn_getIdEx(281474976711134,1), 562954287607237, 562954287601224, 10359, 562954286647461, N'')
--update dbo.refAttributesValues set ObjectType = 10358 where id = 562954287604230
--update dbo.refAttributesValues set idElement = 562954287605230 where id = 562954287604230
--update dbo.refAttributesValues set idAttribute = 562954287606233 where id = 562954287608239
------------------------------------------------------------

select
	*
from
	dbo.refAttributesBase attributesBase
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
where
	attributesBase.deleted = 0

select
	*
from
	dbo.refPeriodicStatuses periodicStatuses
	join dbo.EnumsBase enumsType on enumsType.id = periodicStatuses.IdType

-- Doubles

select
	attributesValues.idElement,
	attributesValues.idAttribute,
	attributesValues.StartDate,
	count(*)
from
	dbo.refAttributesValues attributesValues
where
	attributesValues.deleted = 0
	and attributesValues.idAttribute != 0
group by attributesValues.idElement, attributesValues.idAttribute, attributesValues.StartDate
having count(*) > 1

select
	attributesValues.idElement,
	attributesValues.ObjectType,
	attributesValues.idAttribute,
	attributesValues.StartDate,
	count(*)
from
	dbo.refAttributesValues attributesValues
where
	attributesValues.deleted = 0
	and attributesValues.idAttribute != 0
group by attributesValues.idElement, attributesValues.ObjectType, attributesValues.idAttribute, attributesValues.StartDate
having count(*) > 1

;with cte (idElement, ObjectType, idAttribute, StartDate, cnt)
as
(
select
	attributesValues.idElement,
	attributesValues.ObjectType,
	attributesValues.idAttribute,
	attributesValues.StartDate,
	count(*)
from
	dbo.refAttributesValues attributesValues
where
	attributesValues.deleted = 0
	and attributesValues.idAttribute != 0
group by attributesValues.idElement, attributesValues.ObjectType, attributesValues.idAttribute, attributesValues.StartDate
having count(*) > 1
)
select
	*
from
	dbo.refAttributesValues attributesValues
	join cte cte on cte.idElement = attributesValues.idElement
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
	join dbo.XPObjectType xpObjectType on xpObjectType.OID = attributesValues.ObjectType


select
	*
from
	dbo.refAttributesValues attributesValues
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
	join dbo.XPObjectType xpObjectType on xpObjectType.OID = attributesValues.ObjectType
where
	attributesValues.idElement = 281477182060096

select
	snsAttributesValues.idElement,
	count(*)
from
	dbo.snsAttributesValues snsAttributesValues
group by snsAttributesValues.idElement
having count(*) > 1

------------------------------------------------------------

select
	*
from
	dbo.refAttributesValues attributesValues
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
	join dbo.XPObjectType xpObjectType on xpObjectType.OID = attributesValues.ObjectType
where
	attributesValues.idElement = 318348198660577333
	and attributesValues.deleted = 0
order by attributesValues.idElement, attributesValues.idAttribute, attributesValues.StartDate

select
	*
from
	dbo.refAttributesValues av
where
	av.idElement in (281475094747872, 27866022694421746, 307370674568070396)
order by av.idElement, av.idAttribute, av.StartDate

select
	*
from
	dbo.refAttributesValues attributesValues
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
	join dbo.XPObjectType xpObjectType on xpObjectType.OID = attributesValues.ObjectType
where
	cast(attributesValues.StartDate as date) = N'20160112'

;with cte (idElement, ObjectType, idAttribute, cnt)
as
(
select
	attributesValues.idElement,
	attributesValues.ObjectType,
	attributesValues.idAttribute,
	count(*)
from
	dbo.refAttributesValues attributesValues with (nolock)
	join dbo.refAttributesBase attributesBase with (nolock) on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumValueType with (nolock) on enumValueType.id = attributesBase.idValueType
where
	attributesValues.deleted = 0
	and enumValueType.CodeKey != N'Attrs_ValueType_Multiple'
group by attributesValues.idElement, attributesValues.ObjectType, attributesValues.idAttribute
having count(*) > 1
)
select
	*
from
	dbo.refAttributesValues attributesValues with (nolock)
	join cte cte on cte.idElement = attributesValues.idElement and cte.ObjectType = attributesValues.ObjectType and cte.idAttribute = attributesValues.idAttribute
	join dbo.XPObjectType xpObjectType with (nolock) on xpObjectType.OID = attributesValues.ObjectType
	join dbo.refAttributesBase attributesBase with (nolock) on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumValueType with (nolock) on enumValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumAttrType with (nolock) on enumAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumBaseObject with (nolock) on enumBaseObject.id = attributesBase.idBaseObject
order by attributesValues.idElement, attributesValues.ObjectType, attributesValues.idAttribute, attributesValues.StartDate

select * from dbo.refBuyPoints where id = 281474992314857

------------------------------------------------------------
-- Additional Attributes

select
	*
from
	dbo.EnumsBase
where
	CodeKey like N'Attrs!_ObjectType!_%' escape N'!'


select
	*
from
	dbo.refAttributesBase attributesBase
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
where
	--enumsBaseObject.CodeKey = N'Attrs_ObjectType_refRoutes'
	enumsBaseObject.CodeKey = N'Attrs_ObjectType_refGoods'

select
	--attributesValues.*
	*
from
	--dbo.refBuyPoints [ref]
	dbo.refGoods [ref]
	join dbo.refAttributesValues attributesValues on attributesValues.idElement = [ref].id
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	left join dbo.refAttributesListValues attributesListValues on attributesListValues.id = attributesValues.idValue and attributesListValues.deleted = 0
where
	[ref].FullName = N'Goods with add'
	and [ref].deleted = 0
	and attributesValues.deleted = 0
	and attributesBase.deleted = 0

select
	*
from
	dbo.refAttributesValues attributesValues
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
	--left join dbo.refAttributesListValues attributesListValues on attributesListValues.id = attributesValues.idValue and attributesListValues.deleted = 0
	--join dbo.XPObjectType xpObjectType on xpObjectType.OID = attributesValues.ObjectType
where
	attributesValues.deleted = 0
	and attributesBase.deleted = 0
	and enumsValueType.CodeKey = N'Attrs_ValueType_Photo'
	--and enumsValueType.CodeKey  in (N'Attrs_ValueType_Single', N'Attrs_ValueType_Multiple')
	--and enumsAttrType.CodeKey not in (N'Attrs_Type_Periodic')
	and attributesValues.idValue != 0

declare
	@p0 bigint = 60,
	@p1 bigint = 281474992342428,
	@p2 bigint = 281474992312562,
	@p3 bigint = 281474992314685,
	@p4 bigint = 281474990515696,
	@p5 nvarchar(max) = N'1',
	@p6 bigint = 281474992309018,
	@p7 nvarchar(max) = N'string',
	@p8 bigint = 281474990447600,
	@p9 nvarchar(max) = N'281474990447602',
	@p10 nvarchar(max) = N'281474990447604',
	@p11 bigint = 562954286546724,
	@p12 nvarchar(max) = N'562954286546725'

select
	*
from
	"dbo"."refAttributesValues" N0
where
	(N0."ObjectType" = @p0)
	and (N0."deleted" = 0)
	--and N0."idElement" in (@p1,@p2,@p3)
	--and (select count(*) from "dbo"."refAttributesValues" N1 where (N1."idElement" = N0."idElement") and (N1."ObjectType" = @p0) and (N1."deleted" = 0) and (N1."idAttribute" = @p4) and (N1."Value" = @p5)) = 1
	and exists (select 1 from "dbo"."refAttributesValues" N1 where (N1."idElement" = N0."idElement") and (N1."ObjectType" = @p0) and (N1."deleted" = 0) and (N1."idAttribute" = @p4) and (N1."Value" = @p5))
	--and (select count(*) from "dbo"."refAttributesValues" N2 where (N2."idElement" = N0."idElement") and (N2."ObjectType" = @p0) and (N2."deleted" = 0) and (N2."idAttribute" = @p6) and (N2."Value" = @p7)) = 1
	and exists (select 1 from "dbo"."refAttributesValues" N2 where (N2."idElement" = N0."idElement") and (N2."ObjectType" = @p0) and (N2."deleted" = 0) and (N2."idAttribute" = @p6) and (N2."Value" = @p7))
	and (select count(*) from "dbo"."refAttributesValues" N3 where (N3."idElement" = N0."idElement") and (N3."ObjectType" = @p0) and (N3."deleted" = 0) and (N3."idAttribute" = @p8) and ((N3."idValue" = @p9) or (N3."idValue" = @p10))) = 2
	--and (select count(*) from "dbo"."refAttributesValues" N4 where (N4."idElement" = N0."idElement") and (N4."ObjectType" = @p0) and (N4."deleted" = 0) and (N4."idAttribute" = @p11) and (N4."idValue" = @p12)) = 1
	and exists (select 1 from "dbo"."refAttributesValues" N4 where (N4."idElement" = N0."idElement") and (N4."ObjectType" = @p0) and (N4."deleted" = 0) and (N4."idAttribute" = @p11) and (N4."idValue" = @p12))


------------------------------------------------------------

select
	*
from
	dbo.refPeriodicStatuses ps
	left join dbo.EnumsBase e on e.id=ps.IdType

------------------------------------------------------------

select
	*
from
	dbo.refAttributesValues av
where
	av.StartDate=N'20131231'
order by av.StartDate desc

select
	*
from
	dbo.refPropertiesHistory ph
where
	ph.EventDate=N'20131231'
	
select
	*
from
	dbo.refGoods g
	left join dbo.refAttributesValues av on av.idElement=g.id
	left join dbo.refPropertiesHistory ph on ph.idItem=g.id
where
	g.FullName=N'Тестовый вопрос #4'
	
select
	*
from
	refAttributesBase a
	join enums eat on (eat.id=a.idAttrType)
	join enums ebo on (ebo.id=a.idBaseObject)
	join enums evt on (evt.id=a.idValueType)
where
	(a.deleted=0)
	and (eat.CodeKey=N'Attrs_Type_DocHeader')
	and (ebo.CodeKey=N'Attrs_ObjectType_dhPreorder')

------------------------------------------------------------

declare
	@ObjectTypeOld int,
	@ObjectTypeNew int,
	@idAttribute bigint

select
	@ObjectTypeOld=OID
from
	dbo.XPObjectType
where
	TypeName=N'Chicago2.Core.StuBaseCore.StuObjects.StuPropertiesHistoryBuyPoint'

select
	@ObjectTypeNew=OID
from
	dbo.XPObjectType
where
	TypeName=N'Chicago2.Core.StuBaseCore.StuObjects.StuPeriodicAttributesValueBuyPoint'

select
	@idAttribute=id
from
	dbo.refAttributesBase
where
	(Name=N'Статус ТТ')
	and (idAttrType=7001)
	and (idValueType=7002)
	and (idBaseObject=10)
	and (deleted=0)

select @ObjectTypeOld as ObjectTypeOld, @ObjectTypeNew as ObjectTypeNew, @idAttribute as idAttribute

select
	*
from
	dbo.refPropertiesHistory ph

select
	*
from
	dbo.refAttributesValues av
where
	(av.ObjectType=@ObjectTypeNew)
	and (av.idAttribute=@idAttribute)


select
	*
from
	dbo.refAttributesValues av
	join dbo.refAttributesBase a on a.id=av.idAttribute
	join dbo.XPObjectType ot on ot.OID=av.ObjectType
	join enums eat on (eat.id=a.idAttrType)
	join enums ebo on (ebo.id=a.idBaseObject)
	join enums evt on (evt.id=a.idValueType)
	left join dbo.refAttributesListValues alv on alv.id=av.idValue and alv.idAttribute=av.idAttribute
	join dbo.dhPreOrder dhPO on dhPO.id=av.idElement
where
	av.deleted=0

------------------------------------------------------------
-- Значение атрибута
select
	*
from
	dbo.refAttributesValues av
	join dbo.refPhysicalPersons pp on pp.id = av.idElement
	join dbo.refAttributesBase ab on ab.id=av.idAttribute
	join dbo.EnumsBase evt on evt.id = ab.idValueType
	join dbo.EnumsBase eat on eat.id = ab.idAttrType
where
	av.idElement = 281474978390434


select
	*
from
	dbo.refAttributesValues av
	left join dbo.XPObjectType ot on ot.OID = av.ObjectType
	left join dbo.refAttributesBase ab on ab.id = av.idAttribute
	left join enums eat on (eat.id=ab.idAttrType)
	left join enums ebo on (ebo.id=ab.idBaseObject)
	left join enums evt on (evt.id=ab.idValueType)
where
	av.idElement = 22236524390549760
------------------------------------------------------------

declare
@p0 nvarchar(max)=N'Attrs_ObjectType_dhPreorder',
@p1 bigint=0,
@p2 bit=0,
@p3 bigint=281474976711134,
@p4 bigint=281474976710657

select N0."id",N0."deleted",N0."Name",N0."idValueType",N0."idBaseObject",N0."Position",N0."PDAEditPos",N0."PDAViewPos",N0."idAttrType",N0."idDistributor",N1."ObjectType",N0."IsBrowsable",N0."IsEditable",N0."Code",N0."verstamp" from ((("dbo"."refAttributesBase" N0 with (nolock)
 left join "dbo"."refDistributors" N1 with (nolock) on (N0."idDistributor" = N1."id"))
 inner join "dbo"."Enums" N2 with (nolock) on (N0."idBaseObject" = N2."id"))
 inner join "dbo"."Enums" N3 with (nolock) on (N0."idAttrType" = N3."id"))
where ((N2."CodeKey" = @p0) and (N3."CodeKey" <> N'Attrs_Type_Periodic') and (N0."id" <> @p1) and (N0."deleted" = @p2) and N0."idDistributor" in (@p3,@p4))

/*
declare
@p0 nvarchar(max)=N'Attrs_ObjectType_dhPreorder',
@p1 bigint=0,
@p2 bit=0

select N0."id",N0."deleted",N0."Name",N0."idValueType",N0."idBaseObject",N0."Position",N0."PDAEditPos",N0."PDAViewPos",N0."idAttrType",N0."idDistributor",N1."ObjectType",N0."IsBrowsable",N0."IsEditable",N0."Code",N0."verstamp" from ((("dbo"."refAttributesBase" N0 with (nolock)
 left join "dbo"."refDistributors" N1 with (nolock) on (N0."idDistributor" = N1."id"))
 inner join "dbo"."Enums" N2 with (nolock) on (N0."idBaseObject" = N2."id"))
 inner join "dbo"."Enums" N3 with (nolock) on (N0."idAttrType" = N3."id"))
where ((N2."CodeKey" = @p0) and (N3."CodeKey" <> N'Attrs_Type_Periodic') and (N0."id" <> @p1) and (N0."deleted" = @p2))
*/
/*
declare
@p0 bigint=562954286512166
select N0."id",N0."deleted",N0."idAttribute",N0."Value",N0."IsSelectedByDefault",N0."verstamp" from "dbo"."refAttributesListValues" N0 with (nolock)
where (N0."idAttribute" = @p0)
*/
/*
declare
@p0 bigint=0,
@p1 bit=0
select N0."id",N0."deleted",N0."idAttribute",N0."Value",N0."IsSelectedByDefault",N0."verstamp" from "dbo"."refAttributesListValues" N0 with (nolock)
where ((N0."id" <> @p0) and (N0."deleted" = @p1))
*/
/*
declare
@p0 bigint=281474990447600 ,
@p1 bigint= 562954286502161,
@p2 bigint=562954286502167
select N0."id",N0."deleted",N0."Name",N0."idValueType",N0."idBaseObject",N0."Position",N0."PDAEditPos",N0."PDAViewPos",N0."idAttrType",N0."idDistributor",N1."ObjectType",N0."IsBrowsable",N0."IsEditable",N0."Code",N0."verstamp" from ("dbo"."refAttributesBase" N0 with (nolock)
 left join "dbo"."refDistributors" N1 with (nolock) on (N0."idDistributor" = N1."id"))
where N0."id" in (@p0,@p1,@p2)
*/
/*
declare
@p0 bigint=281474992219555
select N0."id",N0."deleted",N0."idAttribute",N0."Value",N0."IsSelectedByDefault",N0."verstamp" from "dbo"."refAttributesListValues" N0 with (nolock)
where (N0."idAttribute" = @p0)
*/

/*
declare
@p0 bigint=281474992219567,
@p1 bigint=0,
@p2 bit=0
select N0."id",N0."deleted",N0."idAttribute",N0."Value",N0."IsSelectedByDefault",N0."verstamp" from "dbo"."refAttributesListValues" N0 with (nolock)
where ((N0."idAttribute" = @p0) and (N0."id" <> @p1) and (N0."deleted" = @p2))
*/

------------------------------------------------------------

select
	*
from
	dbo.refAttributesValues av
	join dbo.refPromo p on p.id = av.idElement
	join dbo.XPObjectType ot on ot.OID = av.ObjectType
where
	ot.TypeName = N'Chicago2.Core.StuBaseCore.StuObjects.StuPeriodicAttributesValuePromo'
order by p.id, av.StartDate
