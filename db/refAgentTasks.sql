select
	*
from
	dbo.refAgentTasks agentTasks
	join dbo.refSets [sets] on [sets].idItem = agentTasks.id
	join dbo.refOutletSets outletSets on outletSets.idSet = [sets].id
	join dbo.refOutlets outlets on outlets.id = outletSets.idOutlet