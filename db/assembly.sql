alter database [ch] set trustworthy on
alter authorization on database::[ch] to sa
go

if object_id(N'dbo.sp_ReadExcel', N'pc') is not null
	drop procedure dbo.sp_ReadExcel
go

if exists (select 1 from sys.assemblies asms where (asms.name=N'fn_ReadExcel') and (is_user_defined=1))
	drop assembly fn_ReadExcel
go

create assembly fn_ReadExcel from 'D:\WorkSpaces\CHICAGO\DEV\db\src\ReadExcel\fn_ReadExcel\bin\Release\fn_ReadExcel.dll' with permission_set=unsafe
go

CREATE PROCEDURE [dbo].[sp_ReadExcel]
	@isFirstRowColumnNames [bit],
	@FileName [nvarchar](1000),
	@SQLTableName [nvarchar](1000),
	@SheetName [nvarchar](1000),
	@ColumnCount [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [fn_ReadExcel].[fn_ReadExcel.ReadExcel].[LoadExcel]
GO
