select
	*
from
	dbo.DocJournal docJournal
	join dbo.dhMovement dhMovement on dhMovement.id = docJournal.id
	join dbo.drMovement drMovement on drMovement.idDoc = dhMovement.id
	join dbo.refGoods goods on goods.id = drMovement.idGoods
	join dbo.EnumsBase enumsItemType on enumsItemType.id = drMovement.idItemType
where
	cast(docJournal.OpDate as date) = N'20151204'

select
	*
from
	dbo.refGoods
where
	id in (562954286636416, 281474993324327)

-- update dbo.refGoods set idGoodsCO = 281474993324327, Name = N'47486 (002)' where id = 562954286636416