﻿select
	vOutletGridFields.FullAddress,
	docJournal.idOutlet,
	count(*)
from
	dbo.DocJournal docJournal
	join dbo.vOutletGridFields vOutletGridFields on vOutletGridFields.id = docJournal.idOutlet
group by vOutletGridFields.FullAddress, docJournal.idOutlet
having count(*) > 1


select * from dbo.DocJournal where PrintDocNum = N'0020000000000002'
select * from dbo.DocJournal where id = 0 562954286282192

select
	*
from
	[dbsrv].[ch_mt_demo].dbo.DocJournal docJournal
where
	docJournal.id < 281474976710656
and docJournal.id != 0

--update dbo.DocJournal set idCreator = 0 where id = 562954286282192
--update dbo.DocJournal set idPhysicalPerson = 0 where id = 562954286282192

;with cte (idPreviousVersion)
as
(
	select
		d.idPreviousVersion
	from
		dbo.DocJournal d
	where
		d.PrintDocNum = N'235235423'
	group by d.idPreviousVersion
	having count(d.idPreviousVersion) > 1
)
select
	*
from
	dbo.DocJournal d
	join cte cte on cte.idPreviousVersion = d.idPreviousVersion
order by d.idPreviousVersion

------------------------------------------------------------

select
	*
from
	dbo.DocJournal dj
	--join dbo.dhPreOrder dh on dh.id = dj.id
	--join dbo.drPreOrder dr on dr.idDoc = dh.id
	join dbo.refDistributors d on d.NodeID = cast(dj.id/281474976710656 as smallint)
where
	dj.PrintDocNum = N'4210000000000001'
	--dj.id = 41940892519854262
	and d.deleted=0

select
	*
from
	dbo.DocJournal dj
	join dbo.dhPreOrder dh on dh.id = dj.id
	join dbo.drPreOrder dr on dr.idDoc = dh.id
	left join dbo.refOutercodes oc on idItem = dj.id
where
	cast(dj.OpDate as date) = N'20151005'

select * from dbo.drPreOrder where idItemType is null
select * from dbo.drSales where idItemType is null

select
	ds.Name,
	*
from
	dbo.DocJournal dj
	join dbo.enDocStatusBase ds on ds.id=dj.idStatus
	join dbo.dhGoodsReturn dhGR on dhGR.id=dj.id
	join dbo.drGoodsReturn drGR on drGR.idDoc=dhGR.id
where
	cast(dj.DateTimeStamp as date)=N'20131202'
order by dj.DateTimeStamp desc

select
	*
from
	dbo.DocJournal dj
where
	dj.deleted = 0
order by dj.OpDate desc

select * from dbo.enOperationTypesBase where id=23

/*

declare
	@id bigint,
	@dt datetime = N'20141104 13:13:13',
	@n nvarchar(50) = N'0020000000000018'

set @id = dbo.fn_getIdEx(281474976711134,1)

insert into DocJournal
(id, PrintDocNum, OpDate, DateTimeStamp, idStatus, idCreator, idClient, idBuyPoint, Amount, idOperation, idBaseDoc, idFirstVersion, idPreviousVersion, PDADocNum, idRoute, outercode, deleted, PrnDocNum, CrDate, idPhysicalPerson, idVisit, idAction, idPosition, Comment, isEditOut, ObjectType, idExchangeState, idBusinessStatus, IsSent, StatusSentMail)
values
(@id, @n, @dt, @dt, 1, 0, 562954253057668, 562954253069306, 10.0000, 23, 0, @id, 0, 0, 562954254558294, N'', 0, N'', @dt, 281474976711442, 0, 0, 281474976711444, N'', 0, NULL, 2, 0, 0, NULL)

insert into dbo.dhGoodsReturn
(OpDate, id, idCurrency, idBuyer, idBuyPoint, idCounteragent, CurrencyRate, idRoute, idFirm, idPayType, idStore)
values
(@dt, @id, 281474976762064, 562954253069273, 562954253069306, 562954253057668, 1.0000, 562954254558294, 562954248541995, 1, 562954286553684)

insert into drGoodsReturn
(id, idDoc, idGoods, idUnit, UnitFactor, Price, Quantity, Amount, idStatus, Overdue, VATAmount, ItemGroupNum, idItemType)
values
(dbo.fn_getIdEx(281474976711134,1),	@id, 562954286528188, 562954261194299, 1.000000, 10.0000, 1.000000, 10.0000, 0, 0, 0.0000, 1, 766)

*/

/*

declare
	@id bigint = 18858823439617958

select * from dbo.DocJournal where id = @id
select * from dbo.dhPreOrder where id = @id
select * from dbo.drPreOrder where idDoc = @id
select * from dbo.refOutercodes where idItem = @id

delete
	dbo.drPreOrder
from
	dbo.drPreOrder dr
	join dbo.dhPreOrder dh on dr.idDoc = dh.id
where
	dh.id = @id

delete
	dhPreOrder
from
	dhPreOrder dh
	join dbo.DocJournal dj on dh.id = dj.id
where
	dj.id = @id

delete from dbo.DocJournal where id = @id

select * from dbo.DocJournal where id = @id
select * from dbo.dhPreOrder where id = @id
select * from dbo.drPreOrder where idDoc = @id
select * from dbo.refOutercodes where idItem = @id

*/

select
	*
from
	dbo.DocJournal dj
	join dbo.dhSales dhS on dhS.id = dj.id
	join dbo.refPayTypes payType on payType.id = dhS.idPayType
	join dbo.drSales drS on drS.idDoc = dhS.id
	join dbo.refPriceTypes priceType on priceType.id = drS.idPriceType 
	join dbo.refPriceTypes basePriceType on basePriceType.id = priceType.idBasePriceType 
	join dbo.refPriceList priceList on priceList.idGoods = drS.idGoods and priceList.idPayType = dhS.idPayType and priceList.idPriceType = basePriceType.id
	join dbo.refDistributors d on d.NodeID = cast(dj.id/power(cast(2 as bigint), cast(48 as bigint)) as bigint)
	join dbo.refGoods g on g.id = drS.idGoods
where
	dj.PrintDocNum = N'2015030401'
	and dj.deleted = 0
	and payType.deleted = 0
	and priceType.deleted = 0
	and basePriceType.deleted = 0
	and priceList.deleted = 0
	and d.deleted = 0
	and g.deleted = 0

------------------------------------------------------------

select
	*
from
	dbo.DocJournal dj
	left join dbo.refOutercodes ocdj on ocdj.idItem = dj.id
where
	dj.id in (562954286557720, 562954286561746)

declare
	--@id bigint = 562954286557720,
	@id bigint = 562954286561746,
	--@outercode nvarchar(50) = N'blah-blah-blah',
	--@outercode nvarchar(50) = N'outercode 4 0020000000000070',
	@outercode nvarchar(50) = N'outercode 4 0020000000000112',
	@tableName nvarchar(50) = N'DocJournal'

;merge into dbo.refOutercodes as tgt
using
(
	select
		dj.id,
		d.id as idDistr,
		(select OID from dbo.XPObjectType where TypeName = N'Chicago2.Core.xpoobjects.References.OutercodeDocJournal') as OID
	from
		dbo.DocJournal dj
		join dbo.refDistributors d on d.NodeID = cast(dj.id/281474976710656 as smallint)
	where
		dj.id = @id
		and d.deleted=0
) as src
on tgt.idItem = src.id and tgt.idDistr = src.idDistr and tgt.TableName = @tableName and tgt.ObjectType = src.OID
when matched and tgt.outercode != @outercode
	then
		update
			set
				outercode = @outercode
when not matched
	then
		insert
			(id, idDistr, idItem, TableName, outercode, ObjectType)
		values
			(dbo.fn_getIdEx(src.idDistr, 1), src.idDistr, src.id, @tableName, @outercode, src.OID);

select
	*
from
	dbo.DocJournal dj
	left join dbo.refOutercodes ocdj on ocdj.idItem = dj.id
where
	dj.id in (562954286636419)

-- update dbo.refOutercodes set idItem = idItem + 1 where id = 562954286638422

select
	*
from
	dbo.DocJournal docJournal
	left join dbo.enOperationTypesBase operationTypesBase on operationTypesBase.id = docJournal.idOperation
where
	docJournal.id in (562954286185732, 562954286283464, 562954286636419)
