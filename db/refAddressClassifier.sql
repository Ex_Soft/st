select
	*
from
	dbo.refAddressClassifier addressClassifier
where
	addressClassifier.deleted = 0
	and addressClassifier.name = N'������'

/* down */
;with cteDown(id, idParent, name, level, path)
as
(
	select
		t.id,
		t.idParent,
		t.name,
		0 as level,
		cast(N'/' + t.name as varchar(max)) as path
	from
		dbo.refAddressClassifier t
	where
		t.id = 281474977544294
		and t.deleted = 0
	union all
	select
		t.id,
		t.idParent,
		t.name,
		level + 1 as level,
		cast(path + N'/' + t.name as varchar(max)) as path
	from
		cteDown cteDown
		join dbo.refAddressClassifier t on t.idParent = cteDown.id
	where
		t.deleted = 0
)
select
	*
from
	cteDown
where
	cteDown.name = N'4807-�'

select
	*
from
	dbo.refAddressClassifier addressClassifier
	left join dbo.refAddressAltNames addressAltNames on addressAltNames.idAddress = addressClassifier.id
where
	addressClassifier.id = 281474978216952
