﻿select
	*
from
	dbo.refDC dc
	join dbo.refOutlets outlets on outlets.id = dc.idOutlet
	join dbo.vOutletGridFields vOutletGridFields on vOutletGridFields.id = outlets.id
where
	--dc.id = 562954287884144
	dc.Name = N'66497 Дочерняя есть в составе ДЦ, а родительская была раньше в составе ДЦ'

select * from dbo.refOutlets where Name in (N'66497 (main) (4 test)', N'66497 (main)')
/*
update dbo.refOutlets set deleted = 0, idDuplicateOf = id where Name = N'66497 (main)'
update dbo.refDC set idOutlet = (select id from dbo.refOutlets where Name = N'66497 (main)')  where Name = N'66497 Дочерняя есть в составе ДЦ, а родительская была раньше в составе ДЦ'
*/

select
	*
from
	dbo.refDC dc
	join dbo.refDCOutlet dcOutlet on dcOutlet.idDC = dc.id
	join dbo.refOutlets outlets on outlets.id = dcOutlet.idOutlet
	join dbo.vOutletGridFields vOutletGridFields on vOutletGridFields.id = outlets.id
where
	--dc.id = 562954287884144
	dc.Name = N'66497 Дочерняя есть в составе ДЦ, а родительская была раньше в составе ДЦ'

/*
update dbo.refDCOutlet set deleted = 1 from dbo.refDCOutlet dcOutlet join dbo.refDC dc on dc.id = dcOutlet.idDC join dbo.refOutlets outlets on outlets.id = dcOutlet.idOutlet where dc.Name = N'66497 Дочерняя есть в составе ДЦ, а родительская была раньше в составе ДЦ' and outlets.Name = N'66497 (master) (deleted)'
update dbo.refDCOutlet set deleted = 0 from dbo.refDCOutlet dcOutlet join dbo.refDC dc on dc.id = dcOutlet.idDC join dbo.refOutlets outlets on outlets.id = dcOutlet.idOutlet where dc.Name = N'66497 Дочерняя есть в составе ДЦ, а родительская была раньше в составе ДЦ' and outlets.Name = N'66497 (slave)'
update dbo.refOutlets set deleted = 0, idDuplicateOf = id where Name = N'66497 (slave)'
*/