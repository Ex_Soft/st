select
	*
from
	dbo.DocJournal docJournal
	join dbo.dhGoodsReturn dhGoodsReturn on dhGoodsReturn.id = docJournal.id
where
	--cast(docJournal.OpDate as date) = N'20161123'
	--docJournal.PrintDocNum = N'0020000000000129'
	docJournal.id = 562954287603227

select
	cast(docJournal.id/281474976710656 as smallint),
	*
from
	dbo.DocJournal docJournal
	join dbo.dhGoodsReturn dhGoodsReturn on dhGoodsReturn.id = docJournal.id
	join dbo.refAttributesValues attributesValues on attributesValues.idElement = docJournal.id
	/*join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
	join dbo.XPObjectType XPObjectType on XPObjectType.OID = attributesValues.ObjectType
	join dbo.refFilePackets filePackets on filePackets.id = attributesValues.idPacket
	join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idPacket = filePackets.id
	join dbo.refFiles files on files.id = filePacketsFiles.idFile*/
where
	--docJournal.id = 562954287603227
	docJournal.idFirstVersion = 562954287603227
	and docJournal.idStatus = 1

select
	*
from
	dbo.DocJournal docJournal
	join dbo.dhGoodsReturn dhGoodsReturn on dhGoodsReturn.id = docJournal.id
	join dbo.drGoodsReturn drGoodsReturn on drGoodsReturn.idDoc = dhGoodsReturn.id
	join dbo.refAttributesValues attributesValues on attributesValues.idElement = drGoodsReturn.id
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
	join dbo.XPObjectType XPObjectType on XPObjectType.OID = attributesValues.ObjectType
where
	--docJournal.id = 562954287603227
	docJournal.idFirstVersion = 562954287603227
