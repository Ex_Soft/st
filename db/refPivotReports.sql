﻿select
	*
from
	dbsrv.ch_demo_2_22_2_x.dbo.refPivotReports pivotReports
where
	pivotReports.id = 18577348463040161
	pivotReports.CodeKey = N'report_xls_PhotoAudit'
	and pivotReports.deleted = 0


--update dbo.refPivotReports set ExcelOnly = 1 where CodeKey = N'report_xls_gpsControl'

select
	*
from
	dbo.refPivotReports pr
	join dbo.refPivotReportTemplates prt on prt.idPivotReport = pr.id
	join dbo.refStoredProcedures sp on sp.id = pr.idStoredProcedure
	join dbo.refStoredProceduresParam spp on spp.idStoredProcedure = sp.id
	join Enums e on e.id=spp.idMetaData
where
	pr.CodeKey = N'report_xls_BuyPoints_on_Route'
	and pr.deleted = 0
	and prt.deleted = 0
	and sp.deleted = 0
	and spp.deleted = 0

select * from dbo.refStoredProcedures where CodeKey = N'report_xls_BuyPoints_on_Route'
--update dbo.refStoredProcedures set spName = N'Reports.report_xls_BuyPoints_on_Route' where CodeKey = N'report_xls_BuyPoints_on_Route'
------------------------------------------------------------

select
	*
	--dictionaryBase.*
from
	dbo.refPivotReports pivotReports
	join dbo.refDictionaryBase dictionaryBase on dictionaryBase.ReportName = pivotReports.CodeKey
where
	pivotReports.CodeKey = N'report_xls_SalesBuyPoints'
order by dictionaryBase.Value

------------------------------------------------------------

select
	*
from
	dbo.refPivotReports pr
	join dbo.refPivotReportRights prr on prr.idPivotReport = pr.id
	join dbo.refRoles r on r.id = prr.idRole
where
	pr.CodeKey in (N'report_xls_Import_RoutesBP', N'Импорт торговых точек в маршрут')

------------------------------------------------------------

select
  prt.Template
from
  refPivotReportTemplates prt
  join refPivotReports pr on pr.id=prt.idPivotreport
where
  (prt.deleted = 0)
  and (pr.deleted = 0)
  and (prt.Name = N'Импорт дневных посещений')
  and (pr.Name = N'Отчет Импорт дневных посещений из Excel')
for xml RAW, BINARY BASE64
------------------------------------------------------------

select
  prt.Template
from
  refPivotReportTemplates prt
where
  (prt.id=562954286501166)
for xml RAW, BINARY BASE64

delete from refPivotReportTemplates where id=562954286501166

------------------------------------------------------------

select
  *
from
  refStoredProcedures sp
  join refStoredProceduresParam spp on (spp.idStoredProcedure=sp.id)
  join Enums e on (e.id=spp.idMetaData)
  join refPivotReports pr on (pr.idStoredProcedure=sp.id)
  join refPivotReportTemplates prt on (prt.idPivotReport=pr.id)
where
  (sp.deleted=0)
  and (spp.deleted=0)
  and (pr.deleted=0)
  and (prt.deleted=0)
  and (sp.Name in (N'Отчет Импорт Торговых Точек', N'Отчет Импорт дневных посещений'))

------------------------------------------------------------
declare
  @result int

exec @result = dbo.sp_ReadExcel
   @isFirstRowColumnNames=1
 , @FileName=N'D:\temp\TestImportRoutesBPWithError.xlsx'
 , @SQLTableName=N'TestTestTest'
 , @SheetName=N''
 , @ColumnCount=0

select * from TestTestTest

if object_id(N'TestTestTest', N'U') is not null
	drop table TestTestTest
else
	print N'Tampax'
------------------------------------------------------------

declare
	@idRoute bigint=562954254558294
	,@FileName nvarchar(1000)=N'd:\temp\TestImportRoutesBP.xlsx'
	,@idPhysicalPerson bigint=0
	,@guid nvarchar(50)=newid()

exec reports.report_xls_Import_RoutesBP @idRoute, @FileName, @idPhysicalPerson, @guid

------------------------------------------------------------

/* set language russian; */
/* set language english; */

if object_id(N'_exportXlsx_001', N'u') is not null
	drop table _exportXlsx_001

delete from
	rpTempTables

declare 
		@enPlannings		varchar(max),		--Перечисление планов
		@idPhysicalPerson	bigint,				--Код сотрудника создающий отчет
		@guid				nvarchar(50),		--! ключ уникальности, используется для временных таблиц	 
		@need_empty			bit,			    --Признак вывода, 1 - шаблон, 0 - данные 
		@Table_Test			nvarchar(50)        --Название таблицы, куда будет выгружены данные отчета, для тестирования
select
		@enPlannings		= '562949953433355',		--Перечисление планов
		@idPhysicalPerson	= 0,				--Код сотрудника создающий отчет
		@guid				= newid(),		--! ключ уникальности, используется для временных таблиц	 
		@need_empty			= 1,			--Признак вывода, 1 - шаблон, 0 - данные 
		@Table_Test			= NULL --Название таблицы, куда будет выгружены данные отчета, для тестирования

exec Reports.report_xls_BrowsePlannings @need_empty=@need_empty --@enPlannings=@enPlannings

/* select * from refDictionaryBase WHERE [ReportName] = 'report_xls_BrowsePlannings' order by Value */

/* select * from refPlanning */

------------------------------------------------------------
