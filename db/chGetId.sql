select
	*
from
	dbo.chGetId
where
	idRoute = 0

select
	idDistr,
	idRoute,
	count(*)
from
	dbo.chGetId
group by idDistr, idRoute
having count(*) < 1

select
	*
from
	LogDataExchangeAudit logDataExchangeAudit
	join dbo.refDistributors distributors on distributors.NodeID = cast(logDataExchangeAudit.id/281474976710656 as smallint)
where
	logDataExchangeAudit.id between 304246377920942700 and 304246377920942910

declare	@idDistr bigint = 281474976711134
declare @newId bigint = dbo.fn_getIdEx(@idDistr, 1)

select
	@newId,
	LastId,
	@newId - LastID,
	*
from
	dbo.chGetId
where
	idDistr = @idDistr

/*
begin transaction
update dbo.chGetId set LastID = LastID + 10000 where idDistr = @idDistr and idRoute = 0
exec dbo.ReInit(@idDistr)
commit transaction
*/

select
	*
from
	LogDataExchangeAudit logDataExchangeAudit
	join dbo.refDistributors distributors on distributors.NodeID = cast(logDataExchangeAudit.id/281474976710656 as smallint)
where
	logDataExchangeAudit.id between 304246377920942700 and 304246377920942910

;with cte
as
(
	select
		cast([log].NewValue as bigint) as NewValue,
		[log].ChangeDate,
		[log].TableName
	from
		dbo.v_LogDataChange [log] with (nolock)
	where
		[log].OperationType = N'I'
		and [log].FieldName = N'id'
)
select
	[log].NewValue,
	chGetId.LastID,
	[log].ChangeDate,
	[log].TableName,
	*
from
	cte [log]
	join dbo.refDistributors distributors with (nolock) on distributors.NodeID = cast([log].NewValue/281474976710656 as smallint)
	join dbo.chGetId chGetId with (nolock) on chGetId.idDistr = distributors.id
where
	chGetId.idRoute = 0
	and distributors.deleted = 0
	and [log].NewValue > chGetId.LastID + 1000

