﻿select * from dbo.refBuyPoints where idMain in (281474992219542, 281474992219585)
select * from dbo.refSegments where Name = N'70963 Зависает Чикаго при редактировании маршрута'

--insert into dbo.refRouteTerritory (id, idRoute, idBuyPoint, idSegment, deleted) values (dbo.fn_getIdEx(281474976711134, 1), 562954287836083, 281474992312445, 0, 0)
--insert into dbo.refRouteTerritory (id, idRoute, idBuyPoint, idSegment, deleted) values (dbo.fn_getIdEx(281474976711134, 1), 562954287836083, 0, 562954287838087, 0)
--delete from dbo.refRouteTerritory where idRoute = 562954287836083 and idBuyPoint in (281474992219585, 562954286511220)
select * from dbo.refBuypointPositions buypointPositions where buypointPositions.idPosition = 281474977159395 and buypointPositions.idBuypoint in (281474992219542, 562954286511217, 281474992219585, 562954286511220)
-- delete from dbo.refBuypointPositions where idPosition = 281474977159395 and idBuypoint in (281474992219542, 562954286511217)

select
	*
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	left join dbo.refBuypointPositions buypointPositions on buypointPositions.idPosition = [routes].idPosition and buypointPositions.idBuypoint = routeTerritory.idBuyPoint and buypointPositions.deleted = 0
where
	[routes].Name = N'70963 Зависает Чикаго при редактировании маршрута'
	and routeTerritory.idBuyPoint != 0
	and routeTerritory.deleted = 0
	and buypointPositions.id is null

select
	*
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
where
	[routes].Name = N'70963 Зависает Чикаго при редактировании маршрута'
	and routeTerritory.idBuyPoint != 0
	and routeTerritory.deleted = 0
	and not exists (select 1 from dbo.refBuypointPositions buypointPositions where buypointPositions.idPosition = [routes].idPosition and buypointPositions.idBuypoint = routeTerritory.idBuyPoint and buypointPositions.deleted = 0)

select * from dbo.refRoutes where Name = N'CH 3'

select
	*
from
	dbo.refRoutes [routes]
	left join dbo.rgPDAInfo pdaInfo on pdaInfo.idRoute = [routes].id and pdaInfo.deleted = 0
where
	[routes].deleted = 0
	and [routes].idDistributor = 281474976711134 -- 281474976710657

--insert into dbo.rgPDAInfo (idRoute, DeviceID, ModelName, CpuFrequency, MainMemSize, MainMemSpace, FlashMemSize, FlashMemSpace, AppFirstStarted, VerMajor, VerMinor, SubVerMajor, SubVerMinor, deleted) values (2251799820118083, N'4G23DW38500C', N'', 0, 83782652, 55455136, 0, 0, getdate(), 4, 0, 49, 0, 0)

select * from dbo.refRoutes where deleted = 0 and IsCanConvertToSOCO = 1
--update dbo.refRoutes set IsCanConvertToSOCO = 0, IsConvertToSOCO = 0
--update dbo.refRoutes set IsCanConvertToSOCO = 1, IsConvertToSOCO = 1 where id = 562954287599199
select * from dbo.refDistributors where AutoConvertRoutes = 1
--update dbo.refDistributors set AutoConvertRoutes = 0

select
	*
from
	ch_old.dbo.refRoutes [routes]
	join ch_old.dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	join ch_old.dbo.refBuyPoints buyPoints on buyPoints.id = routeTerritory.idBuyPoint
where
	[routes].Name = N'Route #1 (CH3)'
	--[routes].Name like N'%54859%'
	and routeTerritory.deleted = 0

--insert into ch_old.dbo.refRouteVisits (id, idRoute, idBuyPoint, Visit, VisitDate, VisitOrder, deleted) values (dbo.fn_getIdEx(281474976711134, 1), 562954287821058, 281474992361444, null, N'20170918', 2, 0)
--update ch_old.dbo.refRouteVisits set deleted = 1 where id in (562954287822065, 562954287822063)

select
	*
from
	ch_old.dbo.refRoutes [routes]
	join ch_old.dbo.refRouteVisits routeVisits on routeVisits.idRoute = [routes].id
	join ch_old.dbo.refBuyPoints buyPoints on buyPoints.id = routeVisits.idBuyPoint
where
	[routes].Name = N'Копирование календарем в маршруте создает дубли ТТ'
	--[routes].Name like N'%54859%'
	--routeVisits.Visit = 0
	--and routeVisits.VisitDate is not null
	--and routeVisits.deleted = 0
order by routeVisits.Visit, routeVisits.VisitOrder

/*
delete from dbo.refRouteTerritory where deleted = 1

delete
  dbo.refRouteTerritory
from
	dbo.refRouteTerritory routeTerritory
	left join dbo.refOutlets outlets on outlets.id = routeTerritory.idOutlet
where
	outlets.id is null;
*/

/*
delete from dbo.refRouteVisits where deleted = 1

delete
  dbo.refRouteVisits
from
	dbo.refRouteVisits routeVisits
	left join dbo.refOutlets outlets on outlets.id = routeVisits.idOutlet
where
	outlets.id is null;
*/

select
	*
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	--join dbo.refRouteVisits routeVisits on routeVisits.idRoute = [routes].id
	--join dbo.refBuyPoints buyPoints on buyPoints.id = routeVisits.idBuyPoint
	left join dbo.refOutlets outlets on outlets.id = routeTerritory.idOutlet
where
	outlets.id is null;

--update dbo.refRouteVisits set deleted = 0 where id in (562954286651511, 562954286651517)

/*
update
	dbo.refRouteVisits
set
	Visit = 0
from
	dbo.refRoutes [routes]
	join dbo.refRouteVisits routeVisits on routeVisits.idRoute = [routes].id
where
	[routes].Name = N'With add visits with Visit equ 0'
	and routeVisits.deleted = 0
	and routeVisits.Visit is null

update dbo.refRouteVisits set Visit = null where id = 562954286612237

*/

-----------------------------------------------------------

;with buyPointsInSegments (idBuyPoint)
as
(
select
	segmentBuyPoints.idBuyPoint
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	join dbo.refSegments [segments] on [segments].id = routeTerritory.idSegment
	join dbo.refSegmentBuyPoints segmentBuyPoints on segmentBuyPoints.idSegment = [segments].id
where
	[routes].Name = N'Some sales outlets both in territory && in segment'
	and routeTerritory.deleted = 0
	and [segments].deleted = 0
	and segmentBuyPoints.deleted = 0
)
select
	*
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	join buyPointsInSegments buyPointsInSegments on buyPointsInSegments.idBuyPoint = routeTerritory.idBuyPoint
	join dbo.refBuyPoints buyPoints on buyPoints.id = routeTerritory.idBuypoint
where
	[routes].Name = N'Some sales outlets both in territory && in segment'
	and routeTerritory.deleted = 0

-----------------------------------------------------------

select
	*
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	join dbo.refSegments [segments] on [segments].id = routeTerritory.idSegment
	join dbo.refSegmentBuyPoints segmentBuyPoints on segmentBuyPoints.idSegment = [segments].id
	join dbo.refBuyPoints buyPoints on buyPoints.id = segmentBuyPoints.idBuypoint
where
	[routes].Name = N'48600 [рег.тест] Экспортируются ТТ удаленного сегм'
	--and routeTerritory.deleted = 0
	and [segments].deleted = 0
	and segmentBuyPoints.deleted = 0
	and buyPoints.deleted = 0
order by routeTerritory.idSegment, segmentBuyPoints.idBuypoint
------------------------------------------------------------

--update dbo.refRouteTerritory set deleted = 0 where id = 562954286651504

select
	*
from
	dbo.refRoutes routes
	join dbo.refSets [sets] on [sets].idItem = routes.id
	join dbo.refStoreSets storeSets on storeSets.idSet = [sets].id
where
	routes.Name = N'Маршрут с сетами складов'

------------------------------------------------------------

select
	*
from
	dbo.refRoutes r
	join dbo.refRouteVisits rv on rv.idRoute = r.id
	join dbo.EnumsBase eRWCT on eRWCT.id = r.idRouteWeekCountType
	join dbo.ConstsBase c on c.idDistr = r.idDistributor and c.CodeKey = N'RouteWeekCount'
	join dbo.EnumsBase e on e.id = c.Value
where
	r.id = 562954286321863
	and rv.deleted = 0
	and rv.Visit = 17

------------------------------------------------------------

select
	*
from
	dbo.ConstsBase c
	join dbo.EnumsBase eg on eg.id = c.idGroup
	join dbo.EnumsBase ev on ev.id = cast(c.Value as bigint)
where
	c.Deleted = 0
	and c.CodeKey = N'PreorderRestsStorageType'

select
	*
from
	dbo.refRoutes r
	left join dbo.refSets s on s.idItem = r.id
	left join dbo.refStoreSets ss on ss.idSet = s.id
	left join dbo.refStores stores on stores.id = ss.idStore and stores.deleted = 0
	left join dbo.enStoreTypesBase stb on stb.id = ss.idStoreType
where
	r.Code = N'15042601'

------------------------------------------------------------

select
	*
from
	dbo.refRoutes r
	join dbo.refRouteTerritory rt on rt.idRoute = r.id
	join dbo.refBuyPoints bp on bp.idMain = rt.idBuyPoint
where
	r.id = 93731167351516773
	and rt.deleted = 0

select
	vRV.idBuyPoint,
	*
from
	refRouteVisits rv
	join vRouteVisits vRV on (vRV.id=rv.idvRouteVisits)
where
	--(rv.idBuyPoint=24769797950787682)
	(rv.id=24769797962347541)

select
	vRT.idBuyPoint,
	*
from
	refRouteTerritory rt
	join vRouteTerritory vRT on (vRT.id=rt.idvRouteTerritory)
where
	(rt.idRoute=25614222880672720)
	and (rt.idBuyPoint in (24769797950787682, 24769797950787681, 25332747905533046))

select
	bp.id,
	bp.idDistributor,
	bp.isLocal,
	d.Name,
	*
from
	refBuyPoints bp
	join refDistributors d on (d.id=bp.idDistributor)
where
	(bp.idMain=24769797950787682)
order by bp.id

select
	vBP.id,
	vBP.idDistributor,
	vBP.Distributors,
	*
from
	vBPGridFields vBP
where
	(vBP.id in (24769797950787682, 24769797950787681, 25332747905533046))
order by vBP.id

------------------------------------------------------------

select
	bp.Name,
	*
from
	refRouteVisits rv
	join vRouteVisits vRV on (vRV.id=rv.idvRouteVisits)
	join refBuyPoints bp on (bp.id=vRV.idBuyPoint)
where
	(rv.idRoute=25614222880672720)
	--and (rv.Visit=7)
	--and (rv.Visit=6)
	and (rv.Visit=13)
	--and (rv.deleted=0)
order by rv.VisitOrder
/*
delete from refRouteVisits where id in (24769797962347532, 24769797962347533)
*/

/*
select
	*
from
	refBuyPoints bp
where
	(bp.idMain=24769797961796296)
*/

select * from dbo.EnumsBase where CodeKey like N'ConstType!_RouteWeekCount!_%' escape N'!' or CodeKey = N'Route_WeekCountType_Const'