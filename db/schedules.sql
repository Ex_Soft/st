/* delete from dbo.refWorkSchedules where id!=0 */
/* delete from dbo.refWorkSchedules where id=562954286514173 */
/* update dbo.refWorkSchedules set idDistributor=281474976710657 where id=562954286514181 */
select
	*
from
	dbo.refWorkSchedules

/* delete from dbo.refWorkScheduleWeekdays where idWorkSchedule!=0 */
/* delete from dbo.refWorkScheduleWeekdays where idWorkSchedule=562954286514173 */
/* select * from dbo.refWorkScheduleWeekdays */

/* delete from dbo.refWorkSchedulePeriods where id!=0 */
/* delete from dbo.refWorkSchedulePeriods where idWorkSchedule=562954286514165 */
select
	wsp.*,
	pp.Name,
	d.Name as DistributorName,
	ws.Name
from
	dbo.refWorkSchedulePeriods wsp
	join dbo.refWorkSchedules ws on (ws.id=wsp.idWorkSchedule)
	left join dbo.refPhysicalPersons pp on (pp.id=wsp.idEmployee)
	left join dbo.refDistributors d on (d.id=wsp.idDistributor)

/* delete from dbo.refWorkScheduleAlterations */
/* delete from dbo.refWorkScheduleAlterations where id in (20547673299889901) */
/* update dbo.refWorkScheduleAlterations set idDayType = 2401, StartTime = cast(StartTime as date), EndTime = N'19000101' where id = 281475012857429 */
/* update dbo.refPhysicalPersons set Name = N'4 Test WorkSchedule I Employee', Lastname = N'4 Test WorkSchedule I Employee' where id = 20547673299878889 */

select
	*
from
	dbo.refWorkScheduleAlterations

select
	*
from
	dbo.refWorkScheduleAlterations workScheduleAlterations with (nolock)
	left join dbo.refDistributors distributors with (nolock) on distributors.id = workScheduleAlterations.idDistributor
	left join dbo.refPhysicalPersons physicalPersons with (nolock) on physicalPersons.id = workScheduleAlterations.idEmployee
	join dbo.EnumsBase enumsDayType with (nolock) on enumsDayType.id = workScheduleAlterations.idDayType
where
	workScheduleAlterations.[Date] = N'20170224'


/* select * from syscolumns where id=object_id(N'refWorkScheduleAlterations') */

/*
if not exists(select 1 from syscolumns where id=object_id(N'refWorkScheduleAlterations') and name=N'idWorkSchedule')
	begin
		alter table dbo.refWorkScheduleAlterations add idWorkSchedule bigint null
		alter table dbo.refWorkScheduleAlterations add constraint FK_refWorkScheduleAlterations_refWorkSchedules foreign key (idWorkSchedule) references dbo.refWorkSchedules(id)
	end
*/

/* delete from dbo.refWorkScheduleExpanded */
/* select * from dbo.refWorkScheduleExpanded */

/*select * from dbo.EnumsBase where CodeKey like N'Calendar_Day_Types%'*/

/*
select
	*
from
	dbo.refDistributors d
	join dbo.refPositions p on p.idDistributor=d.id
	join dbo.refEmployeesPositions ep on ep.idPosition=p.id
	join dbo.refPhysicalPersons pp on pp.id=ep.idEmployee
where
	d.id=281474977164328
	and p.deleted=0
	and ep.deleted=0
	and pp.deleted=0
*/

------------------------------------------------------------

select
	eDT.Description,
	eR.Description,
	*
from
	dbo.refAppointments a
	join dbo.EnumsBase eDT on eDT.id=a.idDayType
	join dbo.EnumsBase eR on eR.id=a.idRecurrenceType

--update refAppointments set deleted=1 where idOwner=281474976711134
--update refAppointments set deleted=0 where idOwner=281474976711134
--delete from refAppointments where id>2
/*
select
	*
from
	dbo.EnumsBase
where
	CodeKey like N'Calendar_Day_Types_%'
*/

------------------------------------------------------------

select
	*
from
	dbo.refAppointments appointments  with (nolock)
	join dbo.EnumsBase eDT with (nolock) on eDT.id = appointments.idDayType
	left join dbo.refDistributors distributors with (nolock) on distributors.id = appointments.idOwner
	left join dbo.refPhysicalPersons physicalPersons with (nolock) on physicalPersons.id = appointments.idOwner
where
	appointments.deleted = 0
order by appointments.StartDate

select
	case
		when employeesPositions.id = positions.idChief then N'isChief'
		else N'!isChief'
	end as isChief,
	*
from
	dbo.refEmployeesPositions employeesPositions with (nolock)
	join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
	join dbo.refDistributors distributors with (nolock) on distributors.id = positions.idDistributor
	left join dbo.refAppointments appointments with (nolock) on appointments.idOwner = distributors.id and appointments.deleted = 0
where
	employeesPositions.idEmployee = (select id from dbo.refPhysicalPersons where Name = N'test_CAL' and deleted = 0)
	and employeesPositions.deleted = 0
	and positions.deleted = 0
	and distributors.deleted = 0
order by appointments.StartDate

--insert into dbo.refAppointments (id, idOwner, StartTime, EndTime, AllDay, idDayType, IsRecurrence, deleted, [priority], StartDate, EndDate, IsPeriod,WeekDays,DayNumber,MonthNumber,[Guid],idRecurrenceType,Periodicity) values (dbo.fn_getIdEx(281474976710657,1), 281474976710657, N'20150615', N'20150616', 1, 2401, 0, 0, 1, N'20150615', N'20150615',0,127,1,1,N'FCAE217C-0F0B-4E45-AF4B-91E43C8D2562',2500,1)