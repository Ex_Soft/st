﻿select
	--datalength(f.Body),
	*
	--fp.*
	--fpf.*
from
	dbo.refMarketingMaterials marketingMaterials with (nolock)
	join dbo.refFilePackets filePackets with (nolock) on filePackets.id = marketingMaterials.idPacket
	join dbo.refFilePacketsFiles filePacketsFiles with (nolock) on filePacketsFiles.idPacket = filePackets.id
	join dbo.refFiles files with (nolock) on files.id = filePacketsFiles.idFile
	join dbo.EnumsBase enumsMimeType on enumsMimeType.id = files.idMimeType
where
	--marketingMaterials.id = 562954287716495
	marketingMaterials.Name = N'74647 3 Плодятся дубли в refFilePacketsFiles прире'

--update dbo.refFiles set FileName = replicate(N'W', 217) + N'.jpg' where id = 562954287903440

select
	*
from
	dbo.refFiles files
	join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idFile = files.id
	join dbo.refFilePackets filePackets on filePackets.id = filePacketsFiles.idPacket
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = filePackets.idBaseObject
where
	files.id in (562954287610247, 562954287610249, 562954287610251)
order by filePackets.id

;with cte(idFile)
as
(
select distinct
	files.id
from
	dbo.refFiles files with (nolock)
	join dbo.refFilePacketsFiles filePacketsFiles with (nolock) on filePacketsFiles.idFile = files.id
	join dbo.refFilePackets filePackets with (nolock) on filePackets.id = filePacketsFiles.idPacket
	join dbo.EnumsBase enumsBaseObject with (nolock) on enumsBaseObject.id = filePackets.idBaseObject
where
	files.deleted = 0
	and filePacketsFiles.deleted = 0
	and filePacketsFiles.Internal = 0
	and filePackets.deleted = 0
	and enumsBaseObject.CodeKey = N'Object_Name_MarketingMaterial'
)
select
	*
from
	dbo.refFiles files with (nolock)
	join dbo.refFilePacketsFiles filePacketsFiles with (nolock) on filePacketsFiles.idFile = files.id
	join dbo.refFilePackets filePackets with (nolock) on filePackets.id = filePacketsFiles.idPacket
	join dbo.EnumsBase enumsBaseObject with (nolock) on enumsBaseObject.id = filePackets.idBaseObject
where
	files.id in (select idFile from cte)
	and files.deleted = 0
	and filePacketsFiles.deleted = 0
	and filePacketsFiles.Internal = 0
	and filePackets.deleted = 0
order by filePackets.id;


select
	*
from
	dbo.refFilePacketsFiles
where
	Internal = 1

select
	*
from
	dbo.EnumsBase e
where
	e.CodeKey like N'MimeType!_%' escape N'!'

--update dbo.refFiles set idMimeType = 2470194144 where id = 562954287479648
--update dbo.refFiles set idMimeType = 2470194146 where id = 562954287479648

