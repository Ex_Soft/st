select
	*
from
	dbo.refObjects systemObjects
	join dbo.EnumsBase enumsObjectType on enumsObjectType.id = systemObjects.idObjectType
	join dbo.EnumsBase enumsObjectName on enumsObjectName.id = systemObjects.idObjectName
where
	--enumsObjectName.CodeKey like N'Object!_Name!_Q%' escape N'!'
	enumsObjectName.CodeKey = N'Object_Name_AgentGPSs'
	--enumsObjectName.CodeKey in (N'Object_Name_MobileDoc', N'Object_Name_MobileInformation', N'Object_Name_MobileSettings', N'OrgStructure_Right_CRUDGoodsType', N'Object_Name_BonusIssued', N'Object_Name_ReturnToSupplier', N'Object_Name_OtherTurnover', N'Object_Name_AgentGPSs')
	--and enumsObjectType.CodeKey = N'Object_Type_References'
