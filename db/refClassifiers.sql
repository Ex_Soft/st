select
	*
from
	dbo.refClassifiers classifiers
	join dbo.refClassifierNames classifierNames on classifierNames.id = classifiers.idType
where
	classifierNames.id != 0

select
	classifierNames.CodeKey,
	count(*)
from
	dbo.refClassifierNames classifierNames
where
	classifierNames.id != 0
group by classifierNames.CodeKey
having count(*) > 1

select
	classifierNames.Value,
	count(*)
from
	dbo.refClassifierNames classifierNames
where
	classifierNames.id != 0
group by classifierNames.Value
having count(*) > 1

select
	classifierNames.[Description],
	count(*)
from
	dbo.refClassifierNames classifierNames
where
	classifierNames.id != 0
group by classifierNames.[Description]
having count(*) > 1

select * from dbo.refClassifierNames where CodeKey like N'GoodsClassifier%'