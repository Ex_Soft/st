﻿/*
update dbo.refOutercodes set idItem = 562954288030791 where id = 562954288031798
delete dbo.refQstTemplates where idFirstVersion = 562954288030791 and id != 562954288030791
update dbo.refQstTemplates set deleted = 0 where id = 562954288030791
update dbo.refOutercodes set idItem = 562954288030798 where id = 562954288031799
delete dbo.refQstTemplates where idFirstVersion = 562954288030798 and id != 562954288030798
update dbo.refQstTemplates set deleted = 0 where id = 562954288030798
*/

select
	outercodes.id,
	*
from
	dbo.refQstTemplates qstTemplates
	left join dbo.refOutercodes outercodes on outercodes.idItem = qstTemplates.id
where
	--qstTemplates.Name like N'74034 Outercode + MassEdit #%'
	qstTemplates.idFirstVersion in (562954288030791, 562954288030798)
order by qstTemplates.VersionNum, qstTemplates.idFirstVersion;

select
	qstTemplates.id,
	count(*)
from
	dbo.refQstTemplates qstTemplates
	join dbo.refSets [sets] on [sets].idItem = qstTemplates.id
	join dbo.refDistributorSets distributorSets on distributorSets.idSet = [sets].id
where
	qstTemplates.Name = N'64178 Зависает приложение при работе с ШОЛ'
group by qstTemplates.id

select
	qstTemplates.id,
	count(*)
from
	dbo.refQstTemplates qstTemplates
	join dbo.refSets [sets] on [sets].idItem = qstTemplates.id
	join dbo.refBuypointSets buypointSets on buypointSets.idSet = [sets].id
where
	qstTemplates.Name = N'64178 Зависает приложение при работе с ШОЛ'
group by qstTemplates.id

select
	qstTemplates.id,
	count(*)
from
	dbo.refQstTemplates qstTemplates
	join dbo.refSets [sets] on [sets].idItem = qstTemplates.id
	join dbo.refPositionSets positionSets on positionSets.idSet = [sets].id
where
	qstTemplates.Name = N'64178 Зависает приложение при работе с ШОЛ'
group by qstTemplates.id

select
	*
from
	dbo.refQstTemplates qstTemplates
	join dbo.refSets [sets] on [sets].idItem = qstTemplates.id
	--join dbo.refBuypointSets buypointSets on buypointSets.idSet = [sets].id
where
	qstTemplates.Name = N'64178 Зависает приложение при работе с ШОЛ'
	and [sets].TableName = N'refBuyPointSets'

select
	*
from
	dbo.refQstTemplates qstTemplates
	join dbo.refSets [sets] on [sets].idItem = qstTemplates.id
	left join dbo.refDistributorSets distributorSets on distributorSets.idSet = [sets].id
	left join dbo.refBuypointSets buypointSets on buypointSets.idSet = [sets].id
	left join dbo.refPositionSets positionSets on positionSets.idSet = [sets].id
where
	qstTemplates.Name = N'64178 Зависает приложение при работе с ШОЛ'

select
	*
from
	dbo.refQuestionsAnswers questionsAnswers

select
	*
from
	dbo.refQstTemplatesRows templatesRows
	join dbo.refQstTemplatesTopics templatesTopics on templatesTopics.id = templatesRows.idTemplateTopic
	join dbo.refQstTemplates templates on templates.id = templatesTopics.idTemplate
	join dbo.refQstTopics topics on topics.id = templatesTopics.idTopic
	join dbo.EnumsBase enumsAnswerType on enumsAnswerType.id = templatesRows.idAnswerType
where
	enumsAnswerType.CodeKey in (N'Questions_AnswerType_DateOnly', N'Questions_AnswerType_TimeOnly', N'Questions_AnswerType_DateTime')
	and templates.Name = N'68277 ругается на обязательные поля'

select
	*
from
	dbo.refQstTemplatesRows templatesRows
	join dbo.refQuestionsAnswers questionsAnswers on questionsAnswers.idTemplateRow = templatesRows.id

--select * from dbo.SettingsBase where CodeKey = N'MT2CompabilityMode'
--update dbo.SettingsBase set Value = 0 where CodeKey = N'MT2CompabilityMode'

select
	*
from
	dbo.DocJournal docJournal
	left join dbo.dhSurvey dhSurvey on dhSurvey.id = docJournal.id
	left join dbo.refQstTemplates qstTemplates on qstTemplates.id = dhSurvey.idTemplate
	left join dbo.drSurvey drSurvey on drSurvey.idDoc = dhSurvey.id
	left join dbo.refQstTemplatesRows qstTemplatesRows on qstTemplatesRows.id = drSurvey.idTemplateRow
	left join dbo.refGoods goods on goods.id = drSurvey.idQuestion
	left join dbo.refQstTopics qstTopics on qstTopics.id = drSurvey.idTopic
	left join dbo.drSurveyValues drSurveyValues on drSurveyValues.idDocRow = drSurvey.id
	left join dbo.refQstAnswers qstAnswers on qstAnswers.id = drSurveyValues.AnswerID
	left join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idPacket = drSurvey.idPacket
	left join dbo.refFiles files on files.id = filePacketsFiles.idFile
	left join dbo.EnumsBase enumsAnswerType on enumsAnswerType.id = qstTemplatesRows.idAnswerType
where
	--docJournal.PrintDocNum = N'3740000000137460'
	docJournal.id = 84455167464707895
	-- and goods.FullName = N'Зеленая марка'
	and drSurveyValues.AnswerID is null

select
	*
from
	dbo.refQstTemplates templates
	join dbo.refPropertiesHistory propertiesHistory on propertiesHistory.idItem = templates.id
where
	templates.Name = N'55093 Не сохраняется изменение активности у ШОЛ #2'
order by templates.VersionNum

--update dbo.refPropertiesHistory set EventDate = N'20160701' where id in (562954286850587, 562954286860589)

select
	*
from
	dbo.refQstTemplates templates
	join dbo.refSets [sets] on [sets].idItem = templates.id
	join dbo.refBuyPointSets buyPointSets on buyPointSets.idSet = [sets].id
	join dbo.refBuyPoints buyPoints on buyPoints.id = buyPointSets.idBuypoint
	join dbo.refBuypointTypes buypointTypes on buypointTypes.id = buyPointSets.idBuypointType
	join dbo.XPObjectType xpObjectType on xpObjectType.OID = [sets].ObjectType
where
	templates.deleted = 0
	and templates.Name = N'Шаблон опроса для теста импорта в подборы'
	and [sets].TableName = N'refBuyPointSets'

select
	*
from
	refQuestionsAnswers questionsAnswers
	join dbo.refQstAnswers answers on answers.id = questionsAnswers.idValue
where
	questionsAnswers.deleted = 0

select
	*
from
	dbo.EnumsBase
where
	CodeKey like N'Questions!_AnswerType!_%' escape N'!'
order by Value
--order by MTCode

select
	*
from
	dbo.refQstTemplatesRows tr
	join dbo.EnumsBase e on e.id = tr.idAnswerType

--update dbo.SettingsBase set Value = N'1' where CodeKey = N'MT2CompabilityMode'
select * from dbo.SettingsBase where CodeKey = N'MT2CompabilityMode'

select
	*
from
	dbo.refQstTemplates templates
where
	templates.Name = N'refQstTemplates #1'

select * from dbo.refQstTemplates where idFirstVersion in (562954286610265) order by idFirstVersion, VersionNum desc
/* update dbo.refQstTemplates set deleted=1 where id in (562954286610327) */
/* update dbo.refQstTemplates set deleted=1 where idFirstVersion=562954286610856 and VersionNum<8 */

select
	idFirstVersion,
	count(*)
from
	dbo.refQstTemplates
where
	deleted=0
group by idFirstVersion
having count(*) > 1

select
	*
from
	dbo.refQstTemplates templates
where
	templates.Name in (N'refQstTemplates #2', N'refQstTemplates #3')
order by templates.idFirstVersion, templates.VersionNum

select
	templates.id as TemplateId,
	templates.Name as TemplateName,
	goods.id as QuestionId,
	goods.FullName as QuestionName,
	count(topics.id)
from
	dbo.refQstTemplates templates
	join dbo.refQstTemplatesTopics templatesTopics on templatesTopics.idTemplate = templates.id
	join dbo.refQstTopics topics on topics.id = templatesTopics.idTopic
	join dbo.refQstTemplatesRows templatesRows on templatesRows.idTemplateTopic = templatesTopics.id
	join dbo.refGoods goods on goods.id = templatesRows.idQuestion
where
	templates.deleted = 0
	and templatesTopics.deleted = 0
	and topics.deleted = 0
	and templatesRows.deleted = 0
	and goods.deleted = 0
group by templates.id, templates.Name, goods.id, goods.FullName
having count(topics.id) > 1

select
	*
from
	dbo.refQstTemplatesRows templatesRows
	join dbo.refGoods goods on goods.id = templatesRows.idQuestion
	join dbo.refQuestionsAnswers questionsAnswers on questionsAnswers.idTemplateRow = templatesRows.id
	join dbo.refQstAnswers answers on answers.id = questionsAnswers.idValue
where
	templatesRows.deleted = 0
	and goods.deleted = 0
	and questionsAnswers.deleted = 0
	and answers.deleted = 0

--update dbo.refQstTemplatesRows set IsUsePhoto = 1 where id in (562954287684875, 562954287684873, 562954287684877, 562954287684874)

select * from dbo.EnumsBase where CodeKey = N'QstTemplatesRowType_DefaultValue'

/*
update
	dbo.refQstTemplatesRowsDefValues
set
	AnswerNumber = 0
from
	dbo.refQstTemplatesRowsDefValues templatesRowsDefValues with (nolock)
	join dbo.refQstTemplatesRows templatesRows with (nolock) on templatesRows.id = templatesRowsDefValues.idTemplateRow
	join dbo.EnumsBase enumsAnswerType with (nolock) on enumsAnswerType.id = templatesRows.idAnswerType
where
	templatesRowsDefValues.AnswerID in (7, 8, 9)
	and (templatesRowsDefValues.AnswerNumber != 0)
	and enumsAnswerType.CodeKey in (N'Questions_AnswerType_YesNo', N'Questions_AnswerType_YesNoUnknown')
*/

select
	*
from
	dbo.refQstTemplatesRowsDefValues templatesRowsDefValues with (nolock)
	join dbo.refQstTemplatesRows templatesRows with (nolock) on templatesRows.id = templatesRowsDefValues.idTemplateRow
	join dbo.EnumsBase enumsAnswerType with (nolock) on enumsAnswerType.id = templatesRows.idAnswerType
where
	templatesRowsDefValues.AnswerID in (7, 8, 9)
	--and templatesRowsDefValues.AnswerNumber != 0
	and enumsAnswerType.CodeKey in (N'Questions_AnswerType_YesNo', N'Questions_AnswerType_YesNoUnknown')

select
	*
from
	dbo.refQstTemplatesRows templatesRows with (nolock)
	join dbo.EnumsBase enumsAnswerType with (nolock) on enumsAnswerType.id = templatesRows.idAnswerType
	left join dbo.refQstTemplatesRowsDefValues templatesRowsDefValues with (nolock) on templatesRowsDefValues.idTemplateRow = templatesRows.id
where
	templatesRows.id = 281474996590136
	templatesRows.deleted = 0
	and enumsAnswerType.CodeKey in (N'Questions_AnswerType_YesNo', N'Questions_AnswerType_YesNoUnknown')
	and templatesRowsDefValues.AnswerID in (7, 8, 9)
	and templatesRowsDefValues.AnswerNumber != 0
	and templatesRowsDefValues.deleted = 0


select
	/*templates.id as TemplateId,
	templates.Name as TemplateName,
	topics.id as TopicId,
	topics.Name as TopicName,
	goods.id as QuestionId,
	goods.FullName as QuestionName,*/
	--templatesRowsDefValues.*,
	*
	--templatesRows.*
	--g.FullName,
	
	--answers.*
from
	dbo.refQstTemplates templates
	join dbo.refQstTemplatesTopics templatesTopics on templatesTopics.idTemplate = templates.id
	join dbo.refQstTopics topics on topics.id = templatesTopics.idTopic
	join dbo.refQstTemplatesRows templatesRows on templatesRows.idTemplateTopic = templatesTopics.id
	join dbo.refGoods goods on goods.id = templatesRows.idQuestion
	left join dbo.refQstTemplatesRowsDefValues templatesRowsDefValues on templatesRowsDefValues.idTemplateRow = templatesRows.id
	left join dbo.refQstAnswers answersDefault on answersDefault.id = templatesRowsDefValues.AnswerID
	left join dbo.refQuestionsAnswers questionsAnswers on questionsAnswers.idTemplateRow = templatesRows.id -- and questionsAnswers.idValue = answers.id
	left join dbo.refQstAnswers answersList on answersList.id = questionsAnswers.idValue
	join dbo.EnumsBase enumsAnswerType with (nolock) on enumsAnswerType.id = templatesRows.idAnswerType
where
	--templates.id = 281474996590116
	--questionsAnswers.id in (562954287661744, 562954287661745, 562954287661746, 562954287661740, 562954287661741, 562954287661742)
	--templates.Name = N'68290 [Errors-db] System.NullReferenceException'
	--templates.id = 562954288712861
	templates.Name like N'%68290 ![Errors-db] System.NullReferenceException%' escape N'!'
	and templates.deleted = 0
order by goods.FullName, topics.Name

select
	templates.Name,
	topics.Name,
	goods.id,
	goods.FullName,
	*
from
	dbo.refQstAnswers answersList
	left join dbo.refQuestionsAnswers questionsAnswers on questionsAnswers.idValue = answersList.id
	left join dbo.refQstTemplatesRows templatesRows on templatesRows.id = questionsAnswers.idTemplateRow
	left join dbo.refGoods goods on goods.id = templatesRows.idQuestion
	left join dbo.refQstTemplatesTopics templatesTopics on templatesTopics.id = templatesRows.idTemplateTopic
	left join dbo.refQstTemplates templates on templates.id = templatesTopics.idTemplate
	left join dbo.refQstTopics topics on topics.id = templatesTopics.idTopic
	left join dbo.refQstTemplatesRowsDefValues templatesRowsDefValues on templatesRowsDefValues.idTemplateRow = templatesRows.id
	left join dbo.refQstAnswers answersDefault on answersDefault.id = templatesRowsDefValues.AnswerID
where
	--answers.id in (562954286509198, 562954286509199, 562954286509200, 562954286561721, 562954286561722, 562954286561723)
	answersList.GroupName in (N'Выкладка', N'group #2')
	and templates.deleted = 0
order by templates.Name, topics.Name, goods.FullName

select
	*
from
	dbo.refQstTemplates templates
	left join dbo.refQstTemplatesTopics templatesTopics on templatesTopics.idTemplate = templates.id
	left join dbo.refQstTopics topics on topics.id = templatesTopics.idTopic
	left join dbo.refQstTemplatesRows templatesRows on templatesRows.idTemplateTopic = templatesTopics.id
	left join dbo.refGoods goods on goods.id = templatesRows.idQuestion
	left join dbo.refPropertiesHistory propertiesHistory on propertiesHistory.idItem = goods.id
where
	templates.Name = N'60912 Некорректный вариант ответа'
	--templates.deleted = 0
order by goods.FullName

--insert into refPropertiesHistory (id, idItem, idEvent, EventDate, PeriodNum, deleted, outercode, ObjectType, AttributeName, AttributeValue) values (dbo.fn_getIdEx(281474976711134, 1), 562954286509192, 133, N'20150221', 0, 0, 0, 10, N'', N'')

--select * from dbo.refQstTemplatesRows where id=562954286520367
--update dbo.refQstTemplatesRows set deleted=1 where id=562954286520367
--update dbo.refQstTemplatesRows set deleted=1 where id=562954286521313
--update dbo.refQstTemplatesRows set deleted=1 where id=562954286521416
--update dbo.refQstTopics set deleted=1 where id=562954286520329
--update dbo.refGoods set deleted=1 where id=562954286509195

select
	dhS.*,
	drS.*
from
	dbo.DocJournal dj
	join dbo.dhSurvey dhS on dhS.id=dj.id
	join dbo.drSurvey drS on drS.idDoc=dhS.id
where
	cast(dj.DateTimeStamp as date)=N'20131122'

select
	*
from
	dbo.refQstAnswers

select
	*
from
	dbo.refSets s


select
	*
from
	dbo.refGoods g
	join dbo.refQstTemplatesRows templatesRows on templatesRows.idQuestion = g.id
	join dbo.refQstTemplatesTopics templatesTopics on templatesTopics.id=templatesRows.idTemplateTopic
	join dbo.refQstTopics topics on topics.id=templatesTopics.idTopic
	join dbo.refQstTemplates templates on templates.id=templatesTopics.idTemplate
where
	g.FullName like N'???? ??????????%'
	and templates.deleted=0
	and templates.name=N'????? ????  ? 11.3 ?? 28.3 ?? ??? ?????? ??????'

select * from dbo.refDistributors where id=281475861263463

select
	*
from
	dbo.refQstTemplates templates
	join dbo.refSets s on s.idItem=templates.id
	join dbo.refBuypointSets bps on bps.idSet=s.id
where
	templates.name=N'????? ????  ? 11.3 ?? 28.3 ?? ??? ?????? ??????'
	and s.Name=N'???????? ?????'


/*
declare
	@refQstTemplatesId bigint = 562954286541716

delete
	dbo.refQstTemplatesRowsDefValues
from
	dbo.refQstTemplatesRowsDefValues templatesRowsDefValues
	join dbo.refQstTemplatesRows templatesRows on templatesRows.id=templatesRowsDefValues.idTemplateRow
	join dbo.refQstTemplatesTopics templatesTopics on templatesTopics.id=templatesRows.idTemplateTopic
	join dbo.refQstTemplates templates on templates.id=templatesTopics.idTemplate
 where
	templates.id=@refQstTemplatesId

delete
	dbo.refQuestionsAnswers
from
	dbo.refQuestionsAnswers questionsAnswers
	join dbo.refQstTemplatesRows templatesRows on templatesRows.id=questionsAnswers.idTemplateRow
	join dbo.refQstTemplatesTopics templatesTopics on templatesTopics.id=templatesRows.idTemplateTopic
	join dbo.refQstTemplates templates on templates.id=templatesTopics.idTemplate
 where
	templates.id=@refQstTemplatesId

delete
	dbo.refQstTemplatesRows
from
	dbo.refQstTemplatesRows templatesRows
	join dbo.refQstTemplatesTopics templatesTopics on templatesTopics.id=templatesRows.idTemplateTopic
	join dbo.refQstTemplates templates on templates.id=templatesTopics.idTemplate
 where
	templates.id=@refQstTemplatesId

delete
	dbo.refQstTemplatesTopics
from
	dbo.refQstTemplatesTopics templatesTopics
	join dbo.refQstTemplates templates on templates.id=templatesTopics.idTemplate
 where
	templates.id=@refQstTemplatesId

delete from dbo.refQstTemplates where id=@refQstTemplatesId
*/


/*
insert into GroupTesterNigtlyCurrent.dbo.refQstTemplatesRowsDefValues
(id, idTemplateRow, AnswerStr, AnswerNumber, AnswerID, outercode, deleted, idType)
values
(dbo.fn_getIdEx(281474976801757, 1), 7318349394490433, N'', 0, 7318349394481069, N'0', 0, 815)

delete from GroupTesterNigtlyCurrent.dbo.refQstTemplatesRowsDefValues where id = 7318349394491501
 */

/*
delete
	dbo.refQstTemplatesRows
from
	dbo.refQstTemplates templates
	left join dbo.refQstTemplatesTopics templatesTopics on templatesTopics.idTemplate = templates.id
	left join dbo.refQstTemplatesRows templatesRows on templatesRows.idTemplateTopic = templatesTopics.id
where
	templates.id = 562954286571109

delete
	dbo.refQstTemplatesTopics
from
	dbo.refQstTemplates templates
	left join dbo.refQstTemplatesTopics templatesTopics on templatesTopics.idTemplate = templates.id
where
	templates.id = 562954286571109

delete from dbo.refQstTemplates where id = 562954286571109

update dbo.refQstTemplates set deleted = 0 where id = 562954286570136
*/

select * from dbo.refQstAnswers