﻿select * from dbo.TableRules where tableName = N'refPivotReportTemplates'
select * from dbo.TableRules where tableName like N'%po%'
select * from dbo.TableRules where tableRule = 0

/*
tableRule — определяет тип данных и политику доступа к таблице:

    1 — суперглобальный;
    2 — глобальный;
    остальные значения — локальный.

tableVision — определяет видимость и политику доступа к таблице:

    1 — всю свою ветку по орг. структуре;
    2 — дочерние + свои записи;
    3 — родительские + свои записи;
    4 — ЦО + свои записи.
*/