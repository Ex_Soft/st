﻿select * from dbo.refPlanningPeriods

select
	*
from
	dbo.refTypesOfPlanning typesOfPlanning
	join dbo.EnumsBase enumsDimension on enumsDimension.id = typesOfPlanning.idDimension
	join dbo.EnumsBase enumsDecomposition on enumsDecomposition.id = typesOfPlanning.idDecomposition
	join dbo.refStoredProcedures storedProcedures on storedProcedures.id = typesOfPlanning.idStoredProcedure
	join dbo.EnumsBase enumStoredProcedureType on enumStoredProcedureType.id = storedProcedures.idStoredProcedureType
	join dbo.refStoredProceduresParam storedProceduresParam on storedProceduresParam.idStoredProcedure = storedProcedures.id
	join dbo.EnumsBase enumMetaData on enumMetaData.id = storedProceduresParam.idMetaData
	left join dbo.refTypesOfPlanningParam typesOfPlanningParam on typesOfPlanningParam.idTypeOfPlanning = typesOfPlanning.id
	left join dbo.refPlanningParamValues planningParamValues on planningParamValues.IdTypeOfPlanningParam = typesOfPlanningParam.id
where
	typesOfPlanning.Code = N'1_2'

select
	*
from
	dbo.refPlanningBackups planningBackups
	join dbo.refPlanning planning on planning.id = planningBackups.idPlanning

select
	*
from
	dbo.refPlanning planning

select
	--typesOfPlanningParam.*
	*
from
	dbo.refPlanning planning
	join dbo.refTypesOfPlanning typesOfPlanning on typesOfPlanning.id = planning.idTypeOfPlanning
	left join dbo.refTypesOfPlanningParam typesOfPlanningParam on typesOfPlanningParam.idTypeOfPlanning = typesOfPlanning.id
	left join dbo.refPlanningParamValues planningParamValues on planningParamValues.IdTypeOfPlanningParam = typesOfPlanningParam.id
where
	typesOfPlanning.Name = N'Ассортимент товара в заказе (по торговым точкам)'

--insert into dbo.refTypesOfPlanningParam (id, idTypeOfPlanning, paramName, paramBaseName, idMetaData, paramDefaultValue, deleted) values (dbo.fn_getIdEx(281474976710657, 1), 281474990400439, N'Не выбрано', N'ЦО', 13, N'', 0)
--insert into dbo.refPlanningParamValues (id, idPlanning, ValuePlanning, deleted, idTypeOfPlanningParam, idInitMetaData) values (dbo.fn_getIdEx(281474976710657, 1), 562954286388920, N'True', 0, 281475016819790, 13)

--update dbo.refPlanning set Name = N'57053 Планы. Каскадирование планов 1' where Name = N'57053 Планы. Каскадирование планов 1'

select
	--planning.*
	vPlanningFields.*
from
	dbo.refPlanning planning
	join dbo.vPlanningFields vPlanningFields on vPlanningFields.id = planning.id
where
	planning.id = 562954287893227

select
	*
from
	dbo.refPlanning planning
	join dbo.refPlanningValues planningValues on planningValues.idPlanning = planning.id
where
	planningValues.idOutletSet != 0

select
	*
	--planning.*
	--typesOfPlanning.*
	--planningValues.*
from
	dbo.refPlanning planning
	join dbo.refPlanningValues planningValues on planningValues.idPlanning = planning.id
	join dbo.refTypesOfPlanning typesOfPlanning on typesOfPlanning.id = planning.idTypeOfPlanning
	join dbo.EnumsBase enumsDimension on enumsDimension.id = typesOfPlanning.idDimension
	join dbo.EnumsBase enumsDecomposition on enumsDecomposition.id = typesOfPlanning.idDecomposition
	join dbo.EnumsBase enumsPeriodType on enumsPeriodType.id = planning.idPeriodType
	join dbo.EnumsBase enumsSubPeriodType on enumsSubPeriodType.id = planning.idSubPeriodType
where
	planning.id = 562954288065291

select
	*
from
	dbo.refSets [sets]
where
	[sets].idItem = 562954288065291

select
	*
from
	dbo.refTypesOfPlanning typesOfPlanning
	join dbo.EnumsBase enumsDimension on enumsDimension.id = typesOfPlanning.idDimension
	join dbo.EnumsBase enumsDecomposition on enumsDecomposition.id = typesOfPlanning.idDecomposition
where
	typesOfPlanning.isBuypointsSection = 1
	and typesOfPlanning.isSKUSection = 1
	and enumsDecomposition.CodeKey = N'Planning_Decomposition_Sum'

select * from dbo.refSets where id = 0
select * from dbo.refDistributorSets where id = 0
select * from dbo.refOutletSets where id = 0
select * from dbo.refPositionSets where id = 0
select * from dbo.refSkuSets where id = 0

insert into dbo.refOutletSets (id, idSet, idOutlet, idOutletType, deleted) values (0, 0, 0, 0, 1)

select
	*
from
	dbo.refPlanning planning
	join dbo.refSets [sets] on [sets].idItem = planning.id
where
	--planning.id = 281475146781716
	planning.Name = N'74217 Не отображаются изменения при восстановлении плана 2'

select
	*
from
	dbo.refPlanning planning
	join dbo.refPlanningValues planningValues on planningValues.idPlanning = planning.id
	join dbo.refSets [sets] on [sets].id = planningValues.idDistributorSet
	join dbo.refDistributorSets distributorSets on distributorSets.idSet = [sets].id
	join dbo.refDistributors distributors on distributors.id = distributorSets.idDistributor
where
	--planning.id = 281475146781716
	planning.Name = N'74217 Не отображаются изменения при восстановлении плана 2'

select
	*
from
	dbo.refPlanning planning
	join dbo.refPlanningValues planningValues on planningValues.idPlanning = planning.id
	join dbo.refSets positionsSets on positionsSets.idItem = planning.id and positionsSets.id = planningValues.idPositionSet
	join dbo.refPositionSets positionSets on positionSets.idSet = positionsSets.id
	join dbo.refPositions positions on positions.id = positionSets.idPosition
where
	--planning.id = 562954287893227
	planning.Name = N'74217 Не отображаются изменения при восстановлении плана 2'

select
	*
from
	dbo.refPlanning planning
	join dbo.refPlanningValues planningValues on planningValues.idPlanning = planning.id
	join dbo.refSets positionsSets on positionsSets.idItem = planning.id and positionsSets.id = planningValues.idPositionSet
	join dbo.refPositionSets positionSets on positionSets.idSet = positionsSets.id
	--join dbo.refPositions positions on positions.id = positionSets.idPosition
	--join dbo.refRoutes routes on routes.idPosition = positions.id
	--join dbo.refRouteVisits routeVisits on routeVisits.idRoute = routes.id
	--join dbo.refOutlets outlets on outlets.id = routeVisits.idOutlet
where
	--planning.id = 562954287893227
	planning.Name = N'74217 Не отображаются изменения при восстановлении плана 2'

select
	*
from
	dbo.refPlanning planning
	join dbo.refPlanningValues planningValues on planningValues.idPlanning = planning.id
	join dbo.refSets outletsSets on outletsSets.idItem = planning.id and outletsSets.id = planningValues.idOutletSet
	join dbo.refOutletSets outletSets on outletSets.idSet = outletsSets.id
	join dbo.refOutlets outlets on outlets.id = outletSets.idOutlet
where
	--planning.id = 281475146781716
	planning.Name = N'74217 Не отображаются изменения при восстановлении плана 2'

select
	*
from
	dbo.refPlanning planning
	join dbo.refPlanningValues planningValues on planningValues.idPlanning = planning.id
	join dbo.refSets skusSets on skusSets.id = planningValues.idSkuSet
	join dbo.refSkuSets skuSets on skuSets.idSet = skusSets.id
	join dbo.refGoods skus on skus.id = skuSets.idSku
where
	--planning.id = 562954286407069
	planning.Name = N'57053 Планы. Каскадирование планов 1'

select
	*
from
	dbo.refPlanning planning
where
	planning.id = 562954286407069

select
	*
from
	dbo.refPlanning planning
	join dbo.refTypesOfPlanning typesOfPlanning on typesOfPlanning.id = planning.idTypeOfPlanning
	join dbo.refPlanningValues planningValues on planningValues.idPlanning = planning.id
where
	--planning.id = 562954286500228
	planning.Name = N'57053 Планы. Каскадирование планов 4'
order by planningValues.idDistributorSet, planningValues.idPositionSet, planningValues.idOutletSet, planningValues.idSkuSet

select
	*
from
	dbo.refPlanning planning
	join dbo.refSets [sets] on [sets].idItem = planning.id
where
	planning.Name = N'57053 Планы. Каскадирование планов 5'

select
	*
from
	dbo.refPlanning planning
	join dbo.refPlanningPeriods planningPeriods on planningPeriods.idPlanning = planning.id
	join dbo.refPlanningValues planningValues on planningValues.idPlanning = planning.id and planningValues.idPlanningPeriod = planningPeriods.id
where
	planning.Name = N'74217 Не отображаются изменения при восстановлении плана 2'
order by planningPeriods.NumSubPeriod, planningValues.idDistributorSet, planningValues.idPositionSet, planningValues.idOutletSet, planningValues.idSkuSet


select
	*
from
	dbo.refPlanning planning
	join dbo.refPlanningBackups planningBackups on planningBackups.idPlanning = planning.id
where
	planning.Code = N'0020000000000014'
	and planningBackups.SourceData = N'20150422 16:51:15.430'

select
	*
from
	dbo.refPlanningBackups

--delete from dbo.refPlanningBackups where id != 0

select
	*
from
	sys.objects [objects]
	join sys.columns [columns] on [columns].object_id = [objects].object_id
where
	[columns].name like N'%buypoint%'