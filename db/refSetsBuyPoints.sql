select
	*
from
	dbo.refSets sets
	join dbo.refBuyPointSets buyPointSets on buyPointSets.idSet = sets.id
	join dbo.XPObjectType xpObjectType on xpObjectType.OID = sets.ObjectType
where
	sets.Field = N'BuyPoint'