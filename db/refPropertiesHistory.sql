﻿-- Doubles

select
	propertiesHistory.idItem,
	propertiesHistory.EventDate,
	count(*)
from
	dbo.refPropertiesHistory propertiesHistory
where
	propertiesHistory.deleted = 0
	and propertiesHistory.ObjectType = 20
	--and propertiesHistory.idEvent in (133, 134)
group by propertiesHistory.idItem, propertiesHistory.EventDate
having count(*) > 1

select
	*
from
	dbo.refPropertiesHistory propertiesHistory
	left join dbo.EnumsBase enumsBase on enumsBase.id = propertiesHistory.idEvent
	left join dbo.XPObjectType xpObjectType on xpObjectType.OID = propertiesHistory.ObjectType
where
	propertiesHistory.idItem = 313281649079027373

------------------------------------------------------------

--update dbo.refPropertiesHistory set EventDate = N'20001122' where idItem = 562954288002711
/*
update dbo.refPropertiesHistory set deleted = 0 where idItem = 562954288002711
delete from dbo.refPropertiesHistory where idItem = 562954288002711 and cast(EventDate as date) = N'20171122'
update dbo.refPositions set idDistributor = 281474976711134 where id = 562954288002711
*/

select * from dbo.refPositions positions with(nolock) where positions.id = 562954288002711
select
	*
from
	dbo.refPropertiesHistory with(nolock)
where
	idItem in (562954288002711)
	--cast(EventDate as date) = N'20171122'
order by idItem, EventDate, id

-- Ротация персонала

select
	p.Name,
	ph.*,
	Manager.Name as Manager,
	Distributor.Name as Distributor,
	ManagerOwnJurPerson.Name as ManagerOwnJurPerson,
	ManagerOwnJurPersonAccessory.Value as ManagerOwnJurPersonAccessory
from
	dbo.refPositions p
	join dbo.refPropertiesHistory ph on ph.idItem = p.id
	left join dbo.refPhysicalPersons Manager on ph.AttributeName = N'Manager' and Manager.id = cast(coalesce(ph.AttributeValue, N'0') as bigint)
	left join dbo.refDistributors Distributor on ph.AttributeName = N'Distributor' and Distributor.id = cast(coalesce(ph.AttributeValue, N'0') as bigint)
	left join dbo.refOwnJuridicalPersons ManagerOwnJurPerson on ph.AttributeName = N'Manager.OwnJurPerson' and ManagerOwnJurPerson.id = cast(coalesce(ph.AttributeValue, N'0') as bigint)
	left join dbo.EnumsBase ManagerOwnJurPersonAccessory on ph.AttributeName = N'Manager.OwnJurPerson.Accessory' and ManagerOwnJurPersonAccessory.id = cast(coalesce(ph.AttributeValue, N'0') as bigint)
where
	p.id = 10414574138303417
	and ph.deleted = 0
order by ph.EventDate

select
	*
from
	dbo.EnumsBase
where
	CodeKey like N'OwnJuridicalPersons!_Accessory!_%' escape N'!'