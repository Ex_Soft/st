select
	*
from
	AI.refAISchedules aiSchedules
	left join dbo.refStoredProcedures storedProcedures on storedProcedures.id = aiSchedules.idStoredProcedure
	left join dbo.refStoredProcedures storedProcedures2Xml on storedProcedures2Xml.id = aiSchedules.idXMLStoredProcedure

/* insert into dbo.refStoredProcedures (id, spName, [Name], CodeKey, idStoredProcedureType, IsForReport, [timeOut], [Description], deleted) values (dbo.fn_getIdEx((select top 1 id from dbo.refDistributors where IsRoot = 1 and deleted = 0), 1), N'autoInformerByScheduleII', N'autoInformerByScheduleII', N'AutoInformerII (Scheduler)', 2014090410317, 0, 300, N'', 0) */
/* insert into dbo.refStoredProcedures (id, spName, [Name], CodeKey, idStoredProcedureType, IsForReport, [timeOut], [Description], deleted) values (dbo.fn_getIdEx((select top 1 id from dbo.refDistributors where IsRoot = 1 and deleted = 0), 1), N'autoInformerByScheduleII2XML', N'autoInformerByScheduleII2XML', N'AutoInformerII2XML (Scheduler)', 2014090410317, 0, 300, N'', 0) */
/* update AI.refAISchedules set idStoredProcedure = 0 where id = 0 */
/* update AI.refAISchedules set idStoredProcedure = 281474992379608 */
/* update AI.refAISchedules set idXMLStoredProcedure = 0 */

;merge into AI.refAISchedules as tgt
using
(
	select 0 as id, 281475015931344 as idStoredProcedure
	union
	select 281475012410982 as id, 281474992379608 as idStoredProcedure
) as src
on tgt.id = src.id
	when matched
		then update set idStoredProcedure = src.idStoredProcedure;

select * from dbo.refStoredProcedures where spName like N'autoInformerBySchedule%'

select
	*
from
	AI.refAISchedules aiSchedules
	join dbo.refStoredProcedures storedProcedures on storedProcedures.id = aiSchedules.idStoredProcedure
	join dbo.EnumsBase enumsScheduleType on enumsScheduleType.id = aiSchedules.idScheduleType
	join dbo.refStoredProceduresParam storedProceduresParam on storedProceduresParam.idStoredProcedure = storedProcedures.id
	left join AI.refAIScheduleParamValues aiScheduleParamValues on aiScheduleParamValues.idAISchedule = aiSchedules.id and aiScheduleParamValues.idStoredProcedureParam = storedProceduresParam.id

select
	*
from
	AI.refAIScheduleParamValues

select
	* 
from
	dbo.refStoredProceduresParam storedProceduresParam
	join AI.refAIScheduleParamValues aiScheduleParamValues on aiScheduleParamValues.idStoredProcedureParam = storedProceduresParam.id

select
	*
from
	sys.objects
where
	name like N'%autoInformerBySchedule%'


