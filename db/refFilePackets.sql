select * from dbo.EnumsBase where  CodeKey like N'File!_LoadType%' escape N'!'
select * from dbo.refFiles where FileName like N'sim%' Uri like N'http://st-drive.systtech.ru/%'

--update dbo.refFiles set Body = cast(0 as varbinary(max)), Uri = N'http://st-drive.systtech.ru/3a6d959b7b6f404fb04ea1eb62748700', idLoadType = 201601144647403 where id = 562954287936504
select * from dbo.refFiles where id = 562954287936504

select
	files.id,
	files.Uri,
	*
from
	dbo.refFiles files
	join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idFile = files.id
	join dbo.refFilePackets filePackets on filePackets.id = filePacketsFiles.idPacket
	left join dbo.drPhotoReport drPhotoReport on drPhotoReport.idPacket = filePackets.id
	left join dbo.drSurvey drSurvey on drSurvey.idPacket = filePackets.id
	left join dbo.drWorkingDayOfAgent drWorkingDayOfAgent on drWorkingDayOfAgent.idPacket = filePackets.id
where
	files.Uri like N'http%'
	and files.deleted = 0
	and filePacketsFiles.deleted = 0
	and filePackets.deleted = 0
	and not exists (select 1 from dbo.drPhotoReport where idPacket = filePackets.id)
	and not exists (select 1 from dbo.drSurvey where idPacket = filePackets.id)
	and not exists (select 1 from dbo.drWorkingDayOfAgent where idPacket = filePackets.id)

select
	*
from
	dbo.refFilePackets filePackets
	join dbo.refAttributesValues attributesValues on attributesValues.idPacket = filePackets.id
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject
	left join dbo.refAttributesListValues attributesListValues on attributesListValues.id = attributesValues.idValue and attributesListValues.deleted = 0
	join dbo.XPObjectType xpObjectType on xpObjectType.OID = attributesValues.ObjectType
where
	filePackets.deleted = 0
	and attributesValues.deleted = 0

select
	*
from
	dbo.refFilePackets filePackets
	join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idPacket = filePackets.id
	join dbo.refFiles files on files.id = filePacketsFiles.idFile
where
	--filePackets.id = 562954286647461
	filePackets.deleted = 0
	and files.FileName like N'%74%'

select
	--datalength(f.Body),
	*
	--filePackets.*
	--filePacketsFiles.*
	--files.*
from
	dbo.refFilePackets filePackets
	join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idPacket = filePackets.id
	join dbo.refFiles files on files.id = filePacketsFiles.idFile
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = filePackets.idBaseObject 
	join dbo.EnumsBase enumsMimeType on enumsMimeType.id = files.idMimeType
where
	--filePackets.id = 562954286647461
	--filePackets.id = 562954286643427
	files.Uri is not null

--update dbo.refFiles set Uri = N'http://st-drive.systtech.ru/' + Uri where id = 281474994429195 --Uri is not null
--update dbo.refFiles set Uri = N'http://st-drive.systtech.ru/db5c92f0a1be4d809dcd981a3d55ef8d' where id = 562954286720523
--update dbo.refFiles set idLoadType = 201601144647403 where idLoadType = 0 and Uri is not null

select
	*
	--files.id,
	--files.FileName,
	--files.idMimeType,
	--files.Body,
	--distributors.id as idDistributor
from
	dbo.refFiles files
	join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idFile = files.id
	join dbo.refFilePackets filePackets on filePackets.id = filePacketsFiles.idPacket
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = filePackets.idBaseObject
	join dbo.EnumsBase enumsMimeType on enumsMimeType.id = files.idMimeType
	join dbo.EnumsBase enumsLoadType on enumsLoadType.id = files.idLoadType
	join dbo.refDistributors distributors on distributors.NodeID = cast(files.id/281474976710656 as smallint)
where
	files.deleted = 0
	and filePacketsFiles.deleted = 0
	and filePackets.deleted = 0
	and distributors.deleted = 0
	and enumsBaseObject.CodeKey = N'Object_Name_SKU'
	and enumsMimeType.CodeKey in (N'MimeType_JPEG', N'MimeType_PNG')
	and enumsLoadType.CodeKey in (N'File_LoadType_Unknown', N'File_LoadType_STReplication', N'System')
	and len(ltrim(rtrim(coalesce(files.Uri, N'')))) = 0
	and datalength(files.Body) != 0

select
	*
from
	dbo.refFiles files
where
	--[FileName] in (N'koala.jpg', N'helicopter.jpg', N'nhev.jpg')
	--files.Uri is not null
	id = 0

select * from dbo.EnumsBase where CodeKey like N'MimeType%'
select * from dbo.EnumsBase where CodeKey like N'Object!_Name%' escape N'!' order by CodeKey

--update dbo.refFiles set Uri = N'f43deee4be06460fa37e43b84798a3a8' where id = 562954286641425
--delete from dbo.refFiles where id = 562954286639425
--update dbo.refFilePacketsFiles set Name = N'SKU''s images #1' where idPacket = 562954286643427
--update dbo.refFiles set FileName = N'koala_from_db.jpg' where id = 562954286640424

/*
update dbo.refFiles set Priority = 1 where id = 562954286639424
update dbo.refFiles set Priority = 2 where id = 562954286644429
update dbo.refFiles set Priority = 3 where id = 562954286640424
*/

/*
insert into dbo.refFiles
(id, [FileName], idMimeType, Body, deleted, Uri)
values
(dbo.fn_GetIdEx(281474976711134, 1), N'koala.jpg', (select id from dbo.EnumsBase where CodeKey = N'MimeType_JPEG'), cast(0 as varbinary(max)), 0, N'9c6bd2a001224bc2ad740cf9cd3a2dce')
*/

/*
insert into dbo.refFiles
(id, [FileName], idMimeType, Body, deleted)
values
(dbo.fn_GetIdEx(281474976711134, 1), N'koala.jpg', (select id from dbo.EnumsBase where CodeKey = N'MimeType_JPEG'), cast(0 as varbinary(max)), 0)
*/

/*
insert into dbo.refFilePackets
(id, Name, idBaseObject, deleted)
values
(dbo.fn_GetIdEx(281474976711134, 1), N'SKU''s images #2', (select id from dbo.EnumsBase where CodeKey = N'Object_Name_SKU'), 0)
*/

/*
declare @Name nvarchar(max) = N'SKU''s images #2'

insert into dbo.refFilePacketsFiles
(id, Name, idPacket, idFile, Internal, deleted)
select
	dbo.fn_GetIdEx(281474976711134, 1) as id,
	@Name as Name,
	(select id from dbo.refFilePackets where Name = @Name) as idPacket,
	id as idFile,
	0 as Internal,
	0 as deleted
from
	dbo.refFiles
where
	id in (562954286641425, 562954286642426)
*/

------------------------------------------------------------
/*
insert into dbo.refFiles
(id, [FileName], idMimeType, Body, deleted)
values
(dbo.fn_GetIdEx(281474976711134, 1), N'koala.jpg', (select id from dbo.EnumsBase where CodeKey = N'MimeType_JPEG'), cast(0 as varbinary(max)), 0)

declare
	@Path nvarchar(max)=N'D:\temp\images\',
	@ImageFileName nvarchar(max)=N'koala.jpg'

exec(N'
	update
		dbo.refFiles
	set
		Body = (select pic.* from openrowset(bulk ''' + @Path + @ImageFileName + N''', single_blob) as pic)
	where
		id = (select id from dbo.refFiles where [FileName] = N''koala.jpg'' and Uri is null)
	'
)
*/
------------------------------------------------------------
/*

declare
  @cmd nvarchar(max),
  @prm nvarchar(max),
  @id bigint,
  @idDistributor bigint = 281474976711134,
  @idBaseObject bigint = 433

set @cmd=N'
set @idOut = dbo.fn_GetIdEx(@idDistributor, 1)
insert into dbo.refFilePackets (id, idBaseObject, deleted) values (@idOut, @idBaseObject, 0)
'
set @prm=N'@idOut bigint output, @idDistributor bigint, @idBaseObject bigint'
exec sp_executesql @cmd, @prm, @idOut=@id output, @idDistributor=@idDistributor, @idBaseObject=@idBaseObject
select @id

*/
