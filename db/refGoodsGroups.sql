﻿select
	*
from
	dbo.refGoodsGroups goodsGroups
where
	deleted = 0
	--and idParent = 0
	--and Name = N'Группы товаров'
	--and id = 13229323905402837
	and id in (0, 562954286974875, 562954286974876, 281474977067838)
	and idParent in (0, 281474976711636, 281474976711638, 281474976711660, 281474976711672, 281474976711788, 281474976711790, 281474977067838, 281474978290119, 281474989740107, 562954286169696, 562954286182709, 13229323905402835, 13229323905402836)

/* update dbo.refGoodsGroups set idParent = 13229323905402867 where id = 13229323905402837 */ -- circle
/* update dbo.refGoodsGroups set idParent = 13229323905402836 where id = 13229323905402837 */ -- fix

/* down */
;with goodsGroupsDown(id, idParent, Name, level, path)
as
(
	select
		goodsGroups.id,
		goodsGroups.idParent,
		goodsGroups.Name,
		0 as level,
		cast(N'/' + goodsGroups.Name as varchar(max)) as path
	from
		dbo.refGoodsGroups goodsGroups
	where
		goodsGroups.idParent = 0
		and goodsGroups.id != 0
		and goodsGroups.deleted = 0
	union all
	select
		goodsGroups.id,
		goodsGroups.idParent,
		goodsGroups.Name,
		level + 1 as level,
		cast(path + N'/' + goodsGroups.Name as varchar(max)) as path
	from
		dbo.refGoodsGroups goodsGroups
		join goodsGroupsDown goodsGroupsDown on goodsGroups.idParent = goodsGroupsDown.id
	where
		goodsGroups.deleted = 0
)
select
	*
from
	goodsGroupsDown
--option (maxrecursion 10)


