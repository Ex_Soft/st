        bool ValidateAnswersWithPhoto()
        {
            bool
                result = true,
                MT2CompabilityMode = StuFacade.Instance.GetMT2CompabilityMode();

            if (!CurrentObject.Template.QstTemplateTopics.OfType<StuQstTemplatesTopic>().SelectMany(item => item.QstTemplateRows.OfType<StuQstTemplatesRow>()).Any(item => item.IsRequired && ((MT2CompabilityMode && item.IsUsePhoto) || (!MT2CompabilityMode && item.QuestionsAnswerType == QuestionsAnswerType.Questions_AnswerType_Photo)))
                || (CurrentObject.MainRows != null && CurrentObject.MainRows.Count != 1))
                return result;

            result = false;
            var message = StuFacade.Instance.GetResString(MetaData.ToString(), "msgErrorEmptyRowsWithRequiredPhoto") ?? StuFacade.Instance.GetResString("StuBaseDocumentRow", "msgErrorEmptyRows");

            EditView.ActivateNotValidTabPage("MainRows");
            EditView.ShowMessage(message, MessageBoxButtons.OK, MessageBoxIcon.Warning);

            return result;
        }
