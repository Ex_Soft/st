select
	*
from
	dbo.refFiles files
	join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idFile = files.id
	join dbo.refFilePackets filePackets on filePackets.id = filePacketsFiles.idPacket
	join dbo.drSurvey dr on dr.idPacket = filePackets.id
	join dbo.drSurveyPhotos drPhotos on drPhotos.idDocRow = dr.id
where
	drPhotos.id = 562954288026786

/* Restore
update dbo.drSurveyPhotos set photoFileName = N'http://st-drive.systtech.ru/efae150ffedd45569241640f85b6135d' where id = 562954288026786
update dbo.refFiles set FileName = N'System', idMimeType = 0, Uri = N'', idLoadType = 0 where id = 0
*/

select
	*
from
	dbo.DocJournal dj
	--join dbo.dhSurvey dhS on dhS.id = dj.id
where
	dj.id = 562954287660737

select
	sp.photoFileName,
	*
from
	dbo.drSurveyPhotos sp
	join dbo.drSurvey drS on drS.id = sp.idDocRow
	join dbo.dhSurvey dhS on dhS.id = drS.idDoc
	join dbo.DocJournal dj on dj.id = dhS.id
	left join dbo.rgPDAInfo rgPDAInfo on rgPDAInfo.idRoute = dj.idRoute
where
            
	dj.id = 564564865268520
	--dj.PrintDocNum = N'0010000000276274'
	--sp.id in (290893840344746, 290893840345228)
	--sp.photoFileName like N'%photo_1001029_151119103608131.jpg'

select
	--drSurveyPhotos.photoFileName,
	*
from
	dbo.DocJournal docJournal
	join dbo.dhSurvey dh on dh.id = docJournal.id
	join dbo.drSurvey dr on dr.idDoc = dh.id
	join dbo.drSurveyPhotos drSurveyPhotos on drSurveyPhotos.idDocRow = dr.id
where
	docJournal.idFirstVersion = 562954288026781
	--cast(docJournal.OpDate as date) = N'20180122'

select
	docJournal.PrintDocNum,
	templates.id as TemplateId,
	templates.Name as TemplateName,
	topics.id as TopicId,
	topics.Name as TopicName,
	goods.id as QuestionId,
	goods.FullName as QuestionName,
	*
	--docJournal.*,
	--dhSurvey.*,
	--drSurvey.*,
	--drSurveyValues.*,
	--answers.*
	--questionsAnswers.*
	--answersList.*
from
	dbo.DocJournal docJournal
	join dbo.dhSurvey dhSurvey on dhSurvey.id=docJournal.id
	join dbo.drSurvey drSurvey on drSurvey.idDoc=dhSurvey.id
	join dbo.drSurveyValues drSurveyValues on drSurveyValues.idDocRow = drSurvey.id
	join dbo.refQstTopics topics on topics.id = drSurvey.idTopic
	join dbo.refQstTemplatesRows templatesRows on templatesRows.id = drSurvey.idTemplateRow
	join dbo.EnumsBase enumsAnswerType on enumsAnswerType.id = templatesRows.idAnswerType
	join dbo.refQstTemplatesTopics templatesTopics on templatesTopics.id = templatesRows.idTemplateTopic
	join dbo.refQstTemplates templates on templates.id = templatesTopics.idTemplate
	join dbo.refGoods goods on goods.id = drSurvey.idQuestion
	left join dbo.refQstAnswers answers on answers.id = drSurveyValues.AnswerID
	left join dbo.refQuestionsAnswers questionsAnswers on questionsAnswers.idTemplateRow = templatesRows.id
	left join dbo.refQstAnswers answersList on answersList.id = questionsAnswers.idValue
where
	--docJournal.id = 1688849863764621
	--questionsAnswers.id in (562954287661744, 562954287661745, 562954287661746, 562954287661740, 562954287661741, 562954287661742)
	docJournal.PrintDocNum = N'0020000000000082'
	and docJournal.deleted = 0
	and docJournal.idStatus = 1
	--cast(docJournal.DateTimeStamp as date) = N'20170112'

select * from dbo.drSurveyValues where id = 562954287677834
update dbo.drSurveyValues set AnswerStr = N'��� ������', AnswerID = 4 where id = 14636707378891839 562954287677834
--update dbo.drSurveyValues set AnswerID = 0 where id = 562954287677834

select
	*
from
	dbo.DocJournal dj
	join dbo.dhSurvey dhS on dhS.id = dj.id
	join dbo.drSurvey drS on drS.idDoc = dhS.id
	join dbo.refQstTemplates QstTemplates on QstTemplates.id = dhS.idTemplate
	join dbo.refQstTopics QstTopics on QstTopics.id = drS.idTopic
where
	dj.PrintDocNum = N'0020000000000070'


select
	*
from
	dbo.DocJournal dj
	join dbo.dhSurvey dhS on dhS.id = dj.id
	join dbo.drSurvey drS on drS.idDoc = dhS.id
	join dbo.refQstTemplates QstTemplates on QstTemplates.id = dhS.idTemplate
	join dbo.refQstTopics QstTopics on QstTopics.id = drS.idTopic
	join dbo.refGoods g on g.id = drS.idQuestion
where
	dj.PrintDocNum = N'0020000000000070'

--update dbo.refQstTopics set deleted = 1 where id in (562954286570108, 562954286570109)
--update dbo.refGoods set deleted = 1 where id in (562954286570112,562954286570115,562954286570118,562954286570121,562954286570121,562954286570124,562954286570130,562954286570133)

select
	*
from
	dbo.DocJournal dj
	join dbo.dhSurvey dhSurvey on dhSurvey.id = dj.id
	join dbo.drSurvey drSurvey on drSurvey.idDoc = dhSurvey.id
	join dbo.refGoods goods on goods.id = drSurvey.idQuestion
	left join dbo.drSurveyValues drSurveyValues on drSurveyValues.idDocRow = drSurvey.id
where
	--dj.id = 562954293021467
	dj.PrintDocNum = N'0020000000000087'

select
	drPhotos.id,
	drPhotos.photoFileName,
	*
from
	dbo.drSurveyPhotos drPhotos
	join dbo.drSurvey dr on dr.id = drPhotos.idDocRow
	join dbo.DocJournal docJournal on docJournal.id = dr.idDoc
where
	drPhotos.photoFileName like N'http%'
	and (docJournal.idStatus = 1 or (docJournal.idStatus = 3 and docJournal.deleted = 1))