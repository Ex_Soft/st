select * from dbsrv.ch_mt_demo.sys.columns where object_id = object_id(N'dbo.refOutletsMT', N'u') order by name

select
	*
from
	dbsrv.ch_mt_demo.dbo.refOutletsMT outletsMT
	join dbsrv.ch_mt_demo.dbo.refOutletDistributorMT outletDistributorMT on outletDistributorMT.idOutlet = outletsMT.id
	join dbsrv.ch_mt_demo.dbo.refDistributors distributors on distributors.id = outletDistributorMT.idDistributor
where
	outletsMT.deleted = 0
	and outletDistributorMT.deleted = 0
	and distributors.deleted = 0
order by distributors.id, outletsMT.id

delete from dbsrv.ch_mt_demo.dbo.XPUISettings where idPhysicalPersons = 0