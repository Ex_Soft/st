﻿--update dbo.refOutlets set Name = N'test trigger# 1' where id = 281474992204624
--update dbo.refOutlets set Code = N'' where id in (562954287999705, 562954288000708)
--update dbo.refOutlets set Code = N'123456789' where id in (562954287999705, 562954288000708)

select
	*
from
	dbo.refOutlets outlets
	join dbo.refOutletDistributor outletDistributor on outletDistributor.idOutlet = outlets.id
	left join dbo.refOutercodes outercodes on outercodes.idItem = outlets.id and outercodes.idDistr = outletDistributor.idDistributor
where
	outlets.Name = N'For Test OuterCode'

--insert into dbo.refOutletDistributor (id, idOutlet, idDistributor) values (dbo.fn_getIdEx(281474992277690, 1), 562954288012747, 281474992277690)

select
	*
from
	dbo.refOutlets outlets
	join dbo.refOutletDistributor outletDistributor on outletDistributor.idOutlet = outlets.id
where
	--outlets.Name like N'%For Test OuterCode%'
	--outlets.Code in (N'0020000000001003', N'0020000000001004', N'0020000000001004', N'0020000000001005', N'0020000000001007', N'0020000000001011')
	outlets.Code = N'_A_0000000000005'
	and outlets.deleted = 0
	and outlets.IsVendor = 0
	and outletDistributor.deleted = 0
	--and outletDistributor.id in (281474976711134, 281474997029650, 281474997032901, 281474997234592)
order by outlets.id, outlets.code

select
	*
from
	dbo.refOutlets outlets
	join dbo.refOutletTypes outletTypes on outletTypes.id = outlets.idOutletType
where
	outlets.CodeInDistributorsErp like N'20171017%'

--update dbo.refOutletTypes set deleted = 1 where id = 281475017247200

select
	owners.idDuplicateOf as OwnersIdDuplicateOf,
	owners.id as OwnersId,
	owners.Name as OwnersName,
	owners.deleted as OwnersDeleted,
	outlets.idDuplicateOf as OutletsIdDuplicateOf,
	outlets.id as OutletsId,
	outlets.Name as OutletsName,
	outlets.deleted as OutletsDeleted,
	outlets.idOwner as OutletsIdOwner,
	*
from
	dbo.refOutlets outlets
	--join dbo.refOutletDistributor outletDistributor on outletDistributor.idOutlet = outlets.id
	--join dbo.refDistributors distributors on distributors.id = outletDistributor.idDistributor
	join dbo.refOwners owners on owners.id = outlets.idOwner
where
	--outlets.Code in (N'0020000000001112', N'0030000000001479')
	--outlets.id in (281475001239902)
	outlets.Name in (N'#РОДИТЕЛЬСКАЯ', N'#ДОЧЕРНЯЯ')

select * from dbo.LogDataChangeDuplication
-- delete from dbo.LogDataChangeDuplication
-- update dbo.LogDataChangeDuplication set OperationType = 1

select idDuplicateOf, * from dbo.refOwners owners where owners.Name in (N'!1', N'!2', N'!3', N'!4') and id != idDuplicateOf

/*
update dbo.refOutlets set deleted = 0, idDuplicateOf = 562954288022769, idOwner = 281474992259720 where id = 562954288022769
update dbo.refOwners set deleted = 0, idDuplicateOf = 281474992259720 where id = 281474992259720

update dbo.refOutlets set deleted = 0, idDuplicateOf = 562954287967601, idOwner = 562954287964594 where id = 562954287967601
update dbo.refOwners set deleted = 0, idDuplicateOf = 562954287964594 where id = 562954287964594

update dbo.refOutlets set deleted = 0, idDuplicateOf = 562954287968604, idOwner = 562954287964596 where id = 562954287968604
update dbo.refOwners set deleted = 0, idDuplicateOf = 562954287964596 where id = 562954287964596

update dbo.refOutlets set deleted = 0, idDuplicateOf = 562954287969607, idOwner = 562954287965596 where id = 562954287969607
update dbo.refOwners set deleted = 0, idDuplicateOf = 562954287965596 where id = 562954287965596

update dbo.refOutlets set deleted = 0, idDuplicateOf = 562954287970610, idOwner = 562954287964594 where id = 562954287970610
update dbo.refOwners set deleted = 0, idDuplicateOf = 562954287964594 where id = 562954287964594

update dbo.refOutlets set deleted = 0, idDuplicateOf = 562954287971613, idOwner = 562954287964596 where id = 562954287971613
update dbo.refOwners set deleted = 0, idDuplicateOf = 562954287964596 where id = 562954287964596
*/

/*
update dbo.refOutlets set deleted = 0, idDuplicateOf = 562954287988676, idOwner = 562954287987671 where id = 562954287988676
update dbo.refOwners set deleted = 0, idDuplicateOf = 562954287987671 where id = 562954287987671

update dbo.refOutlets set deleted = 0, idDuplicateOf = 562954287988679, idOwner = 562954287987671 where id = 562954287988679
update dbo.refOwners set deleted = 0, idDuplicateOf = 562954287987671 where id = 562954287987671
--
update dbo.refOutlets set deleted = 1, idDuplicateOf = 562954287988673, idOwner = 562954287987671 where id = 562954287988676
update dbo.refOwners set deleted = 1, idDuplicateOf = 562954287986669 where id = 562954287987671

*/

/*
update dbo.refOutlets set deleted = 0, idDuplicateOf = 562954287960572 where id = 562954287960572
update dbo.refOutlets set deleted = 0, idDuplicateOf = 562954287960575 where id = 562954287960575
*/

select * from dbo.refOutletDistributor where idOutlet in (13229323910270632, 562949953510251) and idDistributor = 281474976711134

--insert into dbo.refOutletDistributor (id, idOutlet, idDistributor) values (dbo.fn_getIdEx(281474992256801, 1), 562954287943533, 281474992256801)

select
	*
from
	dbo.refDistributors

select
	*
from
	dbo.refOutlets outlets
	join dbo.refAttributesValues attributesValues on attributesValues.idElement = outlets.id
	join dbo.refFilePackets filePackets on filePackets.id = attributesValues.idPacket
	join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idPacket = filePackets.id
	join dbo.refFiles files on files.id = filePacketsFiles.idFile
	join dbo.EnumsBase enumsLoadType on enumsLoadType.id = files.idLoadType
where
	outlets.id = 281475001239902
	--outlets.deleted = 0
	and attributesValues.deleted = 0
	and filePackets.deleted = 0
	and filePacketsFiles.deleted = 0
	and files.deleted = 0

/*
declare @idFile bigint
select @idFile = id from dbo.refFiles where [FileName] = N'simpsons.jpg'
delete from dbo.refFilePacketsFiles where idFile = @idFile
delete from dbo.refFiles where id = @idFile
*/

select
	*
from
	dbo.refOutlets outlets1
	join dbo.refOutlets outlets2 on outlets2.id = outlets1.idDuplicateOf
where
	outlets2.id is null

select
	*
from
	dbo.refOutlets outlets
	join dbo.refOwners owners on owners.id = outlets.idOwner
	join dbo.refCounteragents counteragents on counteragents.idOwner = owners.id
where
	outlets.deleted = 0
	and owners.deleted = 0
	and counteragents.deleted = 0

select
	*
from
	dbo.refOutlets outlets
	join dbo.refOwners owners on owners.id = outlets.idOwner
	join dbo.refCounteragents counteragents on counteragents.idOwner = owners.id
where
	outlets.id = 281475016483176

	and owners.deleted = 0
	and counteragents.deleted = 0


select
	*
from
	dbo.refOutlets outlets
where
	outlets.id = 281474995419344
	outlets.Code in (N'210000000000001')


select
	*
from
	dbo.refOutlets outlets
	join dbo.refOutletDistributor outletDistributor on outletDistributor.idOutlet = outlets.id
where
	outlets.Name like N'Не создаются контексты ТТ КБ ЦО при связывании дубля%'

select
	vOutletGridFields.id,
	count(*)
from
	dbo.vOutletGridFields vOutletGridFields
group by vOutletGridFields.id
having count(*) > 1;

--update dbo.refOutlets set Code = N'210000000000002' where Code = N' 210000000000002'

select
	*
from
	dbo.refOutlets outlets
	left join dbo.refPropertiesHistory propertiesHistory on propertiesHistory.idItem = outlets.idNetwork
where
	outlets.Name = N'Что-то задублировались записи в refPropertiesHistory 2'

--update dbo.refOutlets set idNetwork = 0 where id = 562954287900421

select
	*
from
	dbo.refOutlets outlets
	left join dbo.vOutletGridFields vOutletGridFields on vOutletGridFields.id = outlets.idView
	left join dbo.refOutlets vendorOutlets on vendorOutlets.id = vOutletGridFields.idVendorOutlet
where
	vendorOutlets.id is null

select
	*
from
	dbo.refOutlets vendorOutlets
	join dbo.refOutletMatches outletMatches on outletMatches.idVendorOutlet = vendorOutlets.id
	left join dbo.refOutlets distributorOutlets on distributorOutlets.id = outletMatches.idDistributorOutlet
where
	distributorOutlets.id is null

select
	*
from
	dbo.refOutlets distributorOutlets
	join dbo.refOutletMatches outletMatches on outletMatches.idDistributorOutlet = distributorOutlets.id
	left join dbo.refOutlets vendorOutlets on vendorOutlets.id = outletMatches.idVendorOutlet
where
	--vendorOutlets.id = 281475001239902
	vendorOutlets.Name = N'ТТ КБ ЦО (6)'

delete
	dbo.refOutletMatches
from
	dbo.refOutletMatches outletMatches
	join dbo.refOutlets vendorOutlets on vendorOutlets.id = outletMatches.idVendorOutlet
where
	--vendorOutlets.id = 281475001239902
	vendorOutlets.Name = N'ТТ КБ ЦО (6)'
