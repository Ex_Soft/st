if exists (select 1 from syscolumns where id = object_id(N'dbo.refOutlets', N'u') and name = N'CodeInManufacturesErp')
	exec sp_rename @objname = N'dbo.refOutlets.CodeInManufacturesErp', @newname = N'Code', @objtype = N'COLUMN'

if exists (select 1 from syscolumns where id = object_id(N'dbo.refOwners', N'u') and name = N'CodeInManufacturesErp')
	exec sp_rename @objname = N'dbo.refOwners.CodeInManufacturesErp', @newname = N'Code', @objtype = N'COLUMN'

if exists (select 1 from syscolumns where id = object_id(N'dbo.refCounteragents', N'u') and name = N'CodeInManufacturesErp')
	exec sp_rename @objname = N'dbo.refCounteragents.CodeInManufacturesErp', @newname = N'Code', @objtype = N'COLUMN'

select
	*
from
	sys.columns [columns]
	join sys.objects [objects] on [objects].[object_id] = [columns].[object_id]
where
	--[columns].name = N'idOutlet'
	[columns].name = N'idBuyPoint'
	and [objects].[type] = N'u'
order by [objects].name

select distinct
	object_name([sysdepends].id) as ParentName,
	object_name([sysdepends].depid) as ChildName,
	object_definition([sysdepends].id) as ParentDefinition
from
	sys.sysdepends [sysdepends]
where
	[sysdepends].id = 1714105147
	or [sysdepends].depid = 1714105147

--drop function fn_GetDocName

select
	*
from
	sys.sysdepends [sysdepends]
	join sys.objects [objects1] on [objects1].[object_id] = [sysdepends].id
	join sys.objects [objects2] on [objects2].[object_id] = [sysdepends].depid
where
	([sysdepends].id = 1714105147 or [sysdepends].depid = 1714105147)
	and ([objects1].type = N'fn' or [objects2].type = N'fn')

if exists (select * from sys.columns where object_id = object_id(N'dbo.DocJournal', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.DocJournal.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select 1 from sys.columns where object_id = object_id(N'dbo.drPackageSales', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.drPackageSales.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select 1 from sys.columns where object_id = object_id(N'dbo.refDCDistributions', N'u') and name = N'idDCBuypoint')
	exec sp_rename @objname = 'dbo.refDCDistributions.idDCBuypoint', @newname = 'idDCOutlet', @objtype = 'COLUMN';

if exists (select 1 from sys.columns where object_id = object_id(N'dbo.refPlanningValues', N'u') and name = N'idBuypointSet')
	exec sp_rename @objname = 'dbo.refPlanningValues.idBuypointSet', @newname = 'idOutletSet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.refOutletsMT', N'u') and name = N'CodeInManufacturesErp')
	exec sp_rename @objname = 'dbo.refOutletsMT.CodeInManufacturesErp', @newname = 'Code', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.snsAuditPhotos', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.snsAuditPhotos.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.LogDataChangeDoubles', N'u') and name = N'idLoadedBuyer')
	exec sp_rename @objname = 'dbo.LogDataChangeDoubles.idLoadedBuyer', @newname = 'idLoadedOwner', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.rgRouteSnapshotsData', N'u') and name = N'idBuypoint')
	exec sp_rename @objname = 'dbo.rgRouteSnapshotsData.idBuypoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.jrnVisits', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.jrnVisits.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.rgPromo', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.rgPromo.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.rgPromoCondition', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.rgPromoCondition.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.dhBonusIssued', N'u') and name = N'idBuyer')
	exec sp_rename @objname = 'dbo.dhBonusIssued.idBuyer', @newname = 'idOwner', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.dhPreOrder', N'u') and name = N'idBuyer')
	exec sp_rename @objname = 'dbo.dhPreOrder.idBuyer', @newname = 'idOwner', @objtype = 'COLUMN';
if exists (select * from sys.columns where object_id = object_id(N'dbo.dhPreOrder', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.dhPreOrder.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.dhSales', N'u') and name = N'idBuyer')
	exec sp_rename @objname = 'dbo.dhSales.idBuyer', @newname = 'idOwner', @objtype = 'COLUMN';
if exists (select * from sys.columns where object_id = object_id(N'dbo.dhSales', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.dhSales.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.dhBankReceipt', N'u') and name = N'idBuyer')
	exec sp_rename @objname = 'dbo.dhBankReceipt.idBuyer', @newname = 'idOwner', @objtype = 'COLUMN';
if exists (select * from sys.columns where object_id = object_id(N'dbo.dhBankReceipt', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.dhBankReceipt.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.dhExpenseCashOrder', N'u') and name = N'idBuyer')
	exec sp_rename @objname = 'dbo.dhExpenseCashOrder.idBuyer', @newname = 'idOwner', @objtype = 'COLUMN';
if exists (select * from sys.columns where object_id = object_id(N'dbo.dhExpenseCashOrder', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.dhExpenseCashOrder.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.dhGoodsReturn', N'u') and name = N'idBuyer')
	exec sp_rename @objname = 'dbo.dhGoodsReturn.idBuyer', @newname = 'idOwner', @objtype = 'COLUMN';
if exists (select * from sys.columns where object_id = object_id(N'dbo.dhGoodsReturn', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.dhGoodsReturn.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.dhMoneyRequest', N'u') and name = N'idBuyer')
	exec sp_rename @objname = 'dbo.dhMoneyRequest.idBuyer', @newname = 'idOwner', @objtype = 'COLUMN';
if exists (select * from sys.columns where object_id = object_id(N'dbo.dhMoneyRequest', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.dhMoneyRequest.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.dhReceiptCashOrder', N'u') and name = N'idBuyer')
	exec sp_rename @objname = 'dbo.dhReceiptCashOrder.idBuyer', @newname = 'idOwner', @objtype = 'COLUMN';
if exists (select * from sys.columns where object_id = object_id(N'dbo.dhReceiptCashOrder', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.dhReceiptCashOrder.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.jrnAgentEvents', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.jrnAgentEvents.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if exists (select * from sys.columns where object_id = object_id(N'dbo.dhMerchandise', N'u') and name = N'idBuyPoint')
	exec sp_rename @objname = 'dbo.dhMerchandise.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if col_length(N'dbo.drPackageGoodsReturn', N'idBuyPoint') > 0
	exec sp_rename @objname = 'dbo.drPackageGoodsReturn.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if col_length(N'dbo.dhPOSM', N'idBuyPoint') > 0
	exec sp_rename @objname = 'dbo.dhPOSM.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if col_length(N'dbo.drWorkingDayOfAgent', N'idBuyPoint') > 0
	exec sp_rename @objname = 'dbo.drWorkingDayOfAgent.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if col_length(N'dbo.dhAdvertEquipment', N'idBuyPoint') > 0
	exec sp_rename @objname = 'dbo.dhAdvertEquipment.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if col_length(N'dbo.dhPhotoReport', N'idBuyPoint') > 0
	exec sp_rename @objname = 'dbo.dhPhotoReport.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if col_length(N'dbo.drReceiptCashOrder', N'idBuyPoint') > 0
	exec sp_rename @objname = 'dbo.drReceiptCashOrder.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';

if col_length(N'dbo.refAttributesDefValues', N'idBuyPoint') > 0
	exec sp_rename @objname = 'dbo.refAttributesDefValues.idBuyPoint', @newname = 'idOutlet', @objtype = 'COLUMN';
