select
	*
from
	dbo.XPObjectType
where
	--TypeName like N'%StuAttributesValue%'
	--OID = 111
	TypeName like N'Chicago2.Core.StuBaseCore.StuObjects.StuDistributor%'

select
	*
from
	dbo.XPObjectType
order by TypeName

select
	TypeName,
	count(*)
from
	dbo.XPObjectType
group by TypeName
having count(*) > 1
order by TypeName

--update dbo.XPObjectType set TypeName = N'*Chicago2.Core.StuBaseCore.StuObjects.StuDistributor' where TypeName = N'Chicago2.Core.StuBaseCore.StuObjects.StuDistributor'
--delete from dbo.XPObjectType where TypeName = N'Chicago2.Core.StuBaseCore.StuObjects.StuDistributor'