/*
update dbo.drPhotoReport set photoFileName = N'http://st-drive.systtech.ru/9b8568edde3f4cbb9e375101a0f396f4' where id = 562954288024774
update dbo.refFiles set FileName = N'System', idMimeType = 0, Uri = N'', idLoadType = 0 where id = 0
*/

select
	*
from
	dbo.refFiles files
	join dbo.refFilePacketsFiles filePacketsFiles on filePacketsFiles.idFile = files.id
	join dbo.refFilePackets filePackets on filePackets.id = filePacketsFiles.idPacket
	join dbo.drPhotoReport dr on dr.idPacket = filePackets.id
where
	dr.id = 562954288024774

select
	*
from
	dbo.DocJournal docJournal
	join dbo.dhPhotoReport dh on dh.id = docJournal.id
	join dbo.drPhotoReport dr on dr.idDoc = dh.id
	--join dbo.refDistributors d on d.NodeID = cast(dj.id/281474976710656 as smallint)
where
	docJournal.id = 562954288024773
	--docJournal.PrintDocNum = N'4210000000000001'
	--cast(docJournal.OpDate as date) = N'20180122'
	--docJournal.id = 41940892519854262
	--and d.deleted=0

select
	dr.id,
	dr.photoFileName,
	*
from
	dbo.drPhotoReport dr
	join dbo.DocJournal docJournal on docJournal.id = dr.idDoc
where
	--dr.id = 562954288024774
	dr.photoFileName like N'http%'
	and (docJournal.idStatus = 1 or (docJournal.idStatus = 3 and docJournal.deleted = 1))
