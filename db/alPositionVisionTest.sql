﻿if object_id(N'refPhysicalPersons', N'u') is null
	begin
		create table refPhysicalPersons
		(
			id bigint not null constraint pk_refPhysicalPersons_ID primary key,
			Name nvarchar(250) not null,
			deleted bit not null constraint df_refPhysicalPersons_deleted default 0
		);

		insert into refPhysicalPersons (id, Name, deleted) values (0, N'System', 1);
		insert into refPhysicalPersons (id, Name) values (281474976712779, N'Сот-2 (Право-2)');
        insert into refPhysicalPersons (id, Name) values (281474976712780, N'Сот-3 (Право-2)');
        insert into refPhysicalPersons (id, Name) values (281474976712781, N'Сот-2.1 (Право-2)');
		insert into refPhysicalPersons (id, Name) values (281474976712782, N'Сот-2.1.1 (Право-2)');
        insert into refPhysicalPersons (id, Name) values (281474976712783, N'Сот-2.1.0 (Право-1)');
        insert into refPhysicalPersons (id, Name) values (281474976712784, N'Сот-2.2 (Право-1)');
        insert into refPhysicalPersons (id, Name) values (281474976712785, N'Сот-2.3 (Право-1)');
        insert into refPhysicalPersons (id, Name) values (281474976712786, N'Сот-2.4 (Право-2)');
        insert into refPhysicalPersons (id, Name) values (281474976712787, N'Сот-3.1 (Право-2)');
        insert into refPhysicalPersons (id, Name) values (281474976712788, N'Сот-ЦО (Право-2)');
        insert into refPhysicalPersons (id, Name) values (281474976712789, N'Сот-ЦО.1 (Право-1)');
	end;

if object_id(N'refPositions', N'u') is null
	begin
		create table refPositions
		(
			id bigint not null constraint pk_refPositions_ID primary key,
			idParent bigint not null constraint fk_refPositions_ID_refPositions_idParent foreign key references refPositions(id),
			idChief bigint not null constraint df_refPositions_idChief default 0,
			Name nvarchar(250) not null,
			deleted bit not null constraint df_refPositions_deleted default 0
		);
		
		insert into refPositions (id, idParent, Name, deleted) values (0, 0, N'System', 1);
        insert into refPositions (id, idParent, Name) values (281474976712779, 0, N'Корень. (Дистр ЦО) (Сот-ЦО) (в доступ предоставлен Сот-ЦО.1)');
        insert into refPositions (id, idParent, Name) values (281474976712780, 281474976712779, N'Поза-2 (Дистр-2) (Сот-2) (в доступ предоставлен Сот-2.1.0)');
        insert into refPositions (id, idParent, Name) values (281474976712781, 281474976712779, N'Поза-3 (Дистр-3) (Сот-3) (в доступ предоставлен Сот-3.1)');
        insert into refPositions (id, idParent, Name) values (281474976712782, 281474976712779, N'Поза-2.3 (Дистр-2.1) (Сот-2.3)')
        insert into refPositions (id, idParent, Name) values (281474976712783, 281474976712779, N'Поза-2.4 (Дистр-2.1) (Сот-2.4)');
        insert into refPositions (id, idParent, Name) values (281474976712784, 281474976712780, N'Поза-2.1 (Дистр-2.1) (Сот-2.1) (в доступ предоставлен Сот-2.1.1)');
        insert into refPositions (id, idParent, Name) values (281474976712785, 281474976712780, N'Поза-2.2 (Дистр-2.1) (Сот-2.2)');
	end;

if object_id(N'refEmployeesPositions', N'u') is null
	begin
		create table refEmployeesPositions
		(
			id bigint not null constraint pk_refEmployeesPositions_ID primary key,
			idPosition bigint not null constraint df_refEmployeesPositions_idPosition default 0,
			idEmployee bigint not null constraint df_refEmployeesPositions_idEmployee default 0,
			deleted bit not null constraint df_refEmployeesPositions_deleted default 0,
			constraint fk_refPositions_ID_refEmployeesPositions_idPosition foreign key (idPosition) references refPositions(id),
			constraint fk_refPhysicalPersons_ID_refEmployeesPositions_idEmployee foreign key (idEmployee) references refPhysicalPersons(id)
		);

		insert into refEmployeesPositions (id, idPosition, idEmployee, deleted) values (0, 0, 0, 1);

		insert into refEmployeesPositions (id, idPosition, idEmployee) values (281474976712779, 281474976712779, 281474976712788);
		update refPositions set idChief = 281474976712779 where id = 281474976712779

		insert into refEmployeesPositions (id, idPosition, idEmployee) values (281474976712780, 281474976712780, 281474976712779);
		update refPositions set idChief = 281474976712780 where id = 281474976712780

		insert into refEmployeesPositions (id, idPosition, idEmployee) values (281474976712781, 281474976712781, 281474976712780);
		update refPositions set idChief = 281474976712781 where id = 281474976712781

		insert into refEmployeesPositions (id, idPosition, idEmployee) values (281474976712782, 281474976712782, 281474976712785);
		update refPositions set idChief = 281474976712782 where id = 281474976712782

		insert into refEmployeesPositions (id, idPosition, idEmployee) values (281474976712783, 281474976712783, 281474976712786);
		update refPositions set idChief = 281474976712783 where id = 281474976712783

		insert into refEmployeesPositions (id, idPosition, idEmployee) values (281474976712784, 281474976712784, 281474976712781);
		update refPositions set idChief = 281474976712784 where id = 281474976712784

		insert into refEmployeesPositions (id, idPosition, idEmployee) values (281474976712785, 281474976712785, 281474976712784);
		update refPositions set idChief = 281474976712785 where id = 281474976712785

		insert into refEmployeesPositions (id, idPosition, idEmployee) values (281474976712786, 281474976712779, 281474976712789);
		insert into refEmployeesPositions (id, idPosition, idEmployee) values (281474976712787, 281474976712780, 281474976712783);
		insert into refEmployeesPositions (id, idPosition, idEmployee) values (281474976712788, 281474976712781, 281474976712787);
		insert into refEmployeesPositions (id, idPosition, idEmployee) values (281474976712789, 281474976712784, 281474976712782);
	end;

if object_id(N'alPositionVision', N'u') is null
	create table alPositionVision
	(
		id bigint not null identity constraint pk_alPositionVision_ID primary key,
		idPosition bigint not null constraint fk_refPositions_ID_alPositionVision_idPosition foreign key references refPositions(id),
		idEmployee bigint not null constraint fk_refPhysicalPersons_ID_alPositionVision_idEmployee foreign key references refPhysicalPersons(id),
		tableVision int not null,
		deleted bit not null constraint df_alPositionVision_deleted default 0,
		visible bit not null constraint df_alPositionVision_visible default 1
	);
go

----------------------------------------------------------------------------------------------
-- Процедура - sp_sys_UpdateTableVision - обновление регистра области видимости
----------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_sys_UpdateTableVision]
AS
BEGIN

-- 2 as tableVision - дочерние
-- 3 as tableVision - родительские
-- 1 as tableVision - вся своя ветка (2+3)
-- 4 - ЦО

	declare @idDistributor bigint
	set @idDistributor = 281474976710657

	declare @tv table (tableVision int primary key)
	
	declare @tableVision int
	set @tableVision = 1

	while @tableVision < 5
	begin	
		insert into @tv (tableVision)
		values(@tableVision)
		set @tableVision = @tableVision + 1
	end

--	select * from @tv

	declare @pe table (idPosition bigint, idParent bigint, idEmployee bigint, [level] int, tableVision int, primary key (idPosition, idEmployee, [level], tableVision, idParent))

	declare @level int
	set @level = 0

	-- получаем сотрудников на позициях - 1, 2, 3
	insert into @pe (idPosition, idParent, idEmployee, [level], tableVision)
	select distinct ps.id as idPosition, ps.idParent, ep.idEmployee, @level as [level], tv.tableVision as tableVision
	from dbo.refPositions          ps with (nolock)
	join dbo.refEmployeesPositions ep with (nolock) on ep.idPosition = ps.id and ep.id > 0 and ep.deleted = 0 and ep.idEmployee > 0
	join dbo.refPhysicalPersons    pp with (nolock) on pp.id = ep.idEmployee and pp.id > 0 and pp.deleted = 0
	join @tv                   tv               on tv.tableVision in (1, 2, 3) 
	where ps.id > 0 and ps.deleted = 0

	-- получаем подчиненные позиции - 1, 2
--	while exists(select top 1 idPosition from @pe where [level] = @level and tableVision in (1,2))
	while @@rowcount > 0
	begin
		set @level = @level + 1
		insert into @pe (idPosition, idParent, idEmployee, [level], tableVision)
		select distinct ps.id as idPosition, ps.idParent, pe.idEmployee, @level as [level], pe.tableVision as tableVision
		from @pe          pe
		join dbo.refPositions ps with (nolock) on ps.idParent = pe.idPosition and ps.deleted = 0 and ps.id > 0
		where pe.[level] = @level - 1 and pe.tableVision in (1,2)
	end

	set @level = 0

	-- получаем родительские позиции - 1, 3
--	while exists(select top 1 idPosition from @pe where [level] = @level and tableVision in (1,3))
	while @@rowcount > 0
	begin
		set @level = @level - 1
		insert into @pe (idPosition, idParent, idEmployee, [level], tableVision)
		select distinct ps.id as idPosition, ps.idParent, pe.idEmployee, @level as [level], pe.tableVision as tableVision
		from @pe          pe
		join dbo.refPositions ps with (nolock) on ps.id = pe.idParent and ps.deleted = 0 and ps.id > 0
		where pe.[level] = @level + 1 and pe.tableVision in (1,3)
	end

	set @level = 0

	-- получаем позиции ЦО - 4
	insert into @pe (idPosition, idParent, idEmployee, [level], tableVision)
	select distinct ps.id as idPosition, ps.idParent, pp.id, @level as [level], tv.tableVision as tableVision
	from dbo.refPositions             ps with (nolock)
	join dbo.refPhysicalPersons       pp with (nolock) on pp.id > 0 and pp.deleted = 0
	join @tv                      tv on tv.tableVision = 4
	where ps.id > 0 and ps.deleted = 0 and ps.idParent = 0

	declare @p table (idPosition bigint, idEmployee bigint, tableVision int, primary key (idPosition, idEmployee, tableVision))
	insert into @p (idPosition, idEmployee, tableVision)
	select distinct idPosition, idEmployee, tableVision
	from @pe pe

	-- удаляем лишние
	update dbo.alPositionVision set deleted = 1
	from dbo.alPositionVision pv
	where deleted = 0 and not exists (select top 1 pe.idPosition from @p pe where pv.idPosition = pe.idPosition and pv.idEmployee = pe.idEmployee and pe.tableVision = pv.tableVision)

	-- востанавливаем удаленные
	update dbo.alPositionVision set deleted = 0 where deleted = 1 and id in
	(
		select max(id)
		from @p pe
		join dbo.alPositionVision pv on pv.idPosition = pe.idPosition and pv.idEmployee = pe.idEmployee and pe.tableVision = pv.tableVision
		group by pe.idPosition, pe.idEmployee, pe.tableVision
		having min(case when deleted = 1 then 1 else 0 end) = 1
	)

	-- добавляем недостающие
	insert into dbo.alPositionVision(idPosition, idEmployee, tableVision, deleted)
	select pe.idPosition, pe.idEmployee, pe.tableVision, 0
	from @p pe 
	left join dbo.alPositionVision pv on pv.idPosition = pe.idPosition and pv.idEmployee = pe.idEmployee and pe.tableVision = pv.tableVision --and pv.deleted = 0
	where pv.id is null

--select * from @pe where TableVision > 4
--select * from @pe --where idEmployee = 281474976711460 --and Level = 0 and TableVision = 2
END
GO
