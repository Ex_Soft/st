﻿select * from dbo.refSegments where Name = N'70963 Зависает Чикаго при редактировании маршрута'
--insert into dbo.refRouteTerritory (id, idRoute, idOutlet, idSegment, deleted) values (dbo.fn_getIdEx(281474976711134, 1), 562954287993685, 0, 562954287992678, 0)
--delete from dbo.refRouteTerritory where idRoute = 562954287993685 and idOutlet = 281474992219585
select * from dbo.refOutletPosition where idPosition = 281474977159395 and idOutlet in (281474992219542, 281474992219585)
--delete from dbo.refOutletPosition where idPosition = 281474977159395 and idOutlet = 281474992219542

select
	[routes].Name,
	count(*)
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
where
	[routes].deleted = 0
	and routeTerritory.deleted = 0
group by [routes].Name
order by 2 desc

select
	*
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	left join dbo.refOutletPosition outletPosition on outletPosition.idPosition = [routes].idPosition and outletPosition.idOutlet = routeTerritory.idOutlet and outletPosition.deleted = 0
where
	[routes].Name = N'70963 Зависает Чикаго при редактировании маршрута'
	and routeTerritory.idOutlet != 0
	and routeTerritory.deleted = 0
	and outletPosition.id is null

select * from dbo.refRoutes where Name = N'Маршрут с ТТ КБ ЦО и ТТ КБ Дистра (by import)'

select
	*
from
	dbo.refRoutes [routes]
	left join dbo.rgPDAInfo pdaInfo on pdaInfo.idRoute = [routes].id and pdaInfo.deleted = 0
where
	[routes].deleted = 0
	and [routes].idDistributor = 281474976711134 -- 281474976710657

--insert into dbo.rgPDAInfo (idRoute, DeviceID, ModelName, CpuFrequency, MainMemSize, MainMemSpace, FlashMemSize, FlashMemSpace, AppFirstStarted, VerMajor, VerMinor, SubVerMajor, SubVerMinor, deleted) values (2251799820118083, N'4G23DW38500C', N'', 0, 83782652, 55455136, 0, 0, getdate(), 4, 0, 49, 0, 0)

select * from dbo.refRoutes where deleted = 0 and IsCanConvertToSOCO = 1
--update dbo.refRoutes set IsCanConvertToSOCO = 0, IsConvertToSOCO = 0
--update dbo.refRoutes set IsCanConvertToSOCO = 1, IsConvertToSOCO = 1 where id = 562954287599199
select * from dbo.refDistributors where AutoConvertRoutes = 1
--update dbo.refDistributors set AutoConvertRoutes = 0

select
	*
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory  on routeTerritory.idRoute = [routes].id
	left join dbo.refOutlets outlets on outlets.id = routeTerritory.idOutlet
	--join dbo.refOutlets outletsD on outletsD.id = outlets.idDuplicateOf
	--join dbo.refSets [sets] on [sets].idItem = routes.id
where
	[routes].Name like N'67744%'
	[routes].id = 564564862030672
	and routeTerritory.id = 562956009863167
	and routeTerritory.deleted = 0
	and routeTerritory.idOutlet = 564564861888747
	routeTerritory.id in (2251799813733538, 2251799820117999) or routeTerritory.idOutlet in (2251799813733538, 2251799820117999)

select
	*
	--, N'''' + outlets.Code + N'''' as RealCode
from
	dbo.refRoutes [routes]
	left join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	left join dbo.refOutlets outlets on outlets.id = routeTerritory.idOutlet
where
	[routes].Name like N'73255%'
	--[routes].Name = N'Дублируются ТТ в территории при связке дубля'
	--[routes].Name like N'Дублируются ТТ в территории при связке дубля%'
	--and routeTerritory.deleted = 0
	and routeTerritory.idSegment = 0
order by routeTerritory.idOutlet

/*
update dbo.refRouteTerritory set idOutlet = 562954287960572, deleted = 0 where id = 562954287960581
update dbo.refRouteTerritory set idOutlet = 562954287960575, deleted = 0 where id = 562954287960582
update dbo.refRouteTerritory set idOutlet = 562954287960572, deleted = 0 where id = 562954287962581
update dbo.refRouteTerritory set idOutlet = 562954287960575, deleted = 0 where id = 562954287962582
*/

select
	*
from
	dbo.refRoutes [routes]
	left join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	left join dbo.refSegments segments on segments.id = routeTerritory.idSegment
	left join dbo.refSegmentOutlet segmentOutlet on segmentOutlet.idSegment = segments.id
	left join dbo.refOutlets outlets on outlets.id = segmentOutlet.idOutlet
where
	--[routes].Name = N'Route With outdated outlets' --N'Route# 1 (CH3)'
	--[routes].Name like N'%54859%'
	--[routes].id = 562954286610233
	[routes].deleted = 0
	--segments.Name = N'Indifferent segment# 1'
	and routeTerritory.deleted = 0
	and segments.deleted = 0
	and segmentOutlet.deleted = 0

--update dbo.refRouteTerritory set RangeFrom = dateadd(day, 1, eomonth(getdate(), -1)), RangeTo = eomonth(getdate()) where id = 562954286610235

select
	*
from
	dbo.refRoutes [routes]
	left join dbo.refRouteVisits routeVisits on routeVisits.idRoute = [routes].id
	--left join dbo.refOutlets outlets on outlets.id = routeVisits.idOutlet
where
	[routes].Name like N'73255%'
	--[routes].Name = N'Дублируются ТТ в территории при связке дубля'
	--[routes].Name like N'Дублируются ТТ в территории при связке дубля%'
	--and routeVisits.deleted = 0
order by routeVisits.Visit, routeVisits.VisitOrder

--delete dbo.refRouteVisits from dbo.refRouteVisits routeVisits join dbo.refRoutes [routes] on [routes].id = routeVisits.idRoute where [routes].Name = N'68820 Доп. посещение преобразуется в основное посе'
--update dbo.refRouteVisits set deleted = 1 from dbo.refRouteVisits routeVisits join dbo.refRoutes [routes] on [routes].id = routeVisits.idRoute where [routes].Name = N'68820 Доп. посещение преобразуется в основное посе'

/*
update dbo.refRouteVisits set idOutlet = 562954287960572, deleted = 0 where id = 562954287960583
update dbo.refRouteVisits set idOutlet = 562954287960575, deleted = 0 where id = 562954287960584
update dbo.refRouteVisits set idOutlet = 562954287960575, deleted = 0 where id = 562954287961574
update dbo.refRouteVisits set idOutlet = 562954287960572, deleted = 0 where id = 562954287961575
update dbo.refRouteVisits set idOutlet = 562954287960572, deleted = 0 where id = 562954287961576
update dbo.refRouteVisits set idOutlet = 562954287960575, deleted = 0 where id = 562954287961577
update dbo.refRouteVisits set idOutlet = 562954287960572, deleted = 0 where id = 562954287961578
update dbo.refRouteVisits set idOutlet = 562954287960575, deleted = 0 where id = 562954287961579
update dbo.refRouteVisits set idOutlet = 562954287960572, deleted = 0 where id = 562954287962583
update dbo.refRouteVisits set idOutlet = 562954287960575, deleted = 0 where id = 562954287962584
update dbo.refRouteVisits set idOutlet = 562954287960572, deleted = 0 where id = 562954287962585
update dbo.refRouteVisits set idOutlet = 562954287960575, deleted = 0 where id = 562954287962586
update dbo.refRouteVisits set idOutlet = 562954287960572, deleted = 0 where id = 562954287962587
update dbo.refRouteVisits set idOutlet = 562954287960575, deleted = 0 where id = 562954287962588
update dbo.refRouteVisits set idOutlet = 562954287960572, deleted = 0 where id = 562954287962589
update dbo.refRouteVisits set idOutlet = 562954287960575, deleted = 0 where id = 562954287962590
*/

--delete from dbo.refRouteVisits where id in (562954287881139,562954287883142,562954287881140,562954287883143,562954287882141)

--update dbo.refRouteVisits set Visit = null, VisitDate = cast(VisitFromTime as date) where id in (562954287872100, 562954287872101)
--update dbo.refRouteVisits set VisitFromTime = dateadd(day, -1, VisitFromTime) where id in (562954287872100)
--update dbo.refRouteVisits set VisitToTime = dateadd(day, -1, VisitToTime) where id in (562954287872100)
--update dbo.refRouteVisits set VisitDate = dateadd(day, -1, VisitDate) where id in (562954287872100)

/*
delete from dbo.refRouteTerritory where deleted = 1

delete
  dbo.refRouteTerritory
from
	dbo.refRouteTerritory routeTerritory
	left join dbo.refOutlets outlets on outlets.id = routeTerritory.idOutlet
where
	outlets.id is null;
*/

/*
delete from dbo.refRouteVisits where deleted = 1

delete
  dbo.refRouteVisits
from
	dbo.refRouteVisits routeVisits
	left join dbo.refOutlets outlets on outlets.id = routeVisits.idOutlet
where
	outlets.id is null;
*/

select
	*
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	--join dbo.refRouteVisits routeVisits on routeVisits.idRoute = [routes].id
	--join dbo.refBuyPoints buyPoints on buyPoints.id = routeVisits.idBuyPoint
	left join dbo.refOutlets outlets on outlets.id = routeTerritory.idOutlet
where
	outlets.id is null;

--update dbo.refRouteVisits set deleted = 0 where id in (562954286651511, 562954286651517)

/*
update
	dbo.refRouteVisits
set
	Visit = 0
from
	dbo.refRoutes [routes]
	join dbo.refRouteVisits routeVisits on routeVisits.idRoute = [routes].id
where
	[routes].Name = N'With add visits with Visit equ 0'
	and routeVisits.deleted = 0
	and routeVisits.Visit is null

update dbo.refRouteVisits set Visit = null where id = 562954286612237

*/

-----------------------------------------------------------

;with buyPointsInSegments (idBuyPoint)
as
(
select
	segmentBuyPoints.idBuyPoint
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	join dbo.refSegments [segments] on [segments].id = routeTerritory.idSegment
	join dbo.refSegmentBuyPoints segmentBuyPoints on segmentBuyPoints.idSegment = [segments].id
where
	[routes].Name = N'Some sales outlets both in territory && in segment'
	and routeTerritory.deleted = 0
	and [segments].deleted = 0
	and segmentBuyPoints.deleted = 0
)
select
	*
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	join buyPointsInSegments buyPointsInSegments on buyPointsInSegments.idBuyPoint = routeTerritory.idBuyPoint
	join dbo.refBuyPoints buyPoints on buyPoints.id = routeTerritory.idBuypoint
where
	[routes].Name = N'Some sales outlets both in territory && in segment'
	and routeTerritory.deleted = 0

-----------------------------------------------------------

select
	*
from
	dbo.refRoutes [routes]
	join dbo.refRouteTerritory routeTerritory on routeTerritory.idRoute = [routes].id
	join dbo.refSegments [segments] on [segments].id = routeTerritory.idSegment
	join dbo.refSegmentBuyPoints segmentBuyPoints on segmentBuyPoints.idSegment = [segments].id
	join dbo.refBuyPoints buyPoints on buyPoints.id = segmentBuyPoints.idBuypoint
where
	[routes].Name = N'48600 [рег.тест] Экспортируются ТТ удаленного сегм'
	--and routeTerritory.deleted = 0
	and [segments].deleted = 0
	and segmentBuyPoints.deleted = 0
	and buyPoints.deleted = 0
order by routeTerritory.idSegment, segmentBuyPoints.idBuypoint
------------------------------------------------------------

--update dbo.refRouteTerritory set deleted = 0 where id = 562954286651504

select
	*
from
	dbo.refRoutes routes
	join dbo.refSets [sets] on [sets].idItem = routes.id
	join dbo.refStoreSets storeSets on storeSets.idSet = [sets].id
where
	routes.Name = N'Маршрут с сетами складов'

------------------------------------------------------------

select
	*
from
	dbo.refRoutes r
	join dbo.refRouteVisits rv on rv.idRoute = r.id
	join dbo.EnumsBase eRWCT on eRWCT.id = r.idRouteWeekCountType
	join dbo.ConstsBase c on c.idDistr = r.idDistributor and c.CodeKey = N'RouteWeekCount'
	join dbo.EnumsBase e on e.id = c.Value
where
	r.id = 562954286321863
	and rv.deleted = 0
	and rv.Visit = 17

------------------------------------------------------------

select
	*
from
	dbo.ConstsBase c
	join dbo.EnumsBase eg on eg.id = c.idGroup
	join dbo.EnumsBase ev on ev.id = cast(c.Value as bigint)
where
	c.Deleted = 0
	and c.CodeKey = N'PreorderRestsStorageType'

select
	*
from
	dbo.refRoutes [routes]
	left join dbo.refSets [sets] on [sets].idItem = [routes].id
	left join dbo.refStoreSets storeSets on storeSets.idSet = [sets].id
	left join dbo.refStores stores on stores.id = storeSets.idStore and stores.deleted = 0
	left join dbo.enStoreTypesBase storeTypes on storeTypes.id = storeSets.idStoreType
where
	[routes].Code = N'1077007'

/*
delete
	dbo.refStoreSets
from
	dbo.refStoreSets storeSets
	join dbo.refSets [sets] on [sets].id = storeSets.idSet
	join dbo.refRoutes [routes] on [routes].id = [sets].idItem
where
	[sets].Field = N'StoreType'
	and [sets].TableName = N'refStoreSets'
	and [routes].Code = N'1077007'

delete
	dbo.refSets
from
	dbo.refSets [sets]
	join dbo.refRoutes [routes] on [routes].id = [sets].idItem
where
	[sets].Field = N'StoreType'
	and [sets].TableName = N'refStoreSets'
	and [routes].Code = N'1077007'
*/
------------------------------------------------------------

select
	*
from
	dbo.refRoutes r
	join dbo.refRouteTerritory rt on rt.idRoute = r.id
	join dbo.refBuyPoints bp on bp.idMain = rt.idBuyPoint
where
	r.id = 93731167351516773
	and rt.deleted = 0

select
	vRV.idBuyPoint,
	*
from
	refRouteVisits rv
	join vRouteVisits vRV on (vRV.id=rv.idvRouteVisits)
where
	--(rv.idBuyPoint=24769797950787682)
	(rv.id=24769797962347541)

select
	vRT.idBuyPoint,
	*
from
	refRouteTerritory rt
	join vRouteTerritory vRT on (vRT.id=rt.idvRouteTerritory)
where
	(rt.idRoute=25614222880672720)
	and (rt.idBuyPoint in (24769797950787682, 24769797950787681, 25332747905533046))

select
	bp.id,
	bp.idDistributor,
	bp.isLocal,
	d.Name,
	*
from
	refBuyPoints bp
	join refDistributors d on (d.id=bp.idDistributor)
where
	(bp.idMain=24769797950787682)
order by bp.id

select
	vBP.id,
	vBP.idDistributor,
	vBP.Distributors,
	*
from
	vBPGridFields vBP
where
	(vBP.id in (24769797950787682, 24769797950787681, 25332747905533046))
order by vBP.id

------------------------------------------------------------

select
	bp.Name,
	*
from
	refRouteVisits rv
	join vRouteVisits vRV on (vRV.id=rv.idvRouteVisits)
	join refBuyPoints bp on (bp.id=vRV.idBuyPoint)
where
	(rv.idRoute=25614222880672720)
	--and (rv.Visit=7)
	--and (rv.Visit=6)
	and (rv.Visit=13)
	--and (rv.deleted=0)
order by rv.VisitOrder
/*
delete from refRouteVisits where id in (24769797962347532, 24769797962347533)
*/

/*
select
	*
from
	refBuyPoints bp
where
	(bp.idMain=24769797961796296)
*/

select * from dbo.EnumsBase where CodeKey like N'ConstType!_RouteWeekCount!_%' escape N'!' or CodeKey = N'Route_WeekCountType_Const'