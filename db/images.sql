﻿/*
Не используй для тестов http://st-drive.systtech.ru/
Используй: http://st-drive-002.azurewebsites.net/
*/
select * from dbo.ConstsBase where CodeKey = N'MaxPhotoSize'
--update dbo.ConstsBase set Value = 1600 where CodeKey = N'MaxPhotoSize'
select * from dbo.SettingsBase where CodeKey = N'PhotoFilesServerPath'
select * from dbo.ConstsBase where CodeKey = N'PhotoExchangeAddress'
--update dbo.ConstsBase set Deleted = 0 where CodeKey = N'PhotoExchangeAddress'
--update dbo.ConstsBase set Deleted = 1 where CodeKey = N'PhotoExchangeAddress'

select * from dbo.SettingsBase where CodeKey = N'PhotoFilesServerPath'
select * from dbo.ConstsBase where CodeKey = N'PhotoExchangeAddress'
-- update dbo.ConstsBase set Deleted = 1 where CodeKey = N'PhotoExchangeAddress'
-- update dbo.ConstsBase set Value = N'http://st-drive.systtech.ru/' where CodeKey = N'PhotoExchangeAddress'
-- update dbo.ConstsBase set Value = N'http://st-drive-002.azurewebsites.net/' where CodeKey = N'PhotoExchangeAddress'

--update dbo.drPhotoReport set photoFileName = N'http://st-drive.systtech.ru/9b8568edde3f4cbb9e375101a0f396f4' where id = 562954288024774

select * from dbo.drPhotoReport where id = 562954288024774
select * from dbo.drSurveyPhotos
select * from dbo.drWorkingDayOfAgentPhotos

select * from dbo.drPhotoReport where cast(photoTime as date) = N'20170419'
select * from dbo.drSurveyPhotos where cast(photoTime as date) = N'20170419'
select * from dbo.drWorkingDayOfAgentPhotos where cast(photoTime as date) = N'20170419'

select len(photoFileName), substring(photoFileName, 1 , 2), substring(photoFileName, 10 , len(photoFileName) - 9), * from dbo.drPhotoReport where photoFileName like N'\\storage\%'
--update dbo.drPhotoReport set photoFileName = substring(photoFileName, 1 , 2) + N'storage.systtech.ru' + substring(photoFileName, 10 , len(photoFileName) - 9) where photoFileName like N'\\storage\%'
select * from dbo.drSurveyPhotos where photoFileName like N'\\storage\%'
--update dbo.drSurveyPhotos set photoFileName = substring(photoFileName, 1 , 2) + N'storage.systtech.ru' + substring(photoFileName, 10 , len(photoFileName) - 9) where photoFileName like N'\\storage\%'

select * from dbo.refFiles where FileName like N'\\storage\%'
--update dbo.refFiles set FileName = substring(FileName, 1 , 2) + N'storage.systtech.ru' + substring(FileName, 10 , len(FileName) - 9) where FileName like N'\\storage\%'

select * from dbo.vAuditPhotos
select * from dbo.vAuditPhotos where DocDate between '20140101 00:00:00' and '20161024 00:00:00' and DistributorName = N'ChocolateFood, Kalinigrad'

EXEC Reports.report_xls_PhotoAudit
@paramStart = '20140101 00:00:00', @paramEnd = '20161024 00:00:00', @enDistributors = '281474976711134', @enPositions = '', @enQstTemplates = '', @enPhotoObjects = '', @enAgentTasks = '', @IsRatingHistory = 'False', @need_empty = 'False', @Table_Test = '', @enOperations = '33 32 30', @idPhysicalPerson = '0', @guid = '4dd990b3fa764bea9623390edb1bfa61'

-- update dbo.drPhotoReport set photoFileName = N'http://89.249.23.82:88/api/image/' + photoFileName where id = 14355223812247469

-- http://st-drive-dev.azurewebsites.net
-- http://st-drive.systtech.ru/api/image/

/*
bdd5cd1a-6353-4cdd-bb28-882f14513f82
04a83e85-8714-44d6-b167-7f7b0d27d0c2
*/
select * from dbo.SettingsBase where CodeKey = N'CompanyId'

select * from dbo.SettingsBase where CodeKey = N'HTTPPhotoReports';
--update dbo.SettingsBase set Value = N'http://localhost/exchange' where CodeKey = N'HTTPPhotoReports';

select * from dbo.SettingsBase where CodeKey = N'ImageProcessingServer';
--update dbo.SettingsBase set Value = N'http://localhost/ImageProcessing/' where CodeKey = N'ImageProcessingServer';

select * from dbo.SettingsBase where CodeKey = N'UNCReplacePattern';
--insert into dbo.SettingsBase (CodeKey, [Type], Value, [Description], [Description_0]) values (N'UNCReplacePattern', N'Service', N'^\\.+?\', N'Регулярное выражение для преобразования UNC в URI', N'Regular expression for transform UNC to URI')
--update dbo.SettingsBase set Value = N'^\\\\.+?\\.+?\\' where CodeKey = N'UNCReplacePattern';

------------------------------------------------------------

declare @use_minipic bit = 0; -- использовать сервис-генератор мини-картинок (необходимо установить отдельный веб-компонент)

--	адрес сервера фотоотчетов
declare @http nvarchar(255);
select @http = Value from dbo.Settings where CodeKey = N'HTTPPhotoReports';

--	адрес сервиса-генератора превьюшек
declare @image_processor nvarchar(max);
select @image_processor = (Value + 'image-preview.aspx?image=') from dbo.Settings where CodeKey = N'ImageProcessingServer';

select
	rtrim(fl.FileName),
	reverse((fl.FileName)),
	substring(reverse(rtrim(fl.FileName)), 1,
		case
			when charindex('\', reverse(rtrim(fl.FileName))) > 0
				then charindex('\',reverse(rtrim(fl.FileName))) - 1
				else len(rtrim(fl.FileName))
		end),
	[file_path]	=
		case
			when charindex('http://', fl.FileName) = 0
				then
					case
						when @use_minipic = 1
							then @image_processor + reverse(substring(reverse(rtrim(fl.FileName)), 1,
								case
									when charindex('\',reverse(rtrim(fl.FileName))) > 0 
										then charindex('\',reverse(rtrim(fl.FileName))) - 1
										else len(rtrim(fl.FileName)) 
								end)) + '&scale=y'
							else 'file://' + rtrim(fl.FileName)
					end
				else
					case
						when charindex('.jpg', fl.FileName) > 0
							then substring(fl.FileName, 1, charindex('.jpg', fl.FileName) - 1)
							else fl.FileName
					end
		end,
	[http_path] =
	case
		when charindex('http://', fl.FileName) = 0
			then cast(@http + reverse(substring(reverse(rtrim(fl.FileName)), 1,
				case
					when charindex('\', reverse(rtrim(fl.FileName))) > 0
						then charindex('\',reverse(rtrim(fl.FileName))) - 1
						else len(rtrim(fl.FileName))
				end)) as nvarchar(2000))
			else
				case when charindex('.jpg', fl.FileName) > 0
					then substring(fl.FileName, 1, charindex('.jpg', fl.FileName) - 1)
					else fl.FileName
				end
	end,
	[photo_link] = rtrim(fl.FileName)
from
	dbo.refFiles fl
