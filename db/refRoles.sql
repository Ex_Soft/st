﻿select
	--objectTypesRights.id,
	*
from
	dbo.refObjects [objects]
	join dbo.EnumsBase enumsObjectType on enumsObjectType.id = [objects].idObjectType
	join dbo.EnumsBase enumsObjectName on enumsObjectName.id = [objects].idObjectName
	join dbo.refObjectTypesRights objectTypesRights on objectTypesRights.idObjectType = [objects].idObjectType
	join dbo.EnumsBase enumsRight on enumsRight.id = objectTypesRights.idRight
where
	[objects].deleted = 0
	and enumsObjectType.CodeKey = N'Object_Type_Journal'
	and enumsObjectName.CodeKey = N'Object_Name_AgentGPSs'
	and objectTypesRights.deleted = 0
	and enumsRight.CodeKey = N'OrgStructure_Right_JournalEdit'

;merge into dbo.refObjectTypesRights as tgt
using
(
	select
		dbo.fn_getIdEx(id, 1) as id,
		(select id from dbo.EnumsBase where CodeKey = N'Object_Type_Journal') as idObjectType,
		(select id from dbo.EnumsBase where CodeKey = N'Object_Name_AgentGPSs') as idRight,
		0 as DefaultValue,
		0 as deleted
	from
		dbo.refDistributors
	where
		IsRoot = 1 and deleted = 0
) as src
on tgt.idObjectType = src.idObjectType and tgt.idRight = src.idRight and tgt.deleted = src.deleted
when matched
	then
		update set DefaultValue = src.DefaultValue
when not matched
	then
		insert (id, idObjectType, idRight, DefaultValue, deleted)
		values (src.id, src.idObjectType, src.idRight, src.DefaultValue, src.deleted);

select
	cast(objectTypesRights.id/281474976710656 as smallint) as NodeID,
	*		
from
	dbo.refObjectTypesRights objectTypesRights
	join dbo.EnumsBase enumsRight on enumsRight.id = objectTypesRights.idRight
	join dbo.refObjects [objects] on [objects].idObjectType = objectTypesRights.idObjectType
	join dbo.EnumsBase enumsObjectType on enumsObjectType.id = [objects].idObjectType
	join dbo.EnumsBase enumsObjectName on enumsObjectName.id = [objects].idObjectName
where
	[objects].deleted = 0
	and enumsRight.CodeKey = N'OrgStructure_Right_AllowSettings'
	and enumsObjectType.CodeKey = N'Object_Type_Utils'
	and enumsObjectName.CodeKey = N'Object_Name_UlsUsersRegistration'

select
	*
from
	dbo.refObjects [objects]
	join dbo.EnumsBase enumsObjectType on enumsObjectType.id = [objects].idObjectType
	join dbo.EnumsBase enumsObjectName on enumsObjectName.id = [objects].idObjectName
	left join dbo.refObjectTypesRights objectTypesRights on objectTypesRights.idObjectType = [objects].idObjectType
	left join dbo.EnumsBase enumsRight on enumsRight.id = objectTypesRights.idRight
where
	--[objects].id = 26
	enumsObjectType.CodeKey = N'Object_Type_References'
	and enumsObjectName.CodeKey = N'Object_Name_Addresses'

select
	*
from
	dbo.refPhysicalPersons physicalPersons with (nolock)
	join dbo.refEmployeesPositions employeesPositions with (nolock) on employeesPositions.idEmployee = physicalPersons.id
	join dbo.refEmployeesRoles employeesRoles with (nolock) on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
	join dbo.refRoleRights roleRights with (nolock) on roleRights.idRole = employeesRoles.idRole
	join dbo.refRoles [roles] on [roles].id = employeesRoles.idRole
	join dbo.EnumsBase enumsRight with (nolock) on enumsRight.id = roleRights.idRight
	join dbo.refObjects [objects] with (nolock) on [objects].id = roleRights.idObject
	join dbo.EnumsBase enumsObjectType on enumsObjectType.id = [objects].idObjectType
	join dbo.EnumsBase enumsObjectName on enumsObjectName.id = [objects].idObjectName
where
	physicalPersons.deleted = 0
	--and physicalPersons.Name = N'BuyPoints editor'
	and employeesPositions.deleted = 0
	and employeesRoles.deleted = 0
	and roleRights.deleted = 0
	and [objects].deleted = 0
	and enumsObjectName.CodeKey = N'Object_Name_Planning'

select
	*
from
	dbo.refRoles roles with (nolock)
	join dbo.refRoleRights roleRights with (nolock) on roleRights.idRole = roles.id
	join dbo.EnumsBase enumsRight with (nolock) on enumsRight.id = roleRights.idRight
	join dbo.EnumsBase enumsType with (nolock) on enumsType.id = roleRights.idType
	join dbo.refObjects [objects] with (nolock) on [objects].id = roleRights.idObject
	join dbo.EnumsBase enumsObjectType with (nolock) on enumsObjectType.id = [objects].idObjectType
	join dbo.EnumsBase enumsObjectName with (nolock) on enumsObjectName.id = [objects].idObjectName
	left join dbo.refRecvisitRights recvisitRights with (nolock) on recvisitRights.idRoleRight = roleRights.id
where
	roles.deleted = 0
	--and roles.Name = N'BuyPoints editor'
	and roleRights.deleted = 0
	and [objects].deleted = 0
	and enumsObjectType.CodeKey = N'Object_Type_References'
	and enumsObjectName.CodeKey = N'Object_Name_Planning'

select
	*
from
	dbo.refRoles roles
where
	roles.deleted = 0
	and roles.Name like N'Агент only #%'

/*
update dbo.refRoles set HasUserRights = 0 where id = 281475013736315
update dbo.refRoles set HasUserRights = 0 where id = 281475013737585
*/
/*
update dbo.refRoles set HasUserRights = 1 where id = 281475013736315
update dbo.refRoles set HasUserRights = 1 where id = 281475013737585
*/

select
	*
from
	dbo.refObjectTypesRights objectTypesRights
	join dbo.EnumsBase enumsObjectType on enumsObjectType.id = objectTypesRights.idObjectType
	join dbo.EnumsBase enumsRight on enumsRight.id = objectTypesRights.idRight
where
	objectTypesRights.id = 281474990516968
	or enumsObjectType.CodeKey = N'Object_Type_MTSettings'

select
	--physicalPersons.id,
	--physicalPersons.Name,
	--count(*)
	*
from
	dbo.refPhysicalPersons physicalPersons with (nolock)
	join dbo.refEmployeesPositions employeesPositions with (nolock) on employeesPositions.idEmployee = physicalPersons.id
	join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
	join dbo.refEmployeesRoles employeesRoles with (nolock) on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
	join dbo.refRoles roles with (nolock) on roles.id = employeesRoles.idRole
where
	physicalPersons.deleted = 0
	and employeesPositions.deleted = 0
	and positions.deleted = 0
	--and employeesRoles.deleted = 0
	and roles.deleted = 0
--group by physicalPersons.id, physicalPersons.Name
--having count(*) > 1
order by physicalPersons.Name;

select
	*
from
	dbo.refPhysicalPersons physicalPersons with (nolock)
	left join dbo.refEmployeesPositions employeesPositions with (nolock) on employeesPositions.idEmployee = physicalPersons.id
	left join dbo.refPositions positions with (nolock) on positions.id = employeesPositions.idPosition
	left join dbo.refEmployeesRoles employeesRoles with (nolock) on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
	left join dbo.refRoles roles with (nolock) on roles.id = employeesRoles.idRole
	left join dbo.refRoleRights roleRights with (nolock) on roleRights.idRole = roles.id
	left join dbo.EnumsBase enumsRight with (nolock) on enumsRight.id = roleRights.idRight
	left join dbo.refObjects o with (nolock) on o.id = roleRights.idObject
	left join dbo.EnumsBase enumsObjectType with (nolock) on enumsObjectType.id = o.idObjectType
	left join dbo.EnumsBase enumsObjectName with (nolock) on enumsObjectName.id = o.idObjectName
	left join dbo.refRecvisitRights recvisitRights with (nolock) on recvisitRights.idRoleRight = roleRights.id
where
	physicalPersons.Name = N'PhysicalPerson Position Editor'
	and enumsObjectType.CodeKey = N'Object_Type_References'
	and enumsObjectName.CodeKey = N'Object_Name_Positions'
	and recvisitRights.CodeKey = N'Classifier4'

select distinct
	enumObjectName.CodeKey,
	enumRight.CodeKey
from
	dbo.refRoles roles
	join dbo.refRoleRights roleRights on roleRights.idRole = roles.id
	join dbo.EnumsBase enumRight on enumRight.id = roleRights.idRight
	join dbo.EnumsBase enumType on enumType.id = roleRights.idType
	join dbo.refObjects o on o.id = roleRights.idObject
	join dbo.EnumsBase enumObjectType on enumObjectType.id = o.idObjectType
	join dbo.EnumsBase enumObjectName on enumObjectName.id = o.idObjectName
where
	roles.deleted = 0
	and roleRights.deleted = 0
	and o.deleted = 0
	and enumType.CodeKey = N'Right_Type_Enum'
order by enumObjectName.CodeKey, enumRight.CodeKey

select
	*
from
	dbo.refRoles roles
	join dbo.refRoleRights roleRights on roleRights.idRole = roles.id
	join dbo.EnumsBase enumRight on enumRight.id = roleRights.idRight
	join dbo.EnumsBase enumType on enumType.id = roleRights.idType
	join dbo.refObjects o on o.id = roleRights.idObject
	join dbo.EnumsBase enumObjectType on enumObjectType.id = o.idObjectType
	join dbo.EnumsBase enumObjectName on enumObjectName.id = o.idObjectName
where
	roles.deleted = 0
	and roleRights.deleted = 0
	and o.deleted = 0
	and roleRights.Value not in (N'false', N'true', N'0', N'1')
	and enumType.CodeKey = N'Right_Type_Bit'

;with cte (idRoleRights)
as
(
	select
		id
	from
		dbo.refRoleRights roleRights
	where
		roleRights.deleted = 0
		and roleRights.Value not in (N'false', N'true', N'0', N'1')
)
select
	*
from
	dbo.refRoleRights roleRights
	join cte cte on cte.idRoleRights = roleRights.id
	join dbo.refRoles roles on roles.id = roleRights.idRole
	join dbo.EnumsBase er on er.id = roleRights.idRight

/* все опции/права */
select
	*
from
	dbo.refObjectTypesRights objectTypesRights
	join dbo.refObjects o on o.idObjectType = objectTypesRights.idObjectType
	join dbo.EnumsBase enumsObjectType on enumsObjectType.id = objectTypesRights.idObjectType
	join dbo.EnumsBase enumsObjectName on enumsObjectName.id = o.idObjectName
	join dbo.EnumsBase enumsRight on enumsRight.id = objectTypesRights.idRight
where
	objectTypesRights.deleted = 0
	and o.deleted = 0
	--and enumsObjectType.CodeKey = N'Object_Type_MTSettings'
	--and enumsObjectType.CodeKey like N'Object!_Type!_MT%' escape N'!'
	--and enumsObjectType.CodeKey = N'Object_Type_MTInformation'
	--and enumsObjectType.CodeKey = N'Object_Type_Utils'
	--and enumsObjectType.CodeKey = N'Object_Type_BPSettings'
	and enumsRight.CodeKey = N'OrgStructure_Right_AllowSettings'
order by
	enumsObjectType.Value

select
	*
from
	dbo.refObjects systemObjects
	join dbo.EnumsBase enumsObjectType on enumsObjectType.id = systemObjects.idObjectType
	join dbo.EnumsBase enumsObjectName on enumsObjectName.id = systemObjects.idObjectName
where
	enumsObjectType.CodeKey = N'Object_Type_Utils'
	and enumsObjectName.CodeKey = N'Object_Name_ConvertRoutes'

------------------------------------------------------------

/* все позиции с конкретной/м опцией/правом */

select
	*
from
	dbo.refRoleRights roleRights
	join dbo.EnumsBase er on er.id = roleRights.idRight
	join dbo.EnumsBase et on et.id = roleRights.idType
	join dbo.refObjects o on o.id = roleRights.idObject
	join dbo.EnumsBase eot on eot.id = o.idObjectType
	join dbo.EnumsBase eon on eon.id = o.idObjectName
	join dbo.refRoles roles on roles.id = roleRights.idRole
	join dbo.refEmployeesRoles employeesRoles on employeesRoles.idRole = roles.id
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.id = employeesRoles.idEmployeesPositionsRoles
where
	roleRights.deleted = 0
	and er.CodeKey = N'OrgStructure_Right_AllowSettings'
	and et.CodeKey = N'Right_Type_Bit'
	and o.deleted = 0
	and eot.CodeKey = N'Object_Type_Utils'
	and eon.CodeKey = N'Object_Name_ConvertRoutes'
	and roles.deleted = 0
	and employeesRoles.deleted = 0
	and employeesPositions.deleted = 0
	and ((lower(ltrim(rtrim(coalesce(roleRights.Value, N'')))) = N'true') or (lower(ltrim(rtrim(coalesce(roleRights.Value, N'')))) = N'1'))

------------------------------------------------------------

/* права по конкретной роли */
select
	*
from
	dbo.refRoles roles
	join dbo.refRoleRights roleRights on roleRights.idRole = roles.id
	join dbo.EnumsBase enumsRight on enumsRight.id = roleRights.idRight
	join dbo.refObjects o on o.id = roleRights.idObject
	join dbo.EnumsBase enumsObjectType on enumsObjectType.id = o.idObjectType
	join dbo.EnumsBase enumsObjectName on enumsObjectName.id = o.idObjectName
where
  roles.Name = N'Position Editor'
  and roles.deleted = 0
  and roleRights.deleted = 0
  and o.deleted = 0
  and enumsObjectType.CodeKey=N'Object_Type_BPSettings'

------------------------------------------------------------

select
  *
from
  refRoleRights rr
  join refRoles r on (r.id=rr.idRole)
  join refObjects o on (o.id=rr.idObject)
  join enums er on (er.id=rr.idRight)
  join enums eot on (eot.id=o.idObjectType)
  join enums eon on (eon.id=o.idObjectName)
where
  (rr.deleted=0)
  and (r.deleted=0)
  and (o.deleted=0)
  and (er.CodeKey = N'OrgStructure_Right_AllowBPSettingsEdit')
  and (eot.codekey=N'Object_Type_BPSettings')
  and (eon.codekey in (N'Object_Name_BPSettings_Street', N'Object_Name_BPSettings_Name'))
order by rr.idRole, rr.idObject, rr.idRight

------------------------------------------------------------

select
  *
from
  refObjectTypesRights otr
  join enums eot on (eot.id=otr.idObjectType)
  join enums er on (er.id=otr.idRight)
where
  (otr.deleted=1)
  and (eot.codekey = N'Object_Type_MTInformation')
  and (er.CodeKey in (N'OrgStructure_Right_EditBuyPointName', N'OrgStructure_Right_EditBuyPointAddress'))

------------------------------------------------------------

select
  *
from
  refPivotReports pr
  join refStoredProcedures sp on (sp.id=pr.idStoredProcedure)
  join refPivotReportRights prr on (prr.idPivotReport=pr.id)
  join enums er on (er.id=prr.idRight)
  join refRoles r on (r.id=prr.idRole)
where
  (pr.deleted=0)
  and (sp.deleted=0)
  and (sp.IsForReport=1)
  and (prr.deleted=0)
  and (r.deleted=0)
  and (r.Name=N'Агент')
  and (prr.enabled=1)

------------------------------------------------------------

select
	--employeesRoles.*,
	--employeesPositions.*,
	*
from
	dbo.refRoles roles
	join dbo.refEmployeesRoles employeesRoles on employeesRoles.idRole = roles.id
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.id = employeesRoles.idEmployeesPositionsRoles
	join dbo.refPhysicalPersons physicalPersons on physicalPersons.id = employeesPositions.idEmployee
where
  roles.deleted = 0
  and employeesRoles.deleted = 0
  and employeesPositions.deleted = 0
  and physicalPersons.deleted = 0
  and physicalPersons.LastName = N'DC Editor'
  --and physicalPersons.LastName in (N'DC Editor', N'Алексеев')

-- update dbo.refEmployeesPositions set idEmployee = 281475001159872 where id = 281475001159902
------------------------------------------------------------

/* права на справочники */

select
	*
from
	dbo.refRoles roles with (nolock)
	join dbo.refRoleRights roleRights with (nolock) on roleRights.idRole = roles.id
	join dbo.EnumsBase enumsRight with (nolock) on enumsRight.id = roleRights.idRight
	join dbo.EnumsBase enumsType with (nolock) on enumsType.id = roleRights.idType
	join dbo.refObjects [objects] with (nolock) on [objects].id = roleRights.idObject
	join dbo.EnumsBase enumsObjectType with (nolock) on enumsObjectType.id = [objects].idObjectType
	join dbo.EnumsBase enumsObjectName with (nolock) on enumsObjectName.id = [objects].idObjectName
	left join dbo.refRecvisitRights recvisitRights with (nolock) on recvisitRights.idRoleRight = roleRights.id
where
	--roles.id = 281474998519619
	roles.Name = N'BuyPoints Editor'
	and roleRights.deleted = 0
	--and enumsRight.CodeKey = N'OrgStructure_Right_RefEditOnly'
	--and enumsRight.CodeKey = N'OrgStructure_Right_RefEditOnly' and ((lower(ltrim(rtrim(coalesce(roleRights.Value, N'')))) = N'true') or (lower(ltrim(rtrim(coalesce(roleRights.Value, N'')))) = N'1'))
	--and enumsRight.CodeKey = N'OrgStructure_Right_RefEditAll' and ((lower(ltrim(rtrim(coalesce(roleRights.Value, N'')))) = N'true') or (lower(ltrim(rtrim(coalesce(roleRights.Value, N'')))) = N'1'))
	and [objects].deleted = 0
	and enumsObjectType.CodeKey = N'Object_Type_References'
	and enumsObjectName.CodeKey = N'Object_Name_Buypoints'
	--and enumsObjectName.CodeKey in (N'Object_Name_Buypoints', N'Object_Name_BuyPointsCO')
	--and recvisitRights.CodeKey = N'Classifier4'
	and recvisitRights.CodeKey like N'add%'
	and recvisitRights.deleted = 0


select
	*
from
	dbo.refPhysicalPersons physicalPersons
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idEmployee = physicalPersons.id
	join dbo.refPositions positions on positions.id = employeesPositions.idPosition
	join dbo.refEmployeesRoles employeesRoles on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
	join dbo.refRoles roles on roles.id = employeesRoles.idRole
	join dbo.refEmployeesRoles employeesRoles on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
	join dbo.refRoleRights roleRights on roleRights.idRole = employeesRoles.idRole
	join dbo.EnumsBase enumsRight on enumsRight.id = roleRights.idRight
	join dbo.refObjects o on o.id = roleRights.idObject
	join dbo.EnumsBase enumsObjectType on enumsObjectType.id = o.idObjectType
	join dbo.EnumsBase enumsObjectName on enumsObjectName.id = o.idObjectName
	join dbo.refRecvisitRights recvisitRights on recvisitRights.idRoleRight = roleRights.id
where
	employeesPositions.idEmployee = 562954287595195
	and enumsObjectType.CodeKey = N'Object_Type_References'
	and enumsObjectName.CodeKey = N'Object_Name_Positions'
	--and (eon.CodeKey=N'Object_Name_RegionalNetworks')
	and enumsRight.CodeKey in (N'OrgStructure_Right_RefEditAll', N'OrgStructure_Right_RefEditOnly')
	--and (er.CodeKey=N'OrgStructure_Right_RefEditOutwith')
	and recvisitRights.CodeKey like N'Classifier%'
	and ((lower(ltrim(rtrim(coalesce(roleRights.Value, N'')))) = N'true') or (lower(ltrim(rtrim(coalesce(roleRights.Value, N'')))) = N'1'))
	and employeesRoles.deleted = 0
	and roleRights.deleted = 0
	and o.deleted = 0
	and recvisitRights.deleted = 0

select
  *
from
  refRoleRights rr
  join refRoles r on (r.id=rr.idRole)
  join refObjects o on (o.id=rr.idObject)
  join EnumsBase er on (er.id=rr.idRight)
  join EnumsBase eot on (eot.id=o.idObjectType)
  join EnumsBase eon on (eon.id=o.idObjectName)
  join refRecvisitRights recvr on (recvr.idRoleRight=rr.id)
where
  (rr.idRole=281474976710769)
  and (rr.idObject=26)
  and (rr.deleted=0)
  and (r.deleted=0)
  and (o.deleted=0)
  and (recvr.deleted=0)

/* все челы с данной ролью */

select
	physicalPersons.Name,
	*
from
	dbo.refRoles roles
	join dbo.refEmployeesRoles employeesRoles on employeesRoles.idRole = roles.id
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.id = employeesRoles.idEmployeesPositionsRoles
	join dbo.refPositions positions on positions.id = employeesPositions.idPosition
	join dbo.refPhysicalPersons physicalPersons on physicalPersons.id = employeesPositions.idEmployee
where
	--roles.Name = N'Право-2'
	physicalPersons.Name like N'Сот%'
	and roles.deleted = 0
	and employeesRoles.deleted = 0
	and employeesPositions.deleted = 0
	and positions.deleted = 0
	and physicalPersons.deleted = 0
order by roles.Name, physicalPersons.Name

/* все роли чела */

select
	*
from
	dbo.refPhysicalPersons physicalPersons
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idEmployee = physicalPersons.id
	join dbo.refPositions positions on positions.id = employeesPositions.idPosition
	join dbo.refEmployeesRoles employeesRoles on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
	join dbo.refRoles roles on roles.id = employeesRoles.idRole
where
	--physicalPersons.id = 9823552278677609
	physicalPersons.Name = N'Position Editor'
	and employeesPositions.deleted = 0
	and positions.deleted = 0
	and employeesRoles.deleted = 0
	and roles.deleted = 0

/* Наличие определенного права */

select
	*
from
	dbo.refPhysicalPersons physicalPersons
	join dbo.refEmployeesPositions employeesPositions on employeesPositions.idEmployee = physicalPersons.id
	join dbo.refEmployeesRoles employeesRoles on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
	join dbo.refRoleRights roleRights on roleRights.idRole = employeesRoles.idRole
	join dbo.EnumsBase enumsRight on enumsRight.id = roleRights.idRight
	join dbo.EnumsBase enumsType on enumsType.id = roleRights.idType
	join dbo.refObjects o on o.id = roleRights.idObject
	join dbo.EnumsBase enumsObjectType on enumsObjectType.id = o.idObjectType
	join dbo.EnumsBase enumsObjectName on enumsObjectName.id = o.idObjectName
	join dbo.refPositions positions on positions.id = employeesPositions.idPosition
	join dbo.refDistributors distributors on distributors.id = positions.idDistributor
where
	--physicalPersons.id = 14355223812246460
	physicalPersons.Name = N'999_01'
	--physicalPersons.Name = N'Position Editor'
	and employeesPositions.deleted = 0
	and employeesRoles.deleted = 0
	and roleRights.deleted = 0
	and enumsRight.CodeKey = N'OrgStructure_Right_AllowSettings'
	and enumsType.CodeKey = N'Right_Type_Bit'
	and o.deleted = 0
	and enumsObjectType.CodeKey = N'Object_Type_AdditionalRights'
	and enumsObjectName.CodeKey = N'Object_Name_AllowEditChildRecords'
	--and ((lower(ltrim(rtrim(coalesce(roleRights.Value, N'')))) = N'true') or (lower(ltrim(rtrim(coalesce(roleRights.Value, N'')))) = N'1'))
	and positions.deleted = 0
	and distributors.deleted = 0

;with cte(id, idParent, Name, Level, Path)
as
(
	select
		distributors.id as id,
		distributors.idParent as idParent,
		distributors.Name as Name,
		0 as Level,
		cast(N'/' + distributors.Name as varchar(max)) as Path
	from
		dbo.refPhysicalPersons physicalPersons
		join dbo.refEmployeesPositions employeesPositions on employeesPositions.idEmployee = physicalPersons.id
		join dbo.refEmployeesRoles employeesRoles on employeesRoles.idEmployeesPositionsRoles = employeesPositions.id
		join dbo.refRoleRights roleRights on roleRights.idRole = employeesRoles.idRole
		join dbo.EnumsBase enumsRight on enumsRight.id = roleRights.idRight
		join dbo.EnumsBase enumsType on enumsType.id = roleRights.idType
		join dbo.refObjects o on o.id = roleRights.idObject
		join dbo.EnumsBase enumsObjectType on enumsObjectType.id = o.idObjectType
		join dbo.EnumsBase enumsObjectName on enumsObjectName.id = o.idObjectName
		join dbo.refPositions positions on positions.id = employeesPositions.idPosition
		join dbo.refDistributors distributors on distributors.id = positions.idDistributor
	where
		--physicalPersons.id = 14355223812246460
		--physicalPersons.Name = N'999_01'
		physicalPersons.Name = N'Былина Елена'
		and employeesPositions.deleted = 0
		and employeesRoles.deleted = 0
		and	roleRights.deleted = 0
		and enumsRight.CodeKey = N'OrgStructure_Right_AllowSettings'
		and enumsType.CodeKey = N'Right_Type_Bit'
		and o.deleted = 0
		and enumsObjectType.CodeKey = N'Object_Type_AdditionalRights'
		and enumsObjectName.CodeKey = N'Object_Name_AllowEditChildRecords'
		and ((lower(ltrim(rtrim(coalesce(roleRights.Value, N'')))) = N'true') or (lower(ltrim(rtrim(coalesce(roleRights.Value, N'')))) = N'1'))
		and positions.deleted = 0
		and distributors.deleted = 0
	union all
	select
		distributors.id as id,
		distributors.idParent as idParent,
		distributors.Name as Name,
		Level + 1,
		cast(Path + N'/' + distributors.Name as varchar(max))
	from
		cte
		join dbo.refDistributors distributors on distributors.idParent = cte.id
	where
		distributors.deleted = 0
)
select
	*
from
	cte

------------------------------------------------------------
-- Добавление прав

-- D:\WorkSpaces\CHICAGO\DEV\db\src\scripts\business\systemdata\Enumerators.sql --
if not exists (select 1 from dbo.EnumsBase where CodeKey = N'Object_Name_ConvertRoutes')
	insert into dbo.EnumsBase (id, CodeKey, Value, MTCode, [Description], Value_0, Description_0)
	values (201609265750701, N'Object_Name_ConvertRoutes', N'Конвертация маршрутов', 0, N'Конвертация маршрутов', N'Converting routes', N'Converting routes')
-- D:\WorkSpaces\CHICAGO\DEV\db\src\scripts\business\systemdata\Enumerators.sql --

-- D:\WorkSpaces\CHICAGO\DEV\db\src\scripts\business\systemdata\RolesObjects.sql --
declare
	@id bigint,
	@idObjectType bigint,
	@idObjectName bigint,
	@OID int = 0,
	@deleted bit = 0

select @idObjectType = id from dbo.EnumsBase where CodeKey = N'Object_Type_Utils'
select @idObjectName = id from dbo.EnumsBase where CodeKey = N'Object_Name_ConvertRoutes'

if not exists (select 1 from dbo.refObjects where idObjectType = @idObjectType and idObjectName = @idObjectName)
	begin
		select @id = max(id) + 1 from dbo.refObjects where id < 10000
		insert into dbo.refObjects (id, idObjectType, idObjectName, OID, deleted)
		values (@id, @idObjectType, @idObjectName, @OID, @deleted)
	end
-- D:\WorkSpaces\CHICAGO\DEV\db\src\scripts\business\systemdata\RolesObjects.sql --
------------------------------------------------------------

------------------------------------------------------------
-- Добавление права

select * from dbo.EnumsBase where id in (201611045849001, 201611045849002, 201611045849003, 201611045849004, 201611045849005, 201611045849006) order by id
select * from dbo.EnumsBase where CodeKey like N'RightType!_%' escape N'!'
select * from dbo.refObjects where idObjectType = 406
--update dbo.EnumsBase set Description_0 = N'By clicking on the button "Start visit" and by clicking on the button "Finish visit"' where id = 201611045849004

-- D:\WorkSpaces\CHICAGO\DEV\db\src\scripts\business\systemdata\Enumerators.sql --
if not exists (select 1 from dbo.EnumsBase where CodeKey = N'OrgStructure_Right_ForbidStartFinishWork')
	insert into dbo.EnumsBase (id, CodeKey, Value, MTCode, [Description], Value_0, Description_0)
	values (201611045849001, N'OrgStructure_Right_ForbidStartFinishWork', N'Запрещать начало/завершение работы с ТТ при превышении максимально допустимого удаления от ТТ', 0, N'Запрещать начало/завершение работы с ТТ при превышении максимально допустимого удаления от ТТ', N'Forbid start/finish work with SO when exceed of the maximum allowable distance from the SO', N'Forbid start/finish work with SO when exceed of the maximum allowable distance from the SO')

if not exists (select 1 from dbo.EnumsBase where CodeKey = N'RightType_ForbidStartFinishWork_AtStartVisit')
	insert into dbo.EnumsBase (id, CodeKey, Value, MTCode, [Description], Value_0, Description_0)
	values (201611045849002, N'RightType_ForbidStartFinishWork_AtStartVisit', N'В начале визита', 0, N'При нажатии на кнопку “Начать визит”', N'At start visit', N'By clicking on the button "Start visit"')

if not exists (select 1 from dbo.EnumsBase where CodeKey = N'RightType_ForbidStartFinishWork_AtFinishVisit')
	insert into dbo.EnumsBase (id, CodeKey, Value, MTCode, [Description], Value_0, Description_0)
	values (201611045849003, N'RightType_ForbidStartFinishWork_AtFinishVisit', N'В конце визита', 0, N'При нажатии на кнопку “Завершить визит”', N'At finish visit', N'By clicking on the button "Finish visit"')

if not exists (select 1 from dbo.EnumsBase where CodeKey = N'RightType_ForbidStartFinishWork_AtStartFinishVisit')
	insert into dbo.EnumsBase (id, CodeKey, Value, MTCode, [Description], Value_0, Description_0)
	values (201611045849004, N'RightType_ForbidStartFinishWork_AtStartFinishVisit', N'В начале и в конце визита', 0, N'При нажатии на кнопку “Начать визит” и при нажатии на кнопку “Завершить визит”', N'At start and finish visit', N'By clicking on the button "Start visit" and by clicking on the button "Finish visit"')

if not exists (select 1 from dbo.EnumsBase where CodeKey = N'RightType_ForbidStartFinishWork_AtFinishVisitFrbd')
	insert into dbo.EnumsBase (id, CodeKey, Value, MTCode, [Description], Value_0, Description_0)
	values (201611045849005, N'RightType_ForbidStartFinishWork_AtFinishVisitFrbd', N'Уведомлять в конце визита, но не запрещать', 0, N'В случае отсутствия данных о координатах (хотя бы раз в течение периода визита), выводится запрос о завершении работы с ТТ', N'At finish of visit but doesn''t forbid', N'In the absence of data on the coordinates (at least once during the period of the visit), you are prompted about finish work with SO')

if not exists (select 1 from dbo.EnumsBase where CodeKey = N'RightType_ForbidStartFinishWork_No')
	insert into dbo.EnumsBase (id, CodeKey, Value, MTCode, [Description], Value_0, Description_0)
	values (201611045849006, N'RightType_ForbidStartFinishWork_No', N'Нет', 0, N'Проверка не производится', N'No', N'Doesn''t check')
-- D:\WorkSpaces\CHICAGO\DEV\db\src\scripts\business\systemdata\Enumerators.sql --

-- D:\WorkSpaces\CHICAGO\DEV\db\src\scripts\business\systemdata\RolesObjectTypes.sql --
declare
	@idObjectType bigint,
	@idDistributor bigint = dbo.fn_GetDefaultDistr(),
	@idRight bigint,
	@DefaultValue bit = 0,
	@deleted bit = 0

select @idObjectType = id from dbo.EnumsBase where CodeKey = N'Object_Type_MTSettings'
select @idRight = id from dbo.EnumsBase where CodeKey = N'OrgStructure_Right_ForbidStartFinishWork'

if not exists (select 1 from dbo.refObjectTypesRights where idObjectType = @idObjectType and idRight = @idRight)
	insert into dbo.refObjectTypesRights (id, idObjectType, idRight, DefaultValue, deleted) values (dbo.fn_getIdEx(@idDistributor, 1), @idObjectType, @idRight, @DefaultValue, @deleted)
-- D:\WorkSpaces\CHICAGO\DEV\db\src\scripts\business\systemdata\RolesObjectTypes.sql --

-- D:\WorkSpaces\CHICAGO\DEV\db\src\scripts\business\systemdata\RolesRights.sql  --
DECLARE 
	@id     bigint,
	@idRole bigint,
	@idObject bigint,
	@idRight  bigint,
	@Value nvarchar(50),
	@idDistributor bigint,
	@deleted bit,
	@idTypeEnum bigint

SET @idDistributor = dbo.fn_GetDefaultDistr()
SELECT @idTypeEnum = id FROM Enums WHERE CodeKey = N'Right_Type_Enum'

DECLARE RoleCursor cursor fast_forward read_only
FOR
	SELECT
		id as idRole
	FROM 	refRoles
	WHERE 	id > 0 and deleted = 0

OPEN RoleCursor

FETCH NEXT FROM RoleCursor INTO @idRole

WHILE @@FETCH_STATUS = 0
BEGIN

	----- Обьект - [Настройки] (Object_Name_MobileSettings) -----
	SELECT @idObject = [o].id FROM refObjects [o]
	JOIN Enums [ot] ON [ot].id = [o].idObjectType
	JOIN Enums [on] ON [on].id = [o].idObjectName
	WHERE [ot].CodeKey = 'Object_Type_MTSettings' AND [on].CodeKey = 'Object_Name_MobileSettings'

	----- Право - [Запрещать начало/завершение работы с ТТ при превышении максимально допустимого удаления от ТТ] (OrgStructure_Right_ForbidStartFinishWork) -----
	SELECT @idRight = id FROM Enums WHERE CodeKey = 'OrgStructure_Right_ForbidStartFinishWork'
	IF NOT EXISTS(SELECT * FROM refRoleRights WHERE idRole = @idRole AND idObject = @idObject AND idRight = @idRight)
	BEGIN
		SELECT @Value = convert(nvarchar(20), id), @deleted = 0
		FROM Enums WHERE CodeKey = 'RightType_ForbidStartFinishWork_No'
		EXEC sp_GetId @idDistributor, @id output
		INSERT refRoleRights (id, idRole, idObject, idRight, idType, Value, deleted)
		VALUES (@id, @idRole, @idObject, @idRight, @idTypeEnum, @Value, @deleted)
	END

FETCH NEXT FROM RoleCursor INTO @idRole
END

CLOSE RoleCursor
DEALLOCATE RoleCursor
-- D:\WorkSpaces\CHICAGO\DEV\db\src\scripts\business\systemdata\RolesRights.sql  --

------------------------------------------------------------