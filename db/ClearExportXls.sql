declare cur cursor read_only fast_forward
for
select Name from sys.objects where type = N'U' and Name like N'!_exportXls%' escape N'!'

declare
 @name sysname

open cur
fetch cur into @name
while @@fetch_status = 0
 begin
  exec(N'drop table [' + @name + N']')
  fetch cur into @name
 end
close cur
deallocate cur

delete from rpTempTables