select
	--attributesValues.*
	*
from
	dbo.refStores refs
	join dbo.refAttributesValues attributesValues on attributesValues.idElement = refs.id
	join dbo.refAttributesBase attributesBase on attributesBase.id = attributesValues.idAttribute
	join dbo.EnumsBase enumsAttrType on enumsAttrType.id = attributesBase.idAttrType
	join dbo.EnumsBase enumsValueType on enumsValueType.id = attributesBase.idValueType
	join dbo.EnumsBase enumsBaseObject on enumsBaseObject.id = attributesBase.idBaseObject

--update dbo.refStores set idStoreType = 1 where id = 21392098230017869