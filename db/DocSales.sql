﻿select
	*
from
	dbo.DocJournal docJournal
	join dbo.dhPackageSales dhPackage on dhPackage.id = docJournal.id
	join dbo.drPackageSales drPackage on drPackage.idDoc = dhPackage.id
	join dbo.refGoods goods on goods.id = drPackage.idGoods
	join dbo.refStores stores on stores.id = drPackage.idStore
where
	cast(docJournal.OpDate as date) = N'20171213'

select
	*
from
	dbo.DocJournal docJournal
	join dbo.dhSales dh on dh.id = docJournal.id
	join dbo.drSales dr on dr.idDoc = docJournal.id
	join dbo.EnumsBase enumsItemType on enumsItemType.id = dr.idItemType
	--join dbo.refDistributors distributors on distributors.NodeID = cast(docJournal.id/281474976710656 as smallint)
where
	--cast(docJournal.OpDate as date) = N'20171214'
	--docJournal.PrintDocNum = N'0020000000000028'
	docJournal.id = 562954288018759
	and enumsItemType.CodeKey != N'DocRowItemType_GlobalGoods' and enumsItemType.CodeKey != N'DocRowItemType_MixGoods'
	and distributors.deleted=0
