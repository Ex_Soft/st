﻿/*
delete from ch_old.dbo.XPUISettings where Tag = N'ExpandedNodesPositions'
insert into ch_old.dbo.XPUISettings select * from ch.dbo.XPUISettings where idPhysicalPersons = 0 and Tag = N'ExpandedNodesPositions'
*/
select * from ch_old.dbo.XPUISettings
------------------------------------------------------------
declare
	@db nvarchar(max) = N'dbsrv.ch_demo_2_22_2_x.',
	@name nvarchar(max) = N'!тест_представл',--N'Administrator', -- N'Василенко Дмитрий', --
	@body nvarchar(max)

set @body = 'delete ' + @db + 'dbo.XPUISettings from ' + @db + 'dbo.XPUISettings xpUISettings join ' + @db + 'dbo.refPhysicalPersons physicalPersons on physicalPersons.id = xpUISettings.idPhysicalPersons'
if len(coalesce(@name, N'')) > 0
	set @body += ' where physicalPersons.Name = N''' + @name + ''''
print @body

exec(@body)

delete dbo.XPUISettings from dbo.XPUISettings xpUISettings join dbo.refPhysicalPersons physicalPersons on physicalPersons.id = xpUISettings.idPhysicalPersons where physicalPersons.Name = N'Administrator'
------------------------------------------------------------

select * from dbo.XPUISettings where Tag like N'%StuResetPrintDocNumbersDlg%'
delete from dbo.XPUISettings where Tag like N'%StuAuthenticationRequired%'
delete from dbo.XPUISettings where Tag = N'StuAgentWorkflowEditDlg_WorkflowLayout'
delete from dbo.XPUISettings where idPhysicalPersons = 0

--update dbo.XPUISettings set Tag = N'showInfo2.21.3.4200' where id = 281475013865273

select
	*
from
	dbo.XPUISettings xpUISettings
	join dbo.refPhysicalPersons physicalPersons on physicalPersons.id = xpUISettings.idPhysicalPersons
where
	physicalPersons.Name = N'Administrator'
	and xpUISettings.Tag like N'showInfo%'

delete
	dbsrv.ch_demo_3_0_1_X.dbo.XPUISettings
from
	dbsrv.ch_demo_3_0_1_X.dbo.XPUISettings xpUISettings
	join dbsrv.ch_demo_3_0_1_X.dbo.refPhysicalPersons physicalPersons on physicalPersons.id = xpUISettings.idPhysicalPersons
where
	physicalPersons.Name = N'Administrator'

select
	*
from
	dbo.XPUISettings xpUISettings
where
	xpUISettings.Tag like N'%StuAgentWorkflowEditDlg_WorkflowLayout%'
