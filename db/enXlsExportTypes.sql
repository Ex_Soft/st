select
	*
from
	dbo.enXlsExportTypesBase enXlsExportTypes
	join dbo.EnumsBase enumsExportType on enumsExportType.id = enXlsExportTypes.idExportType
where
	enXlsExportTypes.refMetadata in (N'Routes', N'SKUs', N'BuyPoints')
order by enXlsExportTypes.refMetadata

if not exists (select 1 from dbo.enXlsExportTypesBase where id = 20151028424751 and refMetadata = N'SKUs')
	insert dbo.enXlsExportTypesBase (id, refMetadata, reportName, reportSP, ReportTemplate, idExportType, reportName_0)
	values (20151028424751, N'SKUs', N'������ �� Excel-�����', N'', N'', 20140127197521, N'Import from Excel-file')

if not exists (select 1 from dbo.enXlsExportTypesBase where id = 20151028424752 and refMetadata = N'SKUs')
	insert dbo.enXlsExportTypesBase (id, refMetadata, reportName, reportSP, ReportTemplate, idExportType, reportName_0)
	values (20151028424752, N'SKUs', N'������� ��������� ������� � Excel-����', N'', N'', 20140127197522, N'Export selected SKUs into Excel-file')

if not exists (select 1 from dbo.enXlsExportTypesBase where id = 20160127483951 and refMetadata = N'BuyPoints')
	insert dbo.enXlsExportTypesBase (id, refMetadata, reportName, reportSP, ReportTemplate, idExportType, reportName_0)
	values (20160127483951, N'BuyPoints', N'������ �� Excel-�����', N'', N'', 20140127197521, N'Import from Excel-file')

if not exists (select 1 from dbo.enXlsExportTypesBase where id = 20160127483952 and refMetadata = N'BuyPoints')
	insert dbo.enXlsExportTypesBase (id, refMetadata, reportName, reportSP, ReportTemplate, idExportType, reportName_0)
	values (20160127483952, N'BuyPoints', N'������� ��������� �� � Excel-����', N'', N'', 20140127197522, N'Export selected sales outlets into Excel-file')
